﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Text;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
public partial class _package : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
	    string channelID = rbm._get("channelID");

	    if (1==1)
	    {
            string filename = channelID + "-" + Stopwatch.GetTimestamp();
            string channelPath = "/_tmp/" + filename;
	        try
	        {
                DirectoryCopy(Server.MapPath("/_channel-source-code"), Server.MapPath(channelPath));
	        }
	        catch (Exception)
	        {
	            
	        }

            var objChannel = rbm.Channel.getData(channelID);
            string nextBuild = NextBuild(objChannel.buildVersion);
            var objTheme = rbm.Themes.GetChannelTheme(channelID);
            //rbm.Channel.stChannel objChannels = rbm.Channel.getChannel(channelID);
		    
            StringBuilder sb = new StringBuilder();

            //Manifest File
            using (StreamReader sr = new StreamReader(Server.MapPath(channelPath + "/manifest")))
            {
                sb.Append(sr.ReadToEnd());
            }

            sb.Replace("{{title}}", objChannel.title);
            sb.Replace("{{subtitle}}", objChannel.description);
            sb.Replace("{{mm_icon_focus_hd}}", objChannel.focus_icon_hd);
            sb.Replace("{{mm_icon_side_hd}}", objChannel.side_icon_hd);
            sb.Replace("{{mm_icon_focus_sd}}", objChannel.focus_icon_sd);
            sb.Replace("{{mm_icon_side_sd}}", objChannel.side_icon_sd);
            sb.Replace("{{build_version}}", nextBuild); //TODO
            sb.Replace("{{splash_hd}}", objChannel.splash_hd);
            sb.Replace("{{splash_sd}}", objChannel.splash_sd);
            sb.Replace("{{splash_bg_color}}", objChannel.splash_bg_color);


            using (StreamWriter file = new StreamWriter(Server.MapPath(channelPath + "/manifest"), false))
            {
                file.Write(sb.ToString());
            }
            //Manifest File

            //appMain.brs
            StringBuilder appMain = new StringBuilder();

            using (StreamReader sr = new StreamReader(Server.MapPath(channelPath + "/source/appMain.brs")))
            {
                appMain.Append(sr.ReadToEnd());
            }
            appMain.Replace("{{overhang_sd}}", objChannel.overhang_sd);
            appMain.Replace("{{overhang_hd}}", objChannel.overhang_hd);
            appMain.Replace("{{logo_sd}}", objChannel.logo_sd);
            appMain.Replace("{{logo_hd}}", objChannel.logo_hd);
            appMain.Replace("{{category_bg_color}}", objTheme.category_bg_color);
            appMain.Replace("{{category_txt_one}}", objTheme.category_txt_one);
            appMain.Replace("{{category_txt_two}}", objTheme.category_txt_two);
            using (StreamWriter file = new StreamWriter(Server.MapPath(channelPath + "/source/appMain.brs"), false))
            {
                file.Write(appMain.ToString());
            }
            //end-appMain.brs
            //appPosterScreen.brs AKA Category View
            StringBuilder appPosterScreen = new StringBuilder();

            using (StreamReader sr = new StreamReader(Server.MapPath(channelPath + "/source/appPosterScreen.brs")))
            {
                appPosterScreen.Append(sr.ReadToEnd());
            }
            appPosterScreen.Replace("{{video_list_style}}", objTheme.category_list_style);

            using (StreamWriter file = new StreamWriter(Server.MapPath(channelPath + "/source/appPosterScreen.brs"), false))
            {
                file.Write(appPosterScreen.ToString());
            }
            //end-appPosterScreen.brs
            //categoryFeed.brs
            StringBuilder sbFeed = new StringBuilder();

            using (StreamReader sr = new StreamReader(Server.MapPath(channelPath + "/source/categoryFeed.brs")))
            {
                sbFeed.Append(sr.ReadToEnd());
            }
            sbFeed.Replace("{{app_url}}", rbm.getSett("app_url"));
            sbFeed.Replace("{{memberID}}", objChannel.userID);
            sbFeed.Replace("{{channelID}}", objChannel.id);
            sbFeed.Replace("{{chksum}}", rbm.MD5(objChannel.id + rbm.getSett("hashkey") + objChannel.userID));
            using (StreamWriter file = new StreamWriter(Server.MapPath(channelPath + "/source/categoryFeed.brs"), false))
			{
                file.Write(sbFeed.ToString());
			}
            //end categoryFeed.brs

			//Copy Images
	        try
	        {
                DirectoryCopy(Server.MapPath("/_channel-images/" + channelID), Server.MapPath(channelPath + "/images"));
	        }
	        catch (Exception)
	        {

	        }

			//Create Zip File
            //ZipFile.CreateFromDirectory(Server.MapPath(channelPath), Server.MapPath(channelPath + ".zip"), CompressionLevel.Optimal, false);
            FastZip fastZip = new FastZip();

            bool recurse = true;  // Include all files by recursing through the directory structure
            string filter = null; // Dont filter any files at all
            fastZip.CreateZip(Server.MapPath(channelPath + ".zip"), Server.MapPath(channelPath), recurse, filter);
			//Directory.Delete(Server.MapPath(channelPath), true);
            //Response.Redirect(channelPath + ".zip");
            RBMLib.lib.write(channelPath + ".zip");

			//Attach and email the admin
            NotifyAdmin(channelID, filename + ".zip");

            //update build and version
            rbm.Channel.UpdateBuildVersion(objChannel.id, nextBuild);
            rbm.Channel.UpdateLastBuild(channelID, filename + ".zip");
           
            //update packages
            updatePackage(channelID, nextBuild, filename + ".zip"); 
	    }
    }
    public void updatePackage(string channelID, string nextBuild, string buildFile)
    {
        var obj = new rbm.Packages.stPackages();
        obj.channelID = channelID;
        obj.build_file = buildFile;
        obj.version = nextBuild;
        rbm.Packages.add(obj);
       
    }
    public string NextBuild(string buildNumber)
    {
        try
        {
            int current = Convert.ToInt32(buildNumber);
            int next = current + 1;
            return rbm.toStr(next);
        }
        catch (Exception)
        {
            return buildNumber;
        }
    }
    public void NotifyAdmin(string channelID, string filename)
    {
        var msg = new rbm.mail.stEmail();
        msg.to = rbm.toStr(rbm.getSett("build_admin"));
        msg.body = "Channel " + channelID + " is ready for testing and deployment.";
        msg.from = "no-reply@rightbrainmedia.com";
        msg.fromName = "ROKU - Build Notifications";

        msg.isHTML = false;
        msg.host = rbm.getSett("smtp_host");
        msg.port = rbm.getSett("smtp_port");
        msg.user = rbm.getSett("smtp_user");
        msg.password = rbm.getSett("smtp_password");
        msg.attachmentFilename = filename;

        rbm.mail.send(msg);
    }

    public void CreateSample(string outPathname,  string folderName)
    {

        FileStream fsOut = File.Create(outPathname);
        ZipOutputStream zipStream = new ZipOutputStream(fsOut);

        zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

        zipStream.Password = null;  // optional. Null is the same as not setting. Required if using AES.

        // This setting will strip the leading part of the folder path in the entries, to
        // make the entries relative to the starting folder.
        // To include the full path for each entry up to the drive root, assign folderOffset = 0.
        int folderOffset = 0;

        //CompressFolder(folderName, zipStream, folderOffset);

        zipStream.IsStreamOwner = true; // Makes the Close also Close the underlying stream
        zipStream.Close();
    }

	private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs = true)
	{
	    try
	    {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it. 
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location. 
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
	    }
	    catch (Exception)
	    {

            throw new DirectoryNotFoundException(
                "Source directory does not exist or could not be found: "
                + sourceDirName);
	    }


	}
}