﻿// ReSharper disable StringIndexOfIsCultureSpecific.1

//using roku; //to do : I dont understand this include...
using roku;
using System;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
	
public partial class _Default : Page {
	//string lang;
	string _page;
	string _section;

    protected void Page_Load(object sender, EventArgs e){
        //try
        //{
            frm.Action = Request.RawUrl;
            loadParams();
            scanControls();
        //}
        //catch (Exception)
        //{
        //    Response.Redirect("/error/500");
        //}
    }

	private void loadParams() {
		_section = getVal("section");
		_page = getVal("page");
        kb.log.write("url: " + Request.RawUrl);
        kb.log.write("QueryString: " + Request.QueryString);
        kb.log.write("lang: " + "\t\t\tfolder: " + _section + "\t\t\tfolder2: " + "\t\t\tfile: " + _page);
	}

	private void scanControls() { 
		Control cntrl = FindControl("frm");

		foreach (Control c in cntrl.Controls){
			if (c is module)
			{
				Control ascx = getControl(_section, "_module_" + c.ID);
				scanControl(ascx);
				c.Controls.Add(ascx);
			}
			else if (c is PlaceHolder)
			{
				Control ascx = getControl(c.ID);
                scanControl(ascx);
				c.Controls.Add(ascx);
			} 
			
		}
	}

	private string getVal(string param) {
		string val = rbm._get(param);
		if (string.IsNullOrEmpty(val)) {
			val = getSession(param);
		}
		return val;
	}

	private string getSession(string param) {
		string val = Convert.ToString(Session[param]);
		if (!string.IsNullOrEmpty(val)) return val;
		switch (param){// Default Names 
			case "section":
				val = "default";
				break;
			case "page":
				val = "default";
				break;
		}
		return val;
	}

	private Control getControl(string id) {
		string path;
		if (id == "header") {// Header of the page
			path = rbm.getPath() + "/" + _section + "/header.ascx";
			if (!File.Exists(Server.MapPath(path))){//if page header not exist, load top level header file
				path = rbm.getPath() + "/default/header.ascx";
			}
		} else if (id == "footer") {// Footer of the page
			path = rbm.getPath() + "/" + _section + "/footer.ascx";
			if (!File.Exists(Server.MapPath(path))){//if page footer not exist, load top level footer file
				path = rbm.getPath() + "/default/footer.ascx";
			}
		}
		else if (id.IndexOf("_global") > -1){// global include
			path = rbm.getPath() + "/default/" + id + ".ascx";
		} else { // Content of the page
			path = rbm.getPath() + "/" + _section + "/" + _page + ".ascx";
			if (!File.Exists(Server.MapPath(path)))
			{// Content not found, show error page
				path = rbm.getPath() + "/default/" + _section + ".ascx";
			}
			if (!File.Exists(Server.MapPath(path)))
			{// Content not found, show error page
				path = rbm.getPath() + "/default/" + _page + ".ascx";
			}

			if (!File.Exists(Server.MapPath(path))){// Content not found, show error page
				path = rbm.getPath() + "/error/404.ascx";
			}
            Control control = LoadControl(path);
            
			HtmlMeta meta;
			// Meta Keywords
            Label lblMeta = (Label)control.FindControl("meta_keywords");
            if (lblMeta != null){
                meta = new HtmlMeta {Name = "keywords", Content = lblMeta.Text};
	            MetaPlaceHolder.Controls.Add(meta);
            }
            // Meta Description
            lblMeta = (Label)control.FindControl("meta_description");
            if (lblMeta != null){
                meta = new HtmlMeta {Name = "description", Content = lblMeta.Text};
	            MetaPlaceHolder.Controls.Add(meta);
            }
            // Meta Title
            lblMeta = (Label)control.FindControl("meta_title");
            if (lblMeta != null){
                Page.Title = lblMeta.Text;
            }
		}
		return LoadControl(path);
	}

	private Control getControl(string page, string id) {
		string path = rbm.getPath() + "/" + page + "/" + id + ".ascx";

		if (!File.Exists(Server.MapPath(path))){
			path = rbm.getPath() + "/default/" + id + ".ascx";
		}
		return File.Exists(Server.MapPath(path)) ? LoadControl(path) : null;
	}

	private static string fixLink(string href) {
		if (href.IndexOf(".") > 0) {
			string[] tmp = href.Split('.');
			href = "/" + tmp[0] + "/" + tmp[1];
		} else {
			href = "/" + href;
		}
		return href;
	}
    
    private void scanControl(Control ascx) {
        foreach (Control cc in ascx.Controls){
            if (cc is HyperLink){
                scanLink(cc);
            }
			if (cc is module)
			{
				if (cc.ID == "script" || cc.ID == "style")
				{
					LiteralControl headRef = (LiteralControl)cc.Controls[0];
					Page.Header.Controls.Add(headRef);
				}
				else
				{
					Control tmp = getControl(_section, "_module_" + cc.ID);
					if (tmp != null)
					{
						scanControl(tmp);
						cc.Controls.Add(tmp);
					}
				}
			}
			else if (cc is PlaceHolder)
			{
				Control tmp = getControl(_section, cc.ID);
				if (tmp != null){
					scanControl(tmp);
					cc.Controls.Add(tmp);
				}
            }
	        
        }
    }

    private void scanLink(Control cont) {
	    HyperLink lnk = (HyperLink)cont;

		if (lnk.Attributes["class"] != "donotparse"){
			lnk.NavigateUrl = fixLink(lnk.NavigateUrl);
		}
    }

	public override void VerifyRenderingInServerForm(Control control)
	{

	}
}