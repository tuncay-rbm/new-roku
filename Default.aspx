﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>

<!doctype html>

<html lang="en">

<head runat="server">

	<title>Roku Channel Builder</title>

    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Roku, Rocku Channel, Channel, Roku Builder, Live Stream, Video, real-time, VOD, Live, Right Brain Media, Video On Demand">
    <meta name="author" content="Right Brain Media">

     	<!-- STYLES -->   
	<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
	<link rel="stylesheet" href="/css/colorpicker.css" type="text/css">
	<link rel="stylesheet" href="/css/yr_style.css" type="text/css">

    <!-- FAVICONS -->
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="icon" href="images/favicon.ico">
    
    <!-- CUSTOM FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <!-- SCRIPTS -->
    <script src="/js/jquery-1.10.2.min.js"></script>
    <script src="/js/jquery1-10-4ui.js"></script>
   	<script src="/js/main.js"></script>
    <script type="text/javascript" src="/js/retina.min.js"></script>
	<script src="/js/smooth.pack.js" type="text/javascript"></script> 

    <!-- HACKS -->
    <!--[if lt IE 9]>
      <script>
        document.createElement('video');
      </script>
    <![endif]-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>   


    <asp:PlaceHolder ID="MetaPlaceHolder" runat="server"></asp:PlaceHolder>
</head>
  
<body runat="server">

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P5JDLR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function (w, d, s, l, i) {
w[l] = w[l] || []; w[l].push(
{ 'gtm.start': new Date().getTime(), event: 'gtm.js' }
); var f = d.getElementsByTagName(s)[0],
j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
'//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-P5JDLR');</script>
<!-- End Google Tag Manager -->
    <form id="frm" runat="server">
		<asp:PlaceHolder ID="header" runat="server" />
	    <asp:PlaceHolder ID="content" runat="server"/>
	    <asp:PlaceHolder ID="footer" runat="server"/>
    </form>
</body>
</html>