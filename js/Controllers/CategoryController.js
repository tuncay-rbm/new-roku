﻿
angular.module('roku', ['colorpicker.module', 'ui.bootstrap', 'angularFileUpload'])
    .controller('CategoryController', CategoryController)
    .directive('errSrc', function () {
    return {
        link: function (scope, element, attrs) {
            element.bind('error', function () {
                if (attrs.src != attrs.errSrc) {
                    attrs.$set('src', attrs.errSrc);
                }
            });
        }
    }
});
function CategoryController($scope, $http, $modal, $log, FileUploader) {
    $scope.channelID = $('#hdChannelID').val();
    $scope.categoryOrder = function (channel) {
        ////console.log('category order', channel);
        $scope.categoryOrderModal('lg', $scope.categories);
    }
    $scope.categoryOrderModal = function (size, selectedAsset) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/CategoryOrder.html',
            controller: function ($scope, $modalInstance, asset) {
                $scope.categories = asset;
                $scope.close = function (reason) {
                    switch (reason) {
                        case 'cancel':
                            modalInstance.close();
                    }
                };

                $scope.orderUp = function (index, up) {
                    var down = !up;
                    var hold = [];
                    var arr = $scope.categories;
                    var x = index;
                    var y = up ? x - 1 : x + 1;
                    if ((up && x > 0) || (!up && x != (arr.length - 1))) {
                        for (var i = 0; i < arr.length; i++) {
                            if (i === x) {
                                hold.push(arr[i])
                                arr.splice(i, 1);
                            }
                        }
                        //////console.log('hold', hold);
                        for (var i = 0; i < arr.length + 1; i++) {
                            if (i === (y)) {
                                arr.splice(i, 0, hold[0]);
                            }
                        }
                    }
                };
                $scope.saveChanges = function () {
                    angular.forEach($scope.categories, function (item, index) {
                        item.ordr = index;
                        var url = 'api.ashx?action=category_order&categoryID=' + item.id + '&ordr=' + item.ordr;
                        ////console.log(url);
                        $http({
                            method: 'POST',
                            url: url,
                            headers: { 'Content-Type': 'text/plain' }
                        }).
                        success(function (data, status, headers, config) {
                            modalInstance.close();
                        }).
                        error(function (data, status, headers, config) {
                            $scope.error = true;
                        });
                    })
                }
            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;

                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
            location.reload();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());

        });
    };
    $scope.uploader = new FileUploader({
        url: 'api.ashx',
        removeAfterUpload: true,
        onSuccessItem: function (item, response, status, headers) {
            $scope.list();
        },
        formData: [],
        queueLimit: 1,
    });
    $scope.getChannel = function () {
        var url = 'api.ashx?action=get_channel&channelID=' + $scope.channelID;
        $http({
            method: 'GET',
            url: url,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.selectedChannel = data;
            $scope.selectedChannel.overhang_path = '/_channel-images/' + $scope.selectedChannel.id + '/' + $scope.selectedChannel.overhang_hd;
            $scope.selectedChannel.logo_path = '/_channel-images/' + $scope.selectedChannel.id + '/' + $scope.selectedChannel.logo_hd;

            ////console.log('$scope.selectedChannel', $scope.selectedChannel);
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.getChannel();
    $scope.getTheme = function () {
        var url = 'api.ashx?action=get_theme&channelID=' + $scope.channelID;
        $http({
            method: 'GET',
            url: url,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.theme = data;
            $scope.channelBackground = { 'background-color': $scope.theme.category_bg_color };
            $scope.channelTitles = { 'color': $scope.theme.category_txt_one };
            $scope.channelDescriptions = { 'color': $scope.theme.category_txt_two };
            ////console.log('theme', $scope.theme);
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.editStyles = function (channel) {
        ////console.log('editing style', channel);
        $scope.styleModal('lg', channel);
    };
    $scope.getTheme();

    //MODAL IMAGE EDITOR
    $scope.styleModal = function (size, selectedAsset) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/Style-Modal.html',
            backdrop: 'static',
            backdropClass: 'modal-backdrop', 
            controller: function ($scope, $modalInstance, $sce, asset) {
                $scope.selectedChannel = asset;
                $scope.selectedChannel.channel_image = '/_channel-images/' + $scope.selectedChannel.id + '/' + $scope.selectedChannel.focus_icon_hd;
                $scope.getTheme = function () {
                    var url = 'api.ashx?action=get_theme&channelID=' + $scope.selectedChannel.id;
                    $http({
                        method: 'GET',
                        url: url,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.selectedChannel.theme = data;
                        $scope.selectedChannel.theme.channelID = $scope.selectedChannel.id;
                        ////console.log('$scope.theme ', $scope.selectedChannel.theme);
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                }
                $scope.selectedChannel.theme = {};
                $scope.getTheme();
                $scope.selectedBackground = function () {
                    var color = $scope.selectedChannel.theme.category_bg_color;
                    if (color == null || color == '' || color == 'undefined') {
                        return '#ffffff';
                    } else {
                        return color;
                    }
                };
                $scope.selectedTitle = function () {
                    var color = $scope.selectedChannel.theme.category_txt_one;
                    if (color == null || color == '' || color == 'undefined') {
                        return '#000000';
                    } else {
                        return color;
                    }
                };
                $scope.selectedDescription = function () {
                    var color = $scope.selectedChannel.theme.category_txt_two;
                    if (color == null || color == '' || color == 'undefined') {
                        return '#000000';
                    } else {
                        return color;
                    }
                };

                $scope.logo_uploader = new FileUploader({
                    url: 'api.ashx?action=logo_hd',
                    removeAfterUpload: true,
                    onSuccessItem: function (item, response, status, headers) {
                        $scope.selectedChannel.logo_path = response + '?';
                    },
                    formData: [{ 'channelID': $scope.selectedChannel.id }],
                    queueLimit: 1,
                });

                $scope.uploader = new FileUploader({
                    url: 'api.ashx?action=overhang_hd',
                    removeAfterUpload: true,
                    onSuccessItem: function (item, response, status, headers) {
                        $scope.selectedChannel.overhang_path = response + '?';

                    },
                    formData: [{ 'action': 'overhang_hd' }, { 'channelID': $scope.selectedChannel.id }],
                    queueLimit: 1,
                });
                $scope.saveChanges = function () {
                    var action = 'update_theme';
                    var themeID = $scope.selectedChannel.theme.id;
                    if (themeID == null || themeID == '' || themeID == 'undefined') {
                        action = 'create_theme';
                    }
                    var url = 'api.ashx?action=' + action;
                    json = angular.toJson($scope.selectedChannel.theme);
                    ////console.log(json);
                    $http({
                        method: 'POST',
                        url: url,
                        data: "data=" + json,
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.closeModal();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                }
                $scope.closeModal = function () {
                    modalInstance.close();
                }
                $scope.setUploaderFormData = function () {
                    $scope.uploader.formData = [];
                    var formData = {};
                    formData.categoryID = $scope.selectedCategory.id;
                    $scope.uploader.formData.push(formData);
                    ////console.log('form data', $scope.uploader.formData);
                };

            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;

                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
            location.reload();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());

        });
    };
    $scope.list = function () {
        var url = 'api.ashx',
        action = '?action=list_categories',
        channelID = '&channelID=' + $scope.channelID,
        data = action + channelID;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.categories = data;
            if (!data.err) {
                for (var i = 0; i < $scope.categories.length; i++) {
                    ////console.log('i', i);
                }
            }
            else { $scope.noRecord = true; }
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.list()

    $scope.categoryHeader = function () {
        var url = 'api.ashx',
        action = '?action=list_images',
        channelID = '&channelID=' + $scope.channelID,
        data = action + channelID;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.image_options = data;
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }


    $scope.addCategory = function () {
        var url = 'api.ashx',
        action = '?action=add_category',
        channelID = '&channelID=' + $scope.channelID,

        data = action + channelID;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.categories = data;
            $scope.noRecord = false;
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });


    };
    $scope.deleteCategory = function (category) {
        $scope.selectedCategory = category;
        $scope.categoryDeleteModal('lg', $scope.selectedCategory);

    }
    $scope.categoryDeleteModal = function (size, selectedAsset) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/Confirm.html',
            controller: function ($scope, $modalInstance, asset) {
                $scope.asset = asset;
                $scope.deleteItem = function () {
                    var url = 'api.ashx?action=delete_category&id=' + $scope.asset.id;
                    $http({
                        method: 'POST',
                        url: url,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                        success(function (data, status, headers, config) {
                            modalInstance.close();

                        }).
                        error(function (data, status, headers, config) {
                            $scope.error = true;
                        });
                }
                $scope.closeModal = function () {
                    modalInstance.dismiss();
                }
            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;
                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
            $scope.list();
        }, function () {

        });
    };
    //MODAL IMAGE EDITOR
    $scope.modalUpdate = function (size, selectedAsset) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/CategoryModal.html',
            controller: function ($scope, $modalInstance, $sce, asset) {
                $scope.selectedCategory = asset;

                $scope.uploader = new FileUploader({
                    url: 'api.ashx?action=category_hd',
                    removeAfterUpload: true,
                    onSuccessItem: function (item, response, status, headers) {
                        $scope.selectedCategory.hd_path = response.hd_path + '?';
                    },
                    formData: [{ 'action': 'category_hd' }, { 'channelID': $scope.selectedCategory.channelID }, { 'categoryID': $scope.selectedCategory.id }],
                    queueLimit: 1,
                });
                $scope.setUploaderFormData = function () {
                    $scope.uploader.formData = [];
                    var formData = {};
                    formData.categoryID = $scope.selectedCategory.id;
                    $scope.uploader.formData.push(formData);
                    ////console.log('form data', $scope.uploader.formData);
                };




                $scope.channel_images = function () {
                    var url = 'api.ashx',
                    action = '?action=list_images',
                    channelID = '&channelID=' + $scope.selectedCategory.channelID,
                    data = action + channelID;
                    $http({
                        method: 'GET',
                        url: url + data,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.image_options = data;
                        ////console.log('$scope.image_options', $scope.image_options);
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                }

                //$scope.saveTheme = function () {
                //    var themeErr = false;
                //    if ($scope.theme.category_bg_color === '' || $scope.theme.category_bg_color === undefined) {
                //        $scope.pick_bg = true;
                //        themeErr = true;
                //    }
                //    if ($scope.theme.category_txt_one === '' || $scope.theme.category_txt_one === undefined) {
                //        $scope.pick_txt_one = true;
                //        themeErr = true;
                //    }
                //    if ($scope.theme.category_txt_two === '' || $scope.theme.category_txt_two === undefined) {
                //        $scope.pick_txt_two = true;
                //        themeErr = true;
                //    }
                //    if ($scope.theme.category_list_style === '' || $scope.theme.category_list_style === undefined) {
                //        $scope.pick_list = true;
                //        themeErr = true;
                //    }
                //    if(!themeErr) {
                //        ////console.log('saving', $scope.theme, '&category_bg_color=' + $scope.theme.category_bg_color);
                //        var url = 'api.ashx?action=save_theme&channelID=' + $scope.channelID;
                //        //category_bg_color = '&category_bg_color=' + $scope.theme.category_bg_color,
                //        //category_txt_one = '&category_txt_one=' + $scope.theme.category_txt_one,
                //        //category_txt_two = '&category_txt_two=' + $scope.theme.category_txt_two,
                //        //category_list_style = '&category_list_style=' + $scope.theme.category_list_style,
                //        var jsonObject = {
                //            id: $scope.theme.id,
                //            channelID:$scope.theme.channelID,
                //            category_bg_color: $scope.theme.category_bg_color,
                //            category_txt_one: $scope.theme.category_txt_one,
                //            category_txt_two: $scope.theme.category_txt_two,
                //            category_list_style: $scope.theme.category_list_style,
                //            video_bg_color: $scope.theme.category_bg_color,
                //            video_txt_one: $scope.theme.category_txt_one,
                //            video_txt_two: $scope.theme.category_txt_two,
                //            video_list_style: $scope.theme.category_list_style
                //        };
                //        json = angular.toJson(jsonObject);
                //        ////console.log(json);
                //        $http({
                //            method: 'POST',
                //            url: url,
                //            data: "data=" + json,
                //            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                //        }).
                //        success(function (data, status, headers, config) {
                //            $scope.themeSaved = true;
                //            $scope.closeModal();
                //        }).
                //        error(function (data, status, headers, config) {
                //            $scope.error = true;
                //        });
                //    }
                //}

                $scope.saveChanges = function () {
                    var url = 'api.ashx?action=save_category_changes';
                    json = angular.toJson($scope.selectedCategory);

                    $http({
                        method: 'POST',
                        url: url,
                        data: "data=" + json,
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.closeModal();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                }
                $scope.closeModal = function () {
                    modalInstance.close();
                }

                $scope.channel_images();


            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;

                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
            location.reload();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());

        });
    };
    if ($scope.selected) {
        $scope.list();
    }
    $scope.editCategoryScreen = function (category) {
        ////console.log('category', category);
        $scope.modalUpdate('lg', category);
    };
}
