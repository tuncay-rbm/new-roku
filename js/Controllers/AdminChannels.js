﻿
angular.module('roku', ['ui.bootstrap', 'angularFileUpload', 'angularUtils.directives.dirPagination','angular-loading-bar']).controller('AdminChannelsController', AdminChannelsController);
function AdminChannelsController($scope, $http, $modal,  FileUploader) {
    $scope.userID = $('#hdUserID').val();
    $scope.currentPage = 1;
    $scope.compileChannel = function (channel) {
        console.log(channel);
        $http({ method: 'POST', url: '/_package.aspx?channelID=' + channel.id }).success(function (response) { window.location.href = response; });
    }

    function list() {
        var url = '/api.ashx?action=admin_channels';
        $http({
            method: 'GET',
            url: url,
            headers: { 'Content-Type': 'text/plain' },
            ignoreLoadingBar: true
        }).
        success(function (data, status, headers, config) {
            $scope.channels = data;
            console.log('$scope.channels', $scope.channels);
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    list();
    $scope.reverse = false;
    $scope.sort = function (fieldName) {
        if ($scope.sortField === fieldName) {
            $scope.reverse = !$scope.reverse;
        } else {
            $scope.sortField = fieldName;
            $scope.reverse = false;
        }
    };

    $scope.isSortUp = function (fieldName) {
        return $scope.sortField === fieldName && !$scope.reverse;
    };
    $scope.isSorthown = function (fieldName) {
        return $scope.sortField === fieldName && $scope.reverse;
    };

    $scope.buildChannel = function (channel) {
        $scope.selectedChannel = channel;
        $scope.buildModal('lg', $scope.selectedChannel);

    }

    $scope.buildModal = function (size, selectedAsset) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/BuildProgress.html',
            controller: function ($scope, $modalInstance, asset) {
                $scope.channel = asset;
                $scope.build = function () {
                    var url = '/_package.aspx?channelID=' + $scope.channel.id;
                    $http({
                        method: 'POST',
                        url: url
                    }).
                    success(function (data, status, headers, config) {
                        $scope.dismiss();
                        window.location.href = data;
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                }
                $scope.build();
                $scope.dismiss = function () {
                    modalInstance.dismiss();

                }
            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;
                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
            //$scope.selected = selectedAsset;
        }, function () {
        });
    };
    $scope.editMode = false;
    $scope.editBuild = false;

    $scope.viewDetails = function (channel) {
        $scope.builds = [];
        $scope.editMode = true;
        $scope.selectedChannel = channel;
        $scope.selectedChannel.builds = [];
        var url = '/api.ashx?action=build_details&channelID=' + channel.id;
        $http({
            method: 'GET',
            url: url
        }).
        success(function (data, status, headers, config) {
            angular.forEach(data, function (item) { 
                $scope.selectedChannel.builds.push(item);
            })
        }).
        error(function (data, status, headers, config) {
        });
    }

    $scope.viewBuild = function (build) {
        $scope.editBuild = true;
        $scope.selectedBuild = build;
        $scope.editBuildModal('lg', $scope.selectedBuild);
    }
    $scope.editBuildModal = function (size, selectedAsset) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/AdminEditBuild.html',
            controller: function ($scope, $modalInstance, asset) {
                $scope.build = asset;

                $scope.status_options = [{label: "Not Submitted", value:""}, {label: "pending", value:"pending"}, {label: "released", value:"released"}, {label: "rejected", value:"rejected"}];
                //$scope.selectedPackageBox = asset.packageBox;
                $scope.packageBoxes = function () {
                    var url = '/api.ashx?action=package_boxes';
                    $http({
                        method: 'GET',
                        url: url
                    }).
                    success(function (data, status, headers, config) {
                        $scope.package_box_options = data;
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });

                }
                $scope.packageBoxes();
                //$scope.build = function () {
                //    var url = '/_package.aspx?channelID=' + $scope.channel.id;
                //    $http({
                //        method: 'POST',
                //        url: url
                //    }).
                //    success(function (data, status, headers, config) {
                //        $scope.dismiss();
                //        window.location.href = data;
                //    }).
                //    error(function (data, status, headers, config) {
                //        $scope.error = true;
                //    });
                //}
                //$scope.build();
                //$scope.dismiss = function () {
                //    modalInstance.dismiss();

                //}
            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;
                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
            //$scope.selected = selectedAsset;
        }, function () {
        });
    };
}

