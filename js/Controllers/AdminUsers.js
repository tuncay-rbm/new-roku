﻿
angular.module('roku', ['ui.bootstrap', 'angularFileUpload', 'angularUtils.directives.dirPagination', 'angular-loading-bar']).controller('AdminUsersController', AdminUsersController);
function AdminUsersController($scope, $http, $modal, FileUploader) {
    $scope.userID = $('#hdUserID').val();
    $scope.authToken = $('#hdAuthToken').val();
    console.log('$scope.authToken', $scope.authToken);
    $scope.currentPage = 1;
    $scope.disableUser = function (user) {
        var url = '/api.ashx?action=disable_user&id=' + user.id;
        $http({
            method: 'POST',
            url: url,
            headers: { 'Content-Type': 'text/plain' },
            ignoreLoadingBar: true
        }).
        success(function (data, status, headers, config) {
            list();

        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.activateUser = function (user) {
        var url = '/api.ashx?action=enable_user&id=' + user.id;
        $http({
            method: 'POST',
            url: url,
            headers: { 'Content-Type': 'text/plain' },
            ignoreLoadingBar: true
        }).
        success(function (data, status, headers, config) {
            list();


        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.loginAsUser = function (user) {
        window.location.href='/channels?action=cloaking&userID=' + user.id + '&token=' + $scope.authToken;
    }

    function list() {
        var url = '/api.ashx?action=admin_users';
        $http({
            method: 'GET',
            url: url,
            headers: { 'Content-Type': 'text/plain' },
            ignoreLoadingBar: true
        }).
        success(function (data, status, headers, config) {
            $scope.users = data;
            angular.forEach($scope.users, function (item) {
                //var url = '/api.ashx?action=get_rbm_user';
                //$http({
                //    method: 'GET',
                //    url: url,
                //    headers: { 'Content-Type': 'text/plain' },
                //    ignoreLoadingBar: true
                //}).
                //success(function (data, status, headers, config) {
                //    $scope.users = data;

                //}).
                //error(function (data, status, headers, config) {
                //    $scope.error = true;
                //});
            })
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    list();
    $scope.reverse = false;
    $scope.sort = function (fieldName) {
        if ($scope.sortField === fieldName) {
            $scope.reverse = !$scope.reverse;
        } else {
            $scope.sortField = fieldName;
            $scope.reverse = false;
        }
    };

    $scope.isSortUp = function (fieldName) {
        return $scope.sortField === fieldName && !$scope.reverse;
    };
    $scope.isSorthown = function (fieldName) {
        return $scope.sortField === fieldName && $scope.reverse;
    };



}

