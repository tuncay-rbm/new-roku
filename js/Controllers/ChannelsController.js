﻿angular.module('roku', ['ui.bootstrap', 'angularFileUpload', 'colorpicker.module']).controller('ChannelsController', ChannelsController);
function ChannelsController($scope, $http, $location, $sce, $modal, $log, $window, FileUploader) {
    $scope.userID = $('#hdUserID').val();
    $scope.authToken = jQuery('#hdAuthToken').val();
    $scope.mediaCount = 0;
    $scope.countingMedia = true;
    $scope.addChannel = function () {
        var url = 'api.ashx',
        action = '?action=add_channel',
        userID = '&userID=' + $scope.userID
        data = action + userID;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            list_channels();
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.editModal = function (size, selectedAsset) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/Channels-Modal.html',
            backdrop: 'static',

            controller: function ($scope, $modalInstance, $sce, asset) {
                $scope.selectedChannel = asset;
                console.log('asset', asset);
                $scope.asset = asset;
                $scope.logospinning = false;

                $scope.logoSpin = function () {
                    $scope.logospinning = true;
                }
                $scope.uploader = new FileUploader({
                    url: '_uploadFiles.aspx',
                    removeAfterUpload: true,
                    onSuccessItem: function (item, response, status, headers) {
                        angular.forEach($scope.image_options, function (item) {
                            if (item.option === 'focus_icon_hd' && $scope.action === 'focus_icon_hd') {
                                item.path = response;
                            }
                            if (item.option === 'splash_hd' && $scope.action === 'splash_hd') {
                                item.path = response;
                            }
                            
                        })

                        //console.log('upload response',response);
                        $scope.logospinning = false;
                    },
                    formData: [{ 'action': $scope.action }, { 'channelID': $scope.channelID }],
                    queueLimit: 1,
                });
                $scope.setUploaderFormData = function (option, channelID) {
                    $scope.action = option;
                    $scope.uploader.formData = [];
                    var formData = {};
                    formData.channelID = channelID;
                    formData.action = option;
                    $scope.uploader.formData.push(formData);
                    //console.log($scope.uploader.formData);
                }

                $scope.channel_images = function () {
                    var url = 'api.ashx',
                    action = '?action=list_images',
                    channelID = '&channelID=' + $scope.asset.id,
                    data = action + channelID;
                    $http({
                        method: 'GET',
                        url: url + data,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.image_options = data;

                        //console.log('$scope.image_options', $scope.image_options);
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                }

                $scope.saveChanges = function () {
                    var url = 'api.ashx?action=save_channel_changes';
                    json = angular.toJson($scope.asset);
                    $http({
                        method: 'POST',
                        url: url,
                        data: "data=" + json,
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.channel_images();
                        modalInstance.close();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                }

                $scope.compileChannel = function (channel) {
                    $scope.compiling = true;
                    var url = 'api.ashx?action=compile_channel&channelID=' + channel.id;
                    $http({
                        method: 'POST',
                        url: url,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.compileEmail();
                        //modalInstance.close();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.messageSent = true;
                        $scope.compiling = false;
                    });
                }
                $scope.messageSent = false;
                $scope.compileMessage = "We received and are processing your request. Check your email for your channel submission confirmation.";
                $scope.compileEmail = function () {
                    var url = 'api.ashx?action=send_compile_message&userID=' + asset.userID;
                    $http({
                        method: 'POST',
                        url: url,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.messageSent = true;
                        $scope.compiling = false;
                    }).
                    error(function (data, status, headers, config) {
                        $scope.compileMessage = "There was an error compiling your channel. Please reload the page and try again.";

                        $scope.messageSent = true;
                        $scope.compiling = false;
                    });
                }
                $scope.closeModal = function () {
                    modalInstance.close();
                }
                $scope.onlySplash = function (option) {
                    return (/*option.option === 'splash_sd' ||*/ option.option === 'splash_hd');
                }
                $scope.onlyFocus = function (option) {
                    return (option.option === 'focus_icon_hd');
                }
                $scope.channel_images();
                //video links
                $scope.trustLink = function (link) {
                    $scope.trustedLink = $sce.trustAsResourceUrl(link);
                };
                // $scope.trustLink($scope.asset.asset.link);
                $log.info('$scope.asset', $scope.asset, 'asset', asset, 'selectedAsset', selectedAsset);


            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;
                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
            location.reload();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    function list_channels() {
        var url = 'api.ashx',
        action = '?action=list_channels',
        userID = '&userID=' + $scope.userID
        data = action + userID;
        $http({
            method: 'GET',
            url: url + data,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.channels = data;
            $scope.list_categories();
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    list_channels();
    $scope.videoCounts = function (playlists) {
        angular.forEach(playlists, function (item) {
            item.videos = [];
            var url = 'api.ashx?action=get_playlist_videos&token=' + $scope.authToken + '&id=' + item.api_id;
            $http({
                method: 'GET',
                url: url,
                headers: { 'Content-Type': 'text/plain' }
            }).
            success(function (data, status, headers, config) {
                if (data != null) {
                    $scope.response = data;
                    if ($scope.response.code == 200) {
                        arr = $scope.response.response.assets;
                        angular.forEach(arr, function (asset) {
                            asset.selected = true;
                            item.videos.push(asset);
                            $scope.mediaCount++;
                        });
                    }
                }
            }).
            error(function (data, status, headers, config) {
                //console.log('http error');
            });
        })
        $scope.roku_playlists = playlists;

        //console.log('new playlists', playlists);
    }

    $scope.editChannel = function (channel) {
        $scope.selectedChannel = channel;
        $scope.editModal('lg', $scope.selectedChannel);
    };
    $scope.deleteChannel = function (channel) {
        var deleteItem = $window.confirm('Delete ' + channel.title + ' ?');
        if (deleteItem) {
            $http({ method: 'POST', url: 'api.ashx?action=delete_channel&id=' + channel.id }).
                success(function (data, status, headers, config) {
                    window.location.reload();
                }).
                error(function (data, status, headers, config) {
                    //console.log('http error');
                }
            );
        }
    }
    //get playlists and encoded assets within to calculate number of media files within the channel
    $scope.list_categories = function () {
        var cats = [];
        angular.forEach($scope.channels, function (item, index) {
            item.mediaCount = 0;
            var channelID = item.id;
            var url = 'api.ashx',
            action = '?action=list_categories',
            channelID = '&channelID=' + channelID,
            data = action + channelID;
            $http({
                method: 'GET',
                url: url + data,
                headers: { 'Content-Type': 'text/plain' }
            }).
            success(function (data, status, headers, config) {
                item.categories = data.err ? null : data;
                if (item.categories != null) {
                    angular.forEach(item.categories, function (category, categoryIndex) {
                        category.playlists = [];
                        var categoryID = category.id;

                        var url = 'api.ashx?action=get_roku_playlists&categoryID=' + categoryID;
                        $http({
                            method: 'GET',
                            url: url,
                            headers: { 'Content-Type': 'text/plain' }
                        }).
                        success(function (data, status, headers, config) {
                            category.playlists = data;
                            angular.forEach(category.playlists, function (pItem) {
                                pItem.videos = [];
                                var url = 'api.ashx?action=get_playlist_videos&token=' + $scope.authToken + '&id=' + pItem.api_id;
                                $http({
                                    method: 'GET',
                                    url: url,
                                    headers: { 'Content-Type': 'text/plain' }
                                }).
                                success(function (data, status, headers, config) {

                                    if (data != null) {
                                        $scope.response = data;
                                        if ($scope.response.code == 200) {
                                            arr = $scope.response.response.assets;
                                            angular.forEach(arr, function (asset) {
                                                asset.selected = true;
                                                pItem.videos.push(asset);
                                                item.mediaCount++;
                                            });
                                        }
                                    }
                                }).
                                error(function (data, status, headers, config) {
                                    //console.log('http error');
                                });
                            })
                        }).
                        error(function (data, status, headers, config) {
                            $scope.error = true;
                        });
                    })

                }
                }).
            error(function (data, status, headers, config) {
                $scope.error = true;
            });
        })
        $scope.countingMedia = false;

    }
    $scope.getRokuPlaylists = function () {
        var url = 'api.ashx?action=get_roku_playlists&categoryID=' + $scope.categoryID;
        $http({
            method: 'GET',
            url: url,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.videoCounts(data);
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });

    }
}
