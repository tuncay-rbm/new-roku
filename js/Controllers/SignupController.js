﻿angular.module('roku', []).controller('SignupController', SignupController);
function SignupController($scope, $http) {
    $scope.packageOptions = function () {
        $scope.tracker = true;
        $scope.error = false;
        $scope.success = false;
        var formvalidate = true;
        $scope.validation = {
            error: true,
            firstname: '',
            lastname: '',
            username: '',
            email: '',
            password: '',
            passwordconfirm: '',

        };

        //if ($scope.formData.firstname == undefined || $scope.formData.firstname == '') {
        //    $scope.validation.firstname = 'First name is required';
        //    formvalidate = false;
        //}
        //if ($scope.formData.lastname == undefined || $scope.formData.lastname == '') {
        //    $scope.validation.lastname = 'Last name is required';
        //    formvalidate = false;
        //}
        if ($scope.formData.email == undefined || $scope.formData.email == '') {
            $scope.validation.email = 'Valid email is required';
            formvalidate = false;
        }
        //if ($scope.formData.username == undefined || $scope.formData.username == '') {
        //    $scope.validation.username = 'Valid username is required';
        //    formvalidate = false;
        //}
        if ($scope.formData.password == undefined || $scope.formData.password == '') {
            $scope.validation.password = 'Password is required';
            formvalidate = false;
        }
        if ($scope.formData.passwordconfirm == undefined || $scope.formData.passwordconfirm == '') {
            $scope.validation.passwordconfirm = 'Both password fields are required';
            formvalidate = false;
        }
        //LENGTH
        //if ($scope.formData.username.length < 9) {
        //    $scope.validation.username = 'Username must be at least 9 characters in length';
        //    formvalidate = false;
        //}
        if ($scope.formData.password.length < 9) {
            $scope.validation.password = 'Password must be at least 9 characters in length';
            formvalidate = false;
        }

        //PASSWORD MATCH
        if ($scope.formData.password != $scope.formData.passwordconfirm) {
            $scope.validation.password = 'The password fields must match';
            formvalidate = false;
        }
        //REGEX FIELDS
        //var usernameregex = /^[a-zA-Z0-9._]+$/;
        //if (!usernameregex.test($scope.formData.username)) {
        //    $scope.validation.username = 'Username must only contain letters, numbers or the following [- _].';
        //    formvalidate = false;
        //}
        var pwregex = /^[a-zA-Z0-9-_,.!@]+$/;
        if (!pwregex.test($scope.formData.password)) {
            $scope.validation.password = 'Passwords must only contain letters, numbers or the following [- _ , . ! @].';
            formvalidate = false;
        }
        var emailregex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (emailregex.test($scope.formData.email)) {
            console.log('email passes');
        } else {
            console.log('email does not pass');
            $scope.validation.email = 'Valid email is required';
            formvalidate = false;
        }
        if (formvalidate == true) {
$scope.alreadyExists();

            //$scope.exists = false;
            //$scope.makePackageCall();


        }


    };
    $scope.exists = false;

    $scope.alreadyExists = function () {
        var _username = $scope.formData.email.replace('.', 'dot').replace('@', 'at');
        console.log('_username', _username);
        // Check for already existing accounts
        var url = 'api.ashx?action=already_exists',
        jsonObject = {
            firstname: _username,
            lastname: _username,
            email: $scope.formData.email,
            username: _username,
            password: $scope.formData.password,
            passwordconfirm: $scope.formData.passwordconfirm,
            errMsg: '',
            packageID: '',
            cardNumber: '',
            cardCVC: '',
            cardExp: '',
            apiAuthToken: '',
            apiUserID: '',
            responseCode: '',
            isValid: false
        },
        json = angular.toJson(jsonObject);
        console.log(json);
        $http({
            url: url,
            method: 'POST',
            data: "data=" + json,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).
        success(function (data, status, headers, config) {
            $scope.errMsg = data;
            console.log('$scope.errMsg', $scope.errMsg);
            if ($scope.errMsg.length) {
                $scope.exists = true;
            } else {
                $scope.exists = false;
                $scope.makePackageCall();
            }

        }).
        error(function (data, status, headers, config) {
            console.log('http error');
            $scope.error = true;
            $scope.tracker = false;
        });
    }

    $scope.makePackageCall = function () {
        $scope.selectedPackage = {};
        var url = 'api.ashx?action=get_packages'
        $http({
            method: 'GET',
            url: url,
            //data: data,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).
        success(function (data, status, headers, config) {
            $scope.packages = data.response.packages;
            $scope.selectedPackage = $scope.packages[0];
            $scope.nextStep(1);
        }).
        error(function (data, status, headers, config) {
            console.log('http error');
        });
    }
    $scope.setPackage = function (selectedPackage) {
        $scope.hasPackage = true;
        $scope.selectedPackage = selectedPackage;
        $scope.nextStep(2);
    }
    $scope.spinning = false;
    $scope.showSpin = function () {
        var chk = $('#lnkSubmit').attr('disabled');
        console.log('chk', chk);
        if (chk != 'disabled') {
            $scope.spinning = true;

        }

    }
    $scope.cardValidation = function () {
        var _href;
        function disableSubmit() {
            _href = $('#lnkSubmit').attr('href');
            $('#lnkSubmit').attr('disabled', 'disabled');
            $('#lnkSubmit').removeAttr('href');
        };
        disableSubmit();

        $('#txtCardNumber').blur(function () {
            validateCard();
            validateSubmit();
        });
        $('#txtCVC').blur(function () {
            validateCVC();
            validateSubmit();
        });
        $('#txtExpiration').blur(function () {
            validateExp();
            validateSubmit();
        });
        $('#txtCardNumber').keyup(function () {
            validateCard();
            validateSubmit();
        });
        $('#txtCVC').keyup(function () {
            validateCVC();
            validateSubmit();
        });
        $('#txtExpiration').keyup(function () {
            validateExp();
            validateSubmit();
        });

        function validateCard() {
            var cardErr = '';
            var card_num = $('#txtCardNumber').val();
            var bValidCard = $.payment.validateCardNumber(card_num);
            if (!bValidCard) {
                $('#cardErr').html('Invalid Card Number.');
                $('#lnkSubmit').attr('disabled', 'disabled');
                $('#lnkSubmit').removeAttr('href');
                return false;
            }
            else {
                $('#cardErr').html('');
                return true;
            }
        }
        function validateCVC() {
            var cardErr = '';
            var cvc = $('#txtCVC').val();
            var bValidCVC = $.payment.validateCardCVC(cvc);
            if (!bValidCVC) {
                $('#cvcErr').html('Invalid Security Code');
                $('#lnkSubmit').attr('disabled', 'disabled');
                $('#lnkSubmit').removeAttr('href');
                return false;
            }
            else {
                $('#cvcErr').html('');
                return true;
            }
        };
        function validateExp() {
            var cardErr = '';
            var exp = $('#txtExpiration').val().split('/');
            var exMonth = exp[0];
            var exYear = exp[1];
            var bValidExp = $.payment.validateCardExpiry(exMonth, exYear);
            if (!bValidExp) {
                $('#expErr').html('Invalid Expiration Date.');
                $('#lnkSubmit').attr('disabled', 'disabled');
                $('#lnkSubmit').removeAttr('href');
                return false;
            }
            else {
                $('#expErr').html('');
                return true;
            }
        };
        $('#txtCardNumber').payment('restrictNumeric');
        $('#txtCardNumber').payment('formatCardNumber');

        $('#txtCVC').payment('restrictNumeric');
        $('#txtCVC').payment('formatCardCVC');

        $('#txtExpiration').payment('formatCardExpiry');
        $('#txtExpiration').payment('formatCardExpiry');

        $('.cc_exp').payment('formatCardExpiry');
        $('.cc_cvc').payment('formatCardCVC');

        $('#cbAgree').click(function () {
            validateSubmit();
        });

        function validateSubmit() {
            var pkg = $('#hdPackageID').val() === '' || $('#hdPackageID').val() === 'undefined' ? false : true;
            var bAgree = $("#cbAgree").is(':checked');
            var bCard = validateCard();
            var bCode = validateCVC();
            var bExp = validateExp();
            if (bAgree && bCard && bCode && bExp && pkg) {
                $('#lnkSubmit').removeAttr('disabled');
                $('#lnkSubmit').attr('href', _href);
            }
            else {
                $('#lnkSubmit').attr('disabled', 'disabled');
                $('#lnkSubmit').removeAttr('href');
                console.log('pkg', pkg);
            }
        }


    };
    $scope.initializeController = function () {
        $scope.cardValidation();
        $scope.exists = true;
        $scope.step = { user: true, packages: false, card: false };
        $scope.nextStep = function (index) {
            switch (index) {
                case 0:
                    $scope.step.user = true;
                    $scope.step.packages = false;
                    $scope.step.card = false;
                    break;
                case 1:
                    if (!$scope.exists) {
                        $scope.step.user = false;
                        $scope.step.packages = true;
                        $scope.step.card = false;
                    }

                    break;
                case 2:
                    if ($scope.hasPackage) {
                        $scope.step.user = false;
                        $scope.step.packages = false;
                        $scope.step.card = true;
                    }

                    break;
            }
        }
        $scope.nextStep(0);
        $scope.formData = {};
        $scope.tracker = false;
        $scope.error = false;
        $scope.errorMsg = 'There was an error. Please try again.';
        $scope.success = false;
        $scope.successMsg = 'Contact information received.';
    };
    $scope.initializeController();



}

