﻿angular.module('roku', ['ui.bootstrap']).controller('AccountController', AccountController);

function AccountController($scope, $http, $modal) {
    //console.log('here');
    $scope.authToken = jQuery('#hdAuthToken').val();
    $scope.categoryID = jQuery('#hdCategoryID').val();
    $scope.userID = jQuery('#hdUserID').val();
    $scope.authToken = jQuery('#hdAuthToken').val();
    $scope.selectedTab = 1;
    $scope.viewTab = function (tab) {
        $scope.selectedTab = tab;

    }
    $scope.showPlans = jQuery('#hdShowPlans').val();
    function togglePlans() {
        if ($scope.showPlans === '1') {
            $scope.viewTab(2);
        } else {
            $scope.viewTab(1);

        }
    }
    $scope.noMatch = true;
    $scope.$watch('confirmPassword', function () {
        if ($scope.currentPassword) {
            if ($scope.confirmPassword && ($scope.newPassword === $scope.confirmPassword)) {
                $scope.noMatch = false;
            } else {
                $scope.noMatch = true;
                $scope.passwordMessage = 'New passwords do not match.'
            }
        }
    })
    $scope.$watch('newPassword', function () {
        if ($scope.confirmPassword) {
            if ($scope.newPassword && ($scope.newPassword === $scope.confirmPassword)) {
                $scope.noMatch = false;
            } else {
                $scope.noMatch = true;
                $scope.passwordMessage = 'New passwords do not match.'
            }
        }
    })
    $scope.cancelModal = function () {
        $scope.startCancel('lg', $scope.user);
    }
    $scope.cancelAccountResult = function () {
        $scope.confirmCancellation('lg', $scope.user);
    }
    $scope.reallyCancelAccount = function (asset) {
        //console.log('really cancel the account', asset);
        var url = 'api.ashx?action=cancel_account&token=' + $scope.authToken + '&apiUserID=' + asset.id;
        $http({
            method: 'POST',
            url: url,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.response = data;
            if (data.code != '200') {
                //console.log('cancel account err');
            } else {
                $scope.accountMessage = 'Your account has been cancelled.';
            }

        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.confirmCancellation = function (size, selectedAsset) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/CancelAccountConfirm.html',
            controller: function ($scope, $modalInstance, asset) {
                $scope.stuff = asset;
                $scope.close = function (option) {
                    if (option == 'cancel') {
                        modalInstance.dismiss();
                    }
                }
                $scope.confirmCancellation = function () {
                    modalInstance.close();
                }
            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;
                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
            $scope.reallyCancelAccount($scope.user);
        }, function () {
            ////console.log('modal dismissed');
        });
    };
    $scope.startCancel = function (size, selectedAsset) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/CancelAccount.html',
            controller: function ($scope, $modalInstance, asset) {
                $scope.stuff = asset;
                $scope.close = function (option) {
                    if (option == 'cancel') {
                        modalInstance.dismiss();
                    }
                }
                $scope.cancelTheAccount = function () {
                    modalInstance.close();
                }
            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;
                }
            }
        });
        modalInstance.result.then(function (selectedAsset) {
            $scope.cancelAccountResult();
        }, function () {
            //
        });
    };
    $scope.passwordSpin = false;
    $scope.updatePassword = function () {
        $scope.passwordSpin = true;
        if ($scope.newPassword === $scope.confirmPassword) {

            var url = 'api.ashx?action=update_password&token=' + $scope.authToken + '&apiUserID=' + $scope.user.id +  '&password=' + $scope.currentPassword + '&newPassword=' + $scope.newPassword;
            $http({
                method: 'POST',
                url: url,
                headers: { 'Content-Type': 'text/plain' }
            }).
            success(function (data, status, headers, config) {
                $scope.response = data;
                if (data.code != '200') {
                    //console.log('password err');
                } else {

                    $scope.noMatch = true;
                    $scope.passwordMessage = 'Your password has been changed.'

                    ////console.log(data);
                }
                $scope.currentPassword = '';
                $scope.newPassword = '';
                $scope.confirmPassword = '';
                $scope.passwordSpin = false;

            }).
            error(function (data, status, headers, config) {
                $scope.error = true;
            });
        } else {
            $scope.noMatch = true;
            $scope.passwordMessage = 'New passwords do not match.'
        }

    }
    $scope.choosePackage = function (pkg) {
        $scope.upcomingPackage = pkg.id;
        var url = 'api.ashx?action=upcoming_package_id&token=' + $scope.authToken + '&packageID=' + pkg.id + '&settingID=' + $scope.user.settings.upcoming_package_id.id;
        $http({
            method: 'POST',
            url: url,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {

            //console.log(data);
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.getUser = function () {
        var url = 'api.ashx?action=get_rbm_user&token=' + $scope.authToken;
        $http({
            method: 'GET',
            url: url,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.user = data.response.user;
            $scope.makePackageCall();
            
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.getUser();

    $scope.updateUser = function (user) {
        //console.log(user);

        var url = 'https://dev-api.rbmtv.com/v5/int/account/' + user.id + '?token=' + $scope.authToken + '&first_name=' + user.first_name + '&last_name=' + user.last_name + '&permissions=' + angular.toJson(user.permissions);
        json = angular.toJson($scope.user);
        $http({
            method: 'PUT',
            url: url,
            data: "data=" + json,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).
        success(function (data, status, headers, config) {
            //console.log(data);
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }
    $scope.nameUpdateMessage = '';
    $scope.update = function (user) {
        //console.log(user);
        $scope.userSpin = true;
        var url = 'api.ashx?action=update_rbm_user&token=' + $scope.authToken + '&id=' + user.id + '&first_name=' + user.first_name + '&last_name=' + user.last_name + '&permissions=' + angular.toJson(user.permissions);
        json = angular.toJson($scope.user);
        $http({
            method: 'POST',
            url: url,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).
        success(function (data, status, headers, config) {
            console.log(data);
            $scope.userSpin = false;
            $scope.nameUpdateMessage = 'Account updated successfully.';
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }

    $scope.makePackageCall = function () {
        $scope.selectedPackage = {};
        var url = 'api.ashx?action=get_packages'
        $http({
            method: 'GET',
            url: url,
            //data: data,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).
        success(function (data, status, headers, config) {
            $scope.packages = data.response.packages;
            angular.forEach($scope.packages, function (item) {
                if (item.id === $scope.user.plan.id) {
                    $scope.selectedPackage == item;
                    jQuery('#hdPackageID').val(item.id);

                }
            })
            $scope.upcomingPackage = $scope.user.settings.upcoming_package_id.value;
        }).
        error(function (data, status, headers, config) {
            //console.log('http error');
        });
    }
    $scope.cardSpin = false;

    $scope.cardValidation = function () {
        var _href;
        function disableSubmit() {
            _href = $('#lnkSubmit').attr('href');
            $('#lnkSubmit').attr('disabled', 'disabled');
            $('#lnkSubmit').removeAttr('href');
        };
        disableSubmit();
        $('#lnkSubmit').click(function () {
            $scope.cardSpin = true;
        })
        $('#txtCardNumber').blur(function () {
            validateCard();
            validateSubmit();
        });
        $('#txtCVC').blur(function () {
            validateCVC();
            validateSubmit();
        });
        $('#txtExpiration').blur(function () {
            validateExp();
            validateSubmit();
        });
        $('#txtCardNumber').keyup(function () {
            validateCard();
            validateSubmit();
        });
        $('#txtCVC').keyup(function () {
            validateCVC();
            validateSubmit();
        });
        $('#txtExpiration').keyup(function () {
            validateExp();
            validateSubmit();
        });

        function validateCard() {
            var cardErr = '';
            var card_num = $('#txtCardNumber').val();
            var bValidCard = $.payment.validateCardNumber(card_num);
            if (!bValidCard) {
                $('#cardErr').html('Invalid Card Number.');
                $('#lnkSubmit').attr('disabled', 'disabled');
                $('#lnkSubmit').removeAttr('href');
                return false;
            }
            else {
                $('#cardErr').html('');
                return true;
            }
        }
        function validateCVC() {
            var cardErr = '';
            var cvc = $('#txtCVC').val();
            var bValidCVC = $.payment.validateCardCVC(cvc);
            if (!bValidCVC) {
                $('#cvcErr').html('Invalid Security Code');
                $('#lnkSubmit').attr('disabled', 'disabled');
                $('#lnkSubmit').removeAttr('href');
                return false;
            }
            else {
                $('#cvcErr').html('');
                return true;
            }
        };
        function validateExp() {
            var cardErr = '';
            var exp = $('#txtExpiration').val().split('/');
            var exMonth = exp[0];
            var exYear = exp[1];
            var bValidExp = $.payment.validateCardExpiry(exMonth, exYear);
            if (!bValidExp) {
                $('#expErr').html('Invalid Expiration Date.');
                $('#lnkSubmit').attr('disabled', 'disabled');
                $('#lnkSubmit').removeAttr('href');
                return false;
            }
            else {
                $('#expErr').html('');
                return true;
            }
        };
        $('#txtCardNumber').payment('restrictNumeric');
        $('#txtCardNumber').payment('formatCardNumber');

        $('#txtCVC').payment('restrictNumeric');
        $('#txtCVC').payment('formatCardCVC');

        $('#txtExpiration').payment('formatCardExpiry');
        $('#txtExpiration').payment('formatCardExpiry');

        $('.cc_exp').payment('formatCardExpiry');
        $('.cc_cvc').payment('formatCardCVC');

        $('#cbAgree').click(function () {
            validateSubmit();
        });

        function validateSubmit() {
            var pkg = $('#hdPackageID').val() === '' || $('#hdPackageID').val() === 'undefined' ? false : true;
            var bCard = validateCard();
            var bCode = validateCVC();
            var bExp = validateExp();
            if (bCard && bCode && bExp && pkg) {
                $('#lnkSubmit').removeAttr('disabled');
                $('#lnkSubmit').attr('href', _href);
            }
            else {
                $('#lnkSubmit').attr('disabled', 'disabled');
                $('#lnkSubmit').removeAttr('href');
                //console.log('pkg', pkg);
            }
        }


    };
    $scope.cardValidation();
    togglePlans();

}
