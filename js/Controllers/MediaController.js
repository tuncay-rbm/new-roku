﻿angular.module('roku', ['ui.bootstrap', 'angularFileUpload',  'angularUtils.directives.dirPagination'])
.directive('errSrc', function () {
    return {
        link: function (scope, element, attrs) {
            element.bind('error', function () {
                if (attrs.src != attrs.errSrc) {
                    attrs.$set('src', attrs.errSrc);
                }
            });
        }
    }
})
.controller('MediaController', MediaController);
function MediaController($scope, $http, $location, $sce, $modal, $log, $window, FileUploader, $timeout) {
    $scope.currentPage = 1;
    $scope.uploadUrl = jQuery('#hdUploadUrl').val();
    $scope.username = jQuery('#hdUserName').val();
    $scope.profileGroupID = jQuery('#hdProfileID').val();
    $scope.userID = jQuery('#hdUserID').val();
    $scope.authToken = jQuery('#hdAuthToken').val();
    $scope.watching = false;

    //console.log($scope.uploadUrl, $scope.username, $scope.profileGroupID, $scope.userID, $scope.authToken);
    $scope.deleteAsset = function (asset) {
        $scope.confirmModal('lg', asset, $scope.authToken);

    }; 
    $scope.confirmModal = function (size, selectedAsset, token) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/Confirm.html',
            controller: function ($scope, $modalInstance, $sce, asset) {
                $scope.asset = asset;
                $scope.asset.title = asset.name;
                $scope.authToken = token;
                //////console.log('selected asset', $scope.asset, $scope.authToken);
                $scope.closeModal = function () {
                    modalInstance.dismiss();
                };
                $scope.deleteItem = function () {
                    var url = 'api.ashx?action=delete_original_asset&token=' + $scope.authToken + '&id=' + asset.id;
                    $http({
                        method: 'POST',
                        url: url,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        modalInstance.close();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                }

            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;

                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
            $scope.get_original_assets();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());

        });
    };
    //URL UPLOADER
    $scope.showUrlUpload = function () {

        $scope.params = {
            name: '',
            status: 'processing', 
            url: $scope.uploadUrl + $scope.authToken,
            id: $scope.profileGroupID,
            username: $scope.username
        }
        $scope.uploadModal('lg', $scope.params);
    }
    $scope.watchUrlUploads = function (queueItem) {
        $scope.original_assets.push(queueItem);
    }
    $scope.uploadModal = function (size, selectedAsset) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/Url-Upload.html',
            controller: function ($scope, $modalInstance, $sce, asset) {
                $scope.close = function () {
                    modalInstance.close();
                }

                $scope.uploadFromUrl = function (remote_url) {
                    $scope.spin = true;

                    //////console.log('url', remote_url);
                    var stuff = {
                        name: '',
                        status:'',
                        username: asset.username,
                        file_url: remote_url,
                        encoding_groups: asset.id,
                        upload_type: 'REMOTE',
                    }
                    var json = angular.toJson(stuff);
                    var url = asset.url;
                    $http({
                        method: 'POST',
                        url: url,
                        data: json,
                        headers: { 'Content-Type': 'application/json' }
                    }).
                    success(function (data, status, headers, config) {
                        var payload = data.response.payload;
                        stuff.name = data.response.payload.file_name;
                        $scope.err = false;
                        modalInstance.close();
                        //////console.log('remote upload', data);
                    }).
                    error(function (data, status, headers, config) {
                        $scope.spin = false;

                        $scope.err = true;
                        var arr = [];
                        if (data.code === 400) {
                            angular.forEach(data, function (item) {
                                arr = item.fields;
                                angular.forEach(arr, function (ritem) {
                                    $scope.message = ritem;
                                })
                            })
                        } else {
                            $scope.message = 'An error has occurred. Verify the URL and try again.';
                        }
                    });

                }
            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;

                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
            $scope.watchUrlUploads($scope.params);
        }, function () {

        });
    };
    //FILE UPLOADER
    var createdDate = Date.now();
    //////console.log('createdDate', createdDate);
    $scope.formData = [];

    $scope.invalidFile = false;

    $scope.invalidFileType = function (item) {
        //console.log(item);
        $scope.invalidFile = true;
        $scope.invalidFileMessage = 'The file extension: ' + item.type + ' is not a valid video extension.';

    }

    $scope.uploader = new FileUploader({
        url: $scope.uploadUrl + $scope.authToken,
        removeAfterUpload: true,
        onBeforeUploadItem: function (item) {
            var filename = item.file.name.replace(/[|&;$%@"<>()+,]/g, "").replace('[','').replace('#','');
            $scope.spin = true;
            var stuff = {
                //profiles: [1426, 1427, 1428],
                encoding_groups: $scope.profileGroupID,
                upload_type: 'BROWSER',
                username: $scope.username,
                file_name: filename,
                file_size: item.file.size,
                file_type: item.file.type,
                file_created_date: Date.now()
            }
            item.formData.push(stuff);
        },
        onSuccessItem: function (item, response, status, headers) {
            //console.log('upload response', response);
            $scope.spin = false;

        },
        queueLimit: 1,

    });
    $scope.uploader.filters.push({
        name: 'filesFilter',
        fn: function (item, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            if ('|video|mp4|mov|mkv|flv|avi|qt|wmv|webm|'.indexOf(type) !== -1) {
                return item;
            } else {
                $scope.invalidFileType(item);

            }
        }
    });


    $scope.get_original_assets = function () {
        ;
        var arr = [];
        var url = 'api.ashx',
        action = '?action=get_original_assets&token=' + $scope.authToken;
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.response = data;
            if ($scope.response.code == 200) {
                angular.forEach($scope.response.response.assets, function (item) {
                    if (item.status != 'ready') {
                        arr.push(item);
                    }
                });
                $scope.original_assets = arr;
                ////console.log('got some', $scope.original_assets);
            }
            
            if (($scope.original_assets.length > 0)) {
                $scope.watchFileStatus();
            }
            else {
                for (var i = 0; i < $scope.timeouts.length; i++) {
                    $scope.watching = false;
                    clearTimeout($scope.timeouts[i]);
                }
            }
        }).
        error(function (data, status, headers, config) {
            ////console.log('http err');
        });

        
    };
    $scope.get_original_assets();
    $scope.encoded_assets = [];
    $scope.get_encoded_assets = function () {
        var url = 'api.ashx',
        action = '?action=get_encoded_assets&token=' + $scope.authToken;
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.response = data;
            var arr = [];
            if ($scope.response.code == 200) {
                var assets = $scope.response.response.assets;
                angular.forEach(assets, function (item) {
                    item.createDate = new Date(item.created_at);
                    item.image_path = item.distribution.download_domain + $scope.username + '/' + item.meta.image;
                    arr.push(item);

                });
                $scope.encoded_assets = arr;

            }
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    };
    $scope.get_encoded_assets();
    $scope.timeouts = [];
    $scope.watchFileStatus = function () {
        checkStatus = window.setTimeout(
            function () {
                $scope.watching = true;
                $scope.get_original_assets();
                //console.log('updating');
            }, 3000);
        $scope.timeouts.push(checkStatus);
    }
}

