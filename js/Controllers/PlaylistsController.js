﻿angular.module('roku', ['ui.bootstrap', 'angularUtils.directives.dirPagination', 'angularFileUpload'])
    .controller('PlaylistController', PlaylistController)    
    .directive('errSrc', function () {
        return {
            link: function (scope, element, attrs) {
                element.bind('error', function () {
                    if (attrs.src != attrs.errSrc) {
                        attrs.$set('src', attrs.errSrc);
                    }
                });
            }
        }
    });
function PlaylistController($scope, $http, $filter, $modal) {
    //console.log('here');
    $scope.userID = jQuery('#hdUserID').val();
    $scope.categoryName = jQuery('#hdCategoryName').val();

    $scope.tips = {
        playlists_info: "Need to add How-To/Instructions for this page.",
        no_playlists: "There are no playlists for this category. Choose 'Create Playlist' above to get started.",
        noVideosToOrder: "There are no videos in this playlist.",
        noEncodedAssets: "You have no videos available. Visit the Media section to begin creating your library.", 
    };
    $scope.currentPage = 1;
    //SORTING & SEARCHING
    $scope.sortOptions = [{ option: 'Name', value: 'title' }, { option: 'Latest', value: 'id' }];
    $scope.sortField = undefined;
    $scope.reverse = false;
    $scope.sort = function (fieldName) {
        if ($scope.sortField === fieldName) {
            $scope.reverse = !$scope.reverse;
        } else {
            $scope.sortField = fieldName;
            $scope.reverse = false;
        }
    };

    $scope.isSortUp = function (fieldName) {
        return $scope.sortField === fieldName && !$scope.reverse;
    };
    $scope.isSorthown = function (fieldName) {
        return $scope.sortField === fieldName && $scope.reverse;
    };
    var toType = function (obj) {
        return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
    }

    var searchMatch = function (haystack, needle) {
        if (!needle) {
            return true;
        }
        if (toType(haystack) != 'string') {
            return String(haystack).indexOf(String(needle)) !== -1;
        } else {
            return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
        }
    };
    $scope.username = jQuery('#hdUsername').val();

    $scope.authToken = jQuery('#hdAuthToken').val();
    $scope.categoryID = jQuery('#hdCategoryID').val();
    $scope.userID = jQuery('#hdUserID').val();
    //original assets
    $scope.get_original_assets = function () {
        var url = 'api.ashx',
        action = '?action=get_original_assets&token=' + $scope.authToken;
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.response = data;
            if ($scope.response.code == 200) {
                $scope.orginal_assets = $scope.response.response.assets;
            }
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    };
    $scope.get_original_assets();

    //PLAYLIST ORDERING
    $scope.playlistOrder = function () {
        $scope.orderModal('lg', $scope.roku_playlists);

    }
    $scope.orderModal = function (size, selectedAsset) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/Playlist-Order.html',
            controller: function ($scope, $modalInstance, FileUploader, $sce, asset) {
                $scope.playlists = asset;
                $scope.closeModal = function () {
                    modalInstance.close();
                };
                $scope.orderUp = function (index, up) {
                    var down = !up;
                    var hold = [];
                    var arr = $scope.playlists;
                    var x = index;
                    var y = up ? x - 1 : x + 1;
                    if ((up && x > 0) || (!up && x != (arr.length-1))) {
                        for (var i = 0; i < arr.length; i++) {
                            if (i === x) {
                                hold.push(arr[i])
                                arr.splice(i, 1);
                            }
                        }
                        ////console.log('hold', hold);
                        for (var i = 0; i < arr.length + 1; i++) {
                            if (i === (y)) {
                                arr.splice(i, 0, hold[0]);
                            }
                        }
                    }

                };

                $scope.saveChanges = function () {
                    var arr = $scope.playlists;
                    for (var i = 0; i < arr.length; i++) {
                        var url = 'api.ashx?action=update_playlist_ordr&id=' + arr[i].id + '&ordr=' + i;
                        //console.log('url', url, arr[i].title);
                        $http({
                            method: 'POST',
                            url: url,
                            headers: { 'Content-Type': 'text/plain' }
                        }).
                        success(function (data, status, headers, config) {
                            $scope.closeModal();
                            ////console.log(data);
                        }).
                        error(function (data, status, headers, config) {
                            $scope.error = true;
                        });
                    }
                };

            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;

                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
        }, function () {

        });
    };
    $scope.orderingVideos = false;
    $scope.orderVideos = function (playlist) {
        $scope.orderingVideos = true;
        $scope.videosToOrder = playlist.videos;
    }
    //encoded media
    $scope.editAsset = function (asset) {
        //console.log(asset);
        asset.authToken = $scope.authToken;
        $scope.editAssetModal('lg', asset);

    }
    $scope.editAssetModal = function (size, selectedAsset) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/Video-Edit.html',
            controller: function ($scope, $modalInstance, FileUploader, $sce, asset) {
                $scope.selectedAsset = asset;
                $scope.formData = [];
                $scope.uploader = new FileUploader({
                    url: 'https://dev-api.rbmtv.com/v5/int/file_upload',
                    removeAfterUpload: true,
                    onBeforeUploadItem: function (item) {
                        var stuff = {
                            token: $scope.selectedAsset.authToken,
                            upload_type: 'thumbnail',
                            encoded_asset_id: $scope.selectedAsset.id
                        }
                        item.formData.push(stuff);
                    },
                    onSuccessItem: function (item, response, status, headers) {
                        //$scope.get_original_assets();
                        //console.log(response);
                    },
                    queueLimit: 1,
                });
                $scope.closeModal = function () {
                    modalInstance.dismiss();
                };
                $scope.saveChanges = function () {
                    var url = 'api.ashx?action=update_encoded_asset';
                    json = angular.toJson($scope.selectedAsset);
                    ////console.log('json', json);
                    $http({
                        method: 'POST',
                        url: url,
                        data: "data=" + json,
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).
                    success(function (data, status, headers, config) {
                        modalInstance.close();
                        ////console.log(data);
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                };

            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;

                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());

        });
    };
    $scope.encoded_assets = [];
    $scope.get_encoded_assets = function () {
        var url = 'api.ashx',
        action = '?action=get_encoded_assets&token=' + $scope.authToken;
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.response = data;
            var arr = [];
            if ($scope.response.code == 200) {
                var assets = $scope.response.response.assets;
                angular.forEach(assets, function (item) {
                    item.createDate = new Date(item.created_at);
                    item.image_path = item.distribution.download_domain + $scope.username + '/' + item.meta.image;
                    arr.push(item);
                    
                });
                $scope.encoded_assets = arr;

            }
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    };
    //magic
    $scope.videoCounts = function(playlists) {
        angular.forEach(playlists, function (item) {
            item.videos = [];
            var url = 'api.ashx?action=get_playlist_videos&token=' + $scope.authToken + '&id=' + item.api_id;
            $http({
                method: 'GET',
                url: url,
                headers: { 'Content-Type': 'text/plain' }
            }).
            success(function (data, status, headers, config) {
                if (data != null) {
                    $scope.response = data;
                    if ($scope.response.code == 200) {
                        arr = $scope.response.response.assets;
                        angular.forEach(arr, function (asset) {
                            asset.selected = true;
                            item.videos.push(asset);
                        });
                    }
                }
            }).
            error(function (data, status, headers, config) {
                //console.log('http error');
            });
        })
        $scope.roku_playlists = playlists;
        //console.log('new playlists', playlists);
    }
    $scope.getRokuPlaylists = function () {
        var url = 'api.ashx?action=get_roku_playlists&categoryID=' + $scope.categoryID;
        $http({
            method: 'GET',
            url: url,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            $scope.videoCounts(data);
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });

    }
    $scope.getRokuPlaylists();
    //PLAYLIST CRUD
    $scope.newPlaylist = function () {
        $scope.selectedPlaylist = {};
        $scope.selectedPlaylist.authToken = $scope.authToken;
        $scope.newPlaylistModal('lg', $scope.selectedPlaylist, $scope.categoryID);

    };
    $scope.editPlaylist = function (playlist) {
        $scope.selectedPlaylist = playlist;
        $scope.selectedPlaylist.authToken = $scope.authToken;
        $scope.newPlaylistModal('lg', $scope.selectedPlaylist, $scope.categoryID);

    }
    $scope.newPlaylistModal = function (size, selectedAsset, categoryID) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/Playlists-Edit.html',
            controller: function ($scope, $modalInstance, $sce, asset) {
                $scope.selectedPlaylist = asset;
                $scope.selectedPlaylist.categoryID = categoryID;

                $scope.closeModal = function () {
                    modalInstance.close();
                };
                $scope.saveChanges = function () {
                    var api_id = $scope.selectedPlaylist.api_id;
                    //console.log($scope.selectedPlaylist, api_id);
                    if (api_id == 'undefined' || api_id == null || api_id == '') {
                        //console.log('create');
                        $scope.createPlaylist();
                    } else {
                        $scope.updatePlaylist();
                    }
                }
                $scope.createPlaylist = function () {
                    var url = 'api.ashx?action=create_playlist&token=' + $scope.selectedPlaylist.authToken;
                    json = angular.toJson($scope.selectedPlaylist);

                    $http({
                        method: 'POST',
                        url: url,
                        data: "data=" + json,
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).
                    success(function (data, status, headers, config) {
                        var x = data;
                        angular.forEach(x, function (item) {
                            $scope.selectedPlaylist.api_id = item.id;
                            var url = 'api.ashx?action=create_roku_playlist';
                            $scope.selectedPlaylist.id = '0';
                            $scope.selectedPlaylist.ordr = '0';

                            var _title = $scope.selectedPlaylist.title;
                            var _description = $scope.selectedPlaylist.description;

                            if (_title == '' || _title == null || _title == 'undefined') {
                                $scope.selectedPlaylist.title = 'Title';
                            }
                            if (_description == '' || _description == null || _description == 'undefined') {
                                $scope.selectedPlaylist.description = 'Description';
                            }
                            json = angular.toJson($scope.selectedPlaylist);

                            $http({
                                method: 'POST',
                                url: url,
                                data: "data=" + json,
                                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                            }).
                            success(function (data, status, headers, config) {
                                $scope.closeModal();
                            }).
                            error(function (data, status, headers, config) {
                                $scope.error = true;
                            });
                        });
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                };
                $scope.updatePlaylist = function () {
                    //console.log('updating your playlist', $scope.selectedPlaylist);
                    var url = 'api.ashx?action=update_roku_playlist',
                    stuff = {
                        id: $scope.selectedPlaylist.id,
                        title: $scope.selectedPlaylist.title,
                        description: $scope.selectedPlaylist.description
                    }
                    json = angular.toJson(stuff);

                    $http({
                        method: 'POST',
                        url: url,
                        data: "data=" + json,
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.closeModal();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                };
            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;

                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
            location.reload();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());

        });
    };

    $scope.deletePlaylist = function (asset) {
        $scope.confirmModal('lg', asset, $scope.authToken);
    };

    $scope.confirmModal = function (size, selectedAsset, token) {
        var modalInstance = $modal.open({
            templateUrl: '/templates/Confirm.html',
            controller: function ($scope, $modalInstance, $sce, asset) {
                $scope.asset = asset;
                $scope.authToken = token;
                //console.log('selected asset', $scope.asset, $scope.authToken);
                $scope.closeModal = function () {
                    modalInstance.close();
                };
                $scope.deleteItem = function () {
                    //console.log('selected asset', $scope.asset.id, $scope.authToken);
                    var url = 'api.ashx?action=delete_roku_playlist&id=' + $scope.asset.id;
                    $http({
                        method: 'POST',
                        url: url,
                        headers: { 'Content-Type': 'text/plain' }
                    }).
                    success(function (data, status, headers, config) {
                        $scope.closeModal();
                    }).
                    error(function (data, status, headers, config) {
                        $scope.error = true;
                    });
                }

            },
            size: size,
            resolve: {
                asset: function () {
                    return selectedAsset;

                }
            }
        });

        modalInstance.result.then(function (selectedAsset) {
            location.reload();
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());

        });
    };

    $scope.cancelChanges = function () {
        $scope.getRokuPlaylists();
        $scope.addingVideos = false;
        $scope.orderChange = false;
    };
    //PLAYLIST VIDEOS
    $scope.getplaylistVideos = function (playlist) {

        var url = 'api.ashx',
        action = '?action=get_playlist_videos&token=' + $scope.authToken + '&id=' + playlist.api_id;
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            //console.log('playlist videos', data);
            var arr = [];
            var videos = [];
            if (data != null) {
                $scope.response = data;
                if ($scope.response.code == 200) {
                    arr = $scope.response.response.assets;
                    angular.forEach(arr, function (item) {
                        var vid = {
                            video_id: item.id,
                            video_title: item.title
                        };
                        videos.push(vid);
                    });
                    angular.forEach($scope.encoded_assets, function (item) {
                        item.selected = false;
                        for (var i = 0; i < videos.length; i++) {
                            if (videos[i].video_id == item.id) {
                                item.selected = true;
                                $scope.selectedPlaylist.videos.push(item);
                            }
                        }
                    })
                    //console.log($scope.selectedPlaylist);
                }
            }
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });
    }

    $scope.get_encoded_assets();

    $scope.addVideos = function (playlist) {
        $scope.addingVideos = true;
        $scope.orderingVideos = true;
        $scope.videosToOrder = playlist.videos;
        $scope.selectedPlaylist = playlist;
        angular.forEach($scope.videosToOrder, function (vItem) {
            angular.forEach($scope.encoded_assets, function (eItem) {
                if (vItem.id === eItem.id) {
                    eItem.selected = true;
                } 
            })

        })
        //$scope.selectedPlaylist.videos = [];
        //$scope.getplaylistVideos($scope.selectedPlaylist);

    }
    $scope.addVideoToPlaylist = function (asset) {
        asset.selected = true;
        asset.order = $scope.videosToOrder.length + 1;

        $scope.videosToOrder.push(asset);
    }
    $scope.removeVideoFromPlaylist = function (video) {
        angular.forEach($scope.videosToOrder, function (item,index) {
            if (item.id === video.id) {
                $scope.videosToOrder.splice(index, 1);
            }
        })
    }
    $scope.orderUp = function (index, up) {
        var hold = [];
        var arr = $scope.videosToOrder;
        var x = index;
        var y = up ? x - 1 : x + 1;
        if ((up && x > 0) || (!up && x != (arr.length - 1))) {
            for (var i = 0; i < arr.length; i++) {
                if (i === x) {
                    hold.push(arr[i])
                    arr.splice(i, 1);
                }
            }
            ////console.log('hold', hold);
            for (var i = 0; i < arr.length + 1; i++) {
                if (i === (y)) {
                    arr.splice(i, 0, hold[0]);
                }
            }

            for (var i = 0; i < arr.length; i++) {
                arr[i].order = i + 1;
            }
        }
    };
    $scope.savePlaylistOrder = function () {
        angular.forEach($scope.roku_playlists, function (item,index) {
            var url = 'api.ashx?action=update_playlist_ordr&id=' + item.id + '&ordr=' + index;
            //console.log('url', url, item.title);
            $http({
                method: 'POST',
                url: url,
                headers: { 'Content-Type': 'text/plain' }
            }).
            success(function (data, status, headers, config) {
                ////console.log(data);
            }).
            error(function (data, status, headers, config) {
                $scope.error = true;
            });
        })
        $scope.orderChange = false;
    }
    $scope.playlistUp = function (index, up) {
        $scope.orderChange = true;
        var hold = [];
        var arr = $scope.roku_playlists;
        var x = index;
        var y = up ? x - 1 : x + 1;
        if ((up && x > 0) || (!up && x != (arr.length - 1))) {
            for (var i = 0; i < arr.length; i++) {
                if (i === x) {
                    hold.push(arr[i])
                    arr.splice(i, 1);
                }
            }
            ////console.log('hold', hold);
            for (var i = 0; i < arr.length + 1; i++) {
                if (i === (y)) {
                    arr.splice(i, 0, hold[0]);
                }
            }
        }

    };
    $scope.pushVideo = function (video) {
        $scope.selectedPlaylist.videos.push(video);
        //console.log('playlistVideos', $scope.selectedPlaylist.videos);
    }
    $scope.spliceVideo = function (video) {
        if ($scope.selectedPlaylist.videos.length) {
            for (var i = 0; i < $scope.selectedPlaylist.videos.length; i++) {
                if ($scope.selectedPlaylist.videos[i] == video) {
                    $scope.selectedPlaylist.videos.splice(i, 1);
                    //console.log('playlistVideos', $scope.selectedPlaylist.videos);
                }
            }
        }
    }
    $scope.savePlaylistVideos = function () {
        var vids = '';
        if ($scope.videosToOrder.length) {
            for (var i = 0; i < $scope.videosToOrder.length; i++) {
                vids += $scope.videosToOrder[i].id + ',';
            }
        }
        
        vids = vids.slice(0, vids.lastIndexOf(','));
        //console.log('vids', vids);
        var url = 'api.ashx',
        action = '?action=save_playlist_selections&token=' + $scope.authToken + '&playlistID=' + $scope.selectedPlaylist.api_id + '&videos=' + vids;
        $http({
            method: 'GET',
            url: url + action,
            headers: { 'Content-Type': 'text/plain' }
        }).
        success(function (data, status, headers, config) {
            //console.log(data);
            $scope.addingVideos = false;
            $scope.videosToOrder = [];
           // location.reload();
        }).
        error(function (data, status, headers, config) {
            $scope.error = true;
        });

    }
    $scope.cancelAddingVideo = function () {
        $scope.addingVideos = false;
        $scope.selectedPlaylist.videos = [];

    }
}
