﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using RBMLib;

public partial class kb
{
    #region common
    public static string getPath(string lang)
    { return ConfigurationManager.AppSettings["path_for_files"] + "/" + lang; }
    #endregion

    #region dbQueryFunctions
    public static void runQueryNoReturn(string sql)
    {
        using (SqlConnection conn = new SqlConnection(lib.getConn()))
        {
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 600;
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }

    public static void runQueryNoReturn(string sql, IEnumerable<SqlParameter> _params)
    {
        using (SqlConnection conn = new SqlConnection(lib.getConn()))
        {
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                foreach (SqlParameter param in _params)
                {
                    cmd.Parameters.Add(param);
                }
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 600;
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }

    public static void runSPNoReturn(string sp, IEnumerable<SqlParameter> _params)
    {
        using (SqlConnection conn = new SqlConnection(lib.getConn()))
        {
            using (SqlCommand cmd = new SqlCommand(sp, conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (SqlParameter param in _params)
                {
                    cmd.Parameters.Add(param);
                }
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 600;
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }

    public static DataTable runQuery4DataTable(string sql, IEnumerable<SqlParameter> _params = null)
    {
        using (SqlConnection conn = new SqlConnection(lib.getConn()))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                if (_params != null)
                {
                    foreach (SqlParameter param in _params)
                    {
                        cmd.Parameters.Add(param);
                    }
                }
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    cmd.Parameters.Clear();
                    return ds.Tables[0];
                }
            }
        }
    }

    public static string runQuery4Value(string sql, IEnumerable<SqlParameter> _params = null)
    {
        using (SqlConnection conn = new SqlConnection(lib.getConn()))
        {
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                if (_params != null)
                {
                    foreach (SqlParameter param in _params)
                    {
                        cmd.Parameters.Add(param);
                    }
                }
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    return !dr.Read() ? "" : lib.toStr(dr["val"]);
                }
            }
        }
    }

    public static List<string> runQuery4Values(string sql, IEnumerable<SqlParameter> _params = null)
    {
        List<string> lst = new List<string>();
        using (SqlConnection conn = new SqlConnection(lib.getConn()))
        {
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                conn.Open();
                if (_params != null)
                {
                    foreach (SqlParameter param in _params)
                    {
                        cmd.Parameters.Add(param);
                    }
                }
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            lst.Add(lib.toStr(dr[dr.GetName(i)]));
                        }
                    }
                    return lst;
                }
            }
        }
    }

    public static string runSp4Value(string sql, IEnumerable<SqlParameter> _params = null)
    {
        using (SqlConnection conn = new SqlConnection(lib.getConn()))
        {
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 600;
                conn.Open();
                if (_params != null)
                {
                    foreach (SqlParameter param in _params)
                    {
                        cmd.Parameters.Add(param);
                    }
                }
                return lib.toStr(cmd.ExecuteScalar());
            }
        }
    }

    public static List<SqlParameter> SQLParam(SqlParameter param)
    {
        return new List<SqlParameter> { param };
    }

    public static SqlParameter SQLParam(string paramName, string paramValue)
    {
        return new SqlParameter(paramName, paramValue);
    }

    public static object isDbNull(object param)
    {
        return (lib.isNull(param)) ? DBNull.Value : param;
    }
    #endregion

    public class staticFiles
    {
        public static string getFileVersion(string path)
        {
            try
            {
                Dictionary<string, stFile> fileList;
                string json;
                try
                {
                    using (StreamReader streamReader = new StreamReader(HttpContext.Current.Server.MapPath("/static_file_versions.json")))
                    {
                        json = streamReader.ReadToEnd();
                    }
                }
                catch (Exception)
                {
                    json = "";
                }
                fileList = lib.isNull(json) ?
                    new Dictionary<string, stFile>() :
                    (Dictionary<string, stFile>)JsonConvert.DeserializeObject(json, typeof(Dictionary<string, stFile>));

                return fileList.ContainsKey(path) ? fileList[path].version : "1.0";
            }
            catch (Exception)
            {
                return "1.0";
            }
        }

        public Dictionary<string, stFile> files;

        public struct stFile
        {
            public string lastModified, version;
        }
    }

    public class error
    {
        public static void log(Exception ex, string url)
        {
            List<SqlParameter> lstParams = new List<SqlParameter>
			{
				SQLParam("@message", ex.Message),
				SQLParam("@source", ex.Source),
				SQLParam("@stack_trace", ex.StackTrace),
				SQLParam("@url", url)
			};

            runQueryNoReturn("Insert Into errors (site, message, source, stack_trace, url, date_time) Values('main', @message, @source, @stack_trace, @url, getdate())", lstParams);
        }
    }

    public class htmlModule
    {
        public struct stContent
        {
            public string id, _lang, _file, html, lastUpdatedOn, lastUpdatedBy;
        }

        public static stContent getHTML(string lang, string file)
        {
            stContent obj = new stContent { id = "0" };
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select * From html_content Where _lang = @lang and _file = @file", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@lang", lang);
                    cmd.Parameters.AddWithValue("@file", file);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj._lang = Convert.ToString(dr["_lang"]);
                            obj._file = Convert.ToString(dr["_file"]);
                            obj.html = Convert.ToString(dr["html"]);
                            obj.lastUpdatedOn = Convert.ToString(dr["lastUpdatedOn"]);
                            obj.lastUpdatedBy = Convert.ToString(dr["lastUpdatedBy"]);
                        }
                    }
                }
            }
            return obj;
        }

        public static void save(stContent obj)
        {
            if (obj.id == "0")
                insert(obj);
            else
                update(obj);
        }

        private static void update(stContent obj)
        {
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Update html_content Set html = @html, lastUpdatedOn = GETDATE(), lastUpdatedBy = @lastUpdatedBy Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@html", obj.html);
                    cmd.Parameters.AddWithValue("@lastUpdatedBy", obj.lastUpdatedBy);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private static void insert(stContent obj)
        {
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into html_content (_lang, _file, html, lastUpdatedOn, lastUpdatedBy) Values(@_lang, @_file, @html, GETDATE(), @lastUpdatedBy)", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@_lang", obj._lang);
                    cmd.Parameters.AddWithValue("@_file", obj._file);
                    cmd.Parameters.AddWithValue("@html", obj.html);
                    cmd.Parameters.AddWithValue("@lastUpdatedBy", obj.lastUpdatedBy);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static DataSet contentHistory(string ID)
        {
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select Top 10 id, lastUpdatedOn From html_content_history Where cID=@contentID Order by id Desc", conn))
                {
                    cmd.Parameters.AddWithValue("@contentID", ID);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static string getHistory(string ID)
        {
            stContent obj = new stContent { id = "0" };
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select html From html_content_history Where id = @ID", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@ID", ID);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        return dr.Read() ? Convert.ToString(dr["html"]) : "";
                    }
                }
            }
        }

    }

    public class image
    {
        public static byte[] getImage(string ID)
        {
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                using (SqlCommand comm = new SqlCommand("dbo.getImage", conn))
                {
                    comm.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    comm.Parameters.AddWithValue("@imageID", ID);
                    using (SqlDataReader dr = comm.ExecuteReader())
                    {
                        return dr.Read() ? (byte[])(dr["image"]) : null;
                    }
                }
            }
        }

        private struct stImage
        {
            public string caption, image, thumb, folder;
        }



        public static string ImageList4CKEditor()
        {
            List<stImage> lst = new List<stImage>();
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select id, caption From images Order by caption", conn))
                {
                    conn.Open();
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            stImage img = new stImage();
                            img.caption = lib.toStr(dr["caption"]);
                            img.image = "/images/" + dr["id"];
                            img.thumb = "/images/" + dr["id"] + "?h=75";
                            img.folder = "images";
                            lst.Add(img);
                        }
                    }
                }
            }
            return lib.obj2json(lst);
        }

        public static DataSet ImageList4Admin()
        {

            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select id, caption, credit, lang, description, savedOn, requestedOn From images Order by caption", conn))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static string upload(string caption, byte[] image)
        {
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into images (caption, image, savedOn) Values(@caption, @image, getdate());SELECT SCOPE_IDENTITY()", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@caption", caption);
                    cmd.Parameters.AddWithValue("@image", image);
                    conn.Open();
                    return lib.toStr(cmd.ExecuteScalar());
                }
            }
        }

        public static void delete(string id)
        {
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Delete from images where id=@id", conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void update(string id, string caption, string credit, string description)
        {
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Update images set caption=@caption, credit=@credit, description=@description Where id=@id", conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@caption", caption);
                    cmd.Parameters.AddWithValue("@credit", credit);
                    cmd.Parameters.AddWithValue("@description", description);

                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void replaceImage(string id, byte[] myImage)
        {
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Update images set image=@image Where id=@id", conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@image", myImage);

                    cmd.ExecuteNonQuery();
                }
            }
        }
    }

    public class files
    {
        public static DataSet FileList4Admin()
        {

            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Select id, caption, lang, description, savedOn, requestedOn From files Order by caption", conn))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static void replaceFile(string id, byte[] file_content)
        {
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Update files set file_content=@file_content Where id=@id", conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@file_content", file_content);

                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static string upload(string caption, byte[] file_content)
        {
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Insert Into files (caption, file_content, savedOn) Values(@caption, @file_content, getdate());SELECT SCOPE_IDENTITY()", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@caption", caption);
                    cmd.Parameters.AddWithValue("@file_content", file_content);
                    conn.Open();
                    return lib.toStr(cmd.ExecuteScalar());
                }
            }
        }

        public static void update(string id, string caption, string description)
        {
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Update files set caption=@caption,  description=@description Where id=@id", conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@caption", caption);
                    cmd.Parameters.AddWithValue("@description", description);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static stFile getFile(string id)
        {
            var obj = new stFile();
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select id, caption, description, file_content From files Where id=@id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.caption = Convert.ToString(dr["caption"]);
                            obj.description = Convert.ToString(dr["description"]);
                            obj.file_content = (byte[])dr["file_content"];
                        }
                    }
                }
            }
            return obj;
        }

        public static stFile getFileByCaption(string caption)
        {
            var obj = new stFile();
            obj.file_found = false;
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                using (SqlCommand cmd = new SqlCommand("Select file_content From files Where caption=@caption", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@caption", caption);
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.file_content = (byte[])dr["file_content"];
                            obj.file_found = true;

                        }
                    }
                }
            }
            return obj;
        }

        public struct stFile
        {
            public string caption, description, id;
            public byte[] file_content;
            public bool file_found;
        }
        public static void delete(string id)
        {
            using (SqlConnection conn = new SqlConnection(lib.getConn()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("Delete from files where id=@id", conn))
                {
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }

    public class log
    {
        public static void write(string msg)
        {
            if (HttpContext.Current.IsDebuggingEnabled)
            {
                try
                {
                    using (TextWriter tw = new StreamWriter(HttpContext.Current.Server.MapPath("/_log.txt"), true))
                    {
                        tw.WriteLine(DateTime.Now + "\t\t" + msg);
                        tw.Close();
                    }
                }
                catch (Exception)
                {
                }
            }
        }
    }
}