﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;

public partial class rbm{
	public class feed
	{
		public struct stFeed
		{
			public string title;
			public List<stPlaylist> playlist;
			
		}
		
		public struct stMeta
		{
			public string duration, description, video_bitrate, image;
		}
		
		public struct stPlaylist
		{
			public string title, image, hls;
			public stMeta meta;
				
		}
        public static string milliSecondToSecond(string duration)
        {
            try
            {
                      //convert millisecond to seconds
                int x = duration.Length;
                int y = x - 3;
                if (y > 0) {
                    duration = duration.Insert(y, ".");
                }
                return duration;
            }
            catch (Exception)
            {

                return duration;
            }
        }
		public static string getVideoFeed(string subCatID)
		{
			using (MySqlConnection conn = new MySqlConnection(getConn()))
			{
				using (MySqlCommand cmd = new MySqlCommand("Select * From playlists Where id = @id", conn))
				{
					conn.Open();
					cmd.Parameters.AddWithValue("@id", subCatID);
					using (MySqlDataReader dr = cmd.ExecuteReader())
					{
						StringBuilder xml = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + Environment.NewLine + "<feed>" + Environment.NewLine);
						if (dr.Read())
						{
							List<RBMtv.Playlist> playlist = getPlaylist(rbm.toStr(dr["api_id"]), subCatID);
							if (playlist != null)
							{
								xml.Append("\t<resultLength>" + playlist.Count + "</resultLength>" + Environment.NewLine);
								xml.Append("\t<endIndex>" + playlist.Count + "</endIndex>" + Environment.NewLine);

								foreach (RBMtv.Playlist asset in playlist)
								{
                                    asset.meta.duration = milliSecondToSecond(asset.meta.duration);
                                    xml.Append("\t<item sdImg=\"" + asset.image.Replace("https", "http") + "\" hdImg=\"" + asset.image.Replace("https", "http") + "\">" + Environment.NewLine);
									xml.Append("\t\t<title>" + asset.title + "</title>" + Environment.NewLine);
									xml.Append("\t\t<contentId>1</contentId>" + Environment.NewLine);
									xml.Append("\t\t<contentType>episode</contentType>" + Environment.NewLine);
									xml.Append("\t\t<contentQuality>SD</contentQuality>" + Environment.NewLine);
                                    xml.Append("\t\t\t<streamFormat>hls</streamFormat>" + Environment.NewLine);
									xml.Append("\t\t<media>" + Environment.NewLine);									
									xml.Append("\t\t\t<streamQuality>SD</streamQuality>" + Environment.NewLine);
									xml.Append("\t\t\t<streamBitrate>" + asset.meta.video_bitrate + "</streamBitrate>" + Environment.NewLine);
									xml.Append("\t\t\t<streamUrl>" + Environment.NewLine);
									xml.Append(asset.hls + Environment.NewLine);
                                    //live stream test...
                                    //xml.Append("http://cdn-dev-live-egress.rbmtv.com/rbmv5-live-20/_definst_/mp4:rokutesting/playlist.m3u8" + Environment.NewLine);
									xml.Append("</streamUrl>" + Environment.NewLine);
									xml.Append("\t\t</media>" + Environment.NewLine);
									xml.Append("\t\t<synopsis>" + Environment.NewLine);
									xml.Append("\t\t\t" + asset.meta.description + Environment.NewLine);
									xml.Append("\t\t</synopsis>" + Environment.NewLine);
									xml.Append("\t\t<genres></genres>" + Environment.NewLine);
									xml.Append("\t\t<runtime>" + asset.meta.duration + "</runtime>" + Environment.NewLine);
									xml.Append("\t</item>" + Environment.NewLine);
								}
								xml.Append("</feed>");
							}
							else
							{
								xml.Append("\t<resultLength>0</resultLength>" + Environment.NewLine);
								xml.Append("\t<endIndex>0</endIndex>" + Environment.NewLine);
								xml.Append("</feed>");

							}
							return xml.ToString();
						}
					}
				}
			}

			return null;
		}

        public static string getCategoryFeed(string channelID)
        {
            StringBuilder xml = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + Environment.NewLine + "<categories>" + Environment.NewLine);

            var objChannel = rbm.Channel.getData(channelID);
            //if (!isNull(objChannels.banner_ad_hd) || !isNull(objChannels.banner_ad_sd))
            //{
            //    xml.Append("\t<banner_ad");
            //    if (!isNull(objChannels.banner_ad_hd))
            //        xml.Append(" hd_img=\"" + getSett("app_url") + "/_channel-images/" + channelID + "/" + objChannels.banner_ad_hd + "\"");

            //    if (!isNull(objChannels.banner_ad_sd))
            //        xml.Append(" sd_img=\"" + getSett("app_url") + "/_channel-images/" + channelID + "/" + objChannels.banner_ad_sd + "\"");

            //    xml.Append(" />" + Environment.NewLine);
            //}


            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Select * From categories Where channelID = @id Order by ordr", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", channelID);
                    using (MySqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            xml.Append("<category title=\"" + toStr(dr["title"]) + "\" description=\"" + toStr(dr["description"]) + "\" sd_img=\"" + getSett("img_url") + "/_category-images/" + channelID + "/" + toStr(dr["id"]) + "/" + toStr(dr["category_sd"]) + "\" hd_img=\"" + getSett("img_url") + "/_category-images/" + channelID + "/" + toStr(dr["id"]) + "/" + toStr(dr["category_hd"]) + "\">" + Environment.NewLine);

                            using (MySqlConnection conn2 = new MySqlConnection(getConn()))
                            {
                                using (MySqlCommand cmd2 = new MySqlCommand("Select * From playlists Where categoryID = @id Order by ordr", conn2))
                                {
                                    conn2.Open();
                                    cmd2.Parameters.AddWithValue("@id", toStr(dr["id"]));
                                    using (MySqlDataReader dr2 = cmd2.ExecuteReader())
                                    {
                                        while (dr2.Read())
                                        {
                                            xml.Append("<categoryLeaf  title=\"" + toStr(dr2["title"]) + "\" description=\"" + toStr(dr2["description"]) + "\" feed=\"" + getURL() + "/_feed.aspx?action=videoFeed&id=" + toStr(dr2["id"]) + "&chksum=" + rbm.MD5(toStr(dr2["id"]) + "-rbm-") + "\" />" + Environment.NewLine);
                                        }
                                    }
                                }
                            }
                            xml.Append("</category>" + Environment.NewLine);
                        }
                    }
                }
            }
            xml.Append("</categories>");
            return xml.ToString();
        }
		
		private static string getFeedContent(string url)
		{
			if (!url.Contains("http"))
				url = "http://" + url;
			WebClient wc = new WebClient();
			return wc.DownloadString(url);
		}

		private static List<RBMtv.Playlist> getPlaylist(string playlistID, string subCatID)
		{
            //string chkDate = DateTime.Today.ToString("yyy-MM-dd");
            //string chk = MD5("rbm-acl-" + chkDate);
            //string username = rbm.getRbmUserBySubCategoryID(subCatID);
            //string password = chk;
			try
			{
                string url = rbm.getSett("playlistUrl");
                WebRequest request = WebRequest.Create(url + playlistID + "/json");
                //WebRequest request = WebRequest.Create("https://player.rbmtv.com/feed/184/json");

				HttpWebResponse response = (HttpWebResponse)request.GetResponse();
				Stream dataStream = response.GetResponseStream();
				StreamReader reader = new StreamReader(dataStream);
				string json = reader.ReadToEnd();
				var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                RBMtv.VideoList obj = (RBMtv.VideoList)Newtonsoft.Json.JsonConvert.DeserializeObject(json, typeof(RBMtv.VideoList));
                return obj.playlist;
			}
			catch (Exception ex)
			{
				return null;
			}
		}
	}

    public class RBMtv
    {
        public class Meta
        {
            public string video_bitrate { get; set; }
            public string audio_bitrate { get; set; }
            public string duration { get; set; }
            public object description { get; set; }
        }

        public class Playlist
        {
            public string synopsis { get; set; }
            public string title { get; set; }
            public string image { get; set; }
            public string hls { get; set; }
            public Meta meta { get; set; }
        }

        public class VideoList
        {
            public int width { get; set; }
            public int height { get; set; }
            public string title { get; set; }
            public string link { get; set; }
            public List<Playlist> playlist { get; set; }
        }
    }

}