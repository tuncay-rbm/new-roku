﻿using System;
using System.Web;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using Stripe;
using System.IO;
using System.Collections.Specialized;
using System.Globalization;
using ImageResizer;
using System.Net.Mime;

public partial class rbm{
	#region common
	private static string getConn()
    { return ConfigurationManager.ConnectionStrings["sqlConn"].ConnectionString; }
	public static string getSett(string sett)
	{ return ConfigurationManager.AppSettings[sett]; }
	public static string getPath()
	{ return ConfigurationManager.AppSettings["path_for_files"]; }
	public static string toStr(object obj)
	{ return Convert.ToString(obj); }
	public static Boolean isNull(object obj){
		try
		{ return string.IsNullOrEmpty(Convert.ToString(obj)); }
		catch (Exception)
		{ return false; }
	}
    public static string getPath(string path) {
        return HttpContext.Current.Server.MapPath(path);
    }
    public static string getToken() {
        var user = (rbm.Members.stMembers)HttpContext.Current.Session["user"];
        return user.authToken;    
    }
    public static string getAdminToken() {
        string chkDate = DateTime.Today.ToString("yyy-MM-dd");
        return MD5("rbm-acl-" + chkDate);
    }

    public static string getApiUserID()
    {
        var user = (rbm.Members.stMembers)HttpContext.Current.Session["user"];
        return user.apiUserID;
    }
	public static string getUrl(string section, string page = "") {
		return (section == "home" && isNull(page)) ? "/": "/" + section + "/" + page;
	}
    public static string mySqlDate(DateTime date)
    { return date.Year.ToString() + "-" + date.Month.ToString() + "-" + date.Day.ToString(); }
	public static void fill(DropDownList lst, DataSet ds, string valueField, string textField){
		lst.DataTextField = textField;
		lst.DataValueField = valueField;
		lst.DataSource = ds;
		lst.DataBind();
	}
	public static string userFriendlyShortDate(object dt){
		return isNull(toStr(dt)) ? "" : userFriendlyShortDate(Convert.ToDateTime(dt));
	}
	public static string userFriendlyShortDate(DateTime dt){
		return dt.ToString("MMM") + " " + dt.ToString("dd") + ", " + dt.Year;
	}
	public static string userFriendlyShortDateTime(object dt){
		return (isNull(toStr(dt))) ? "": userFriendlyShortDateTime(Convert.ToDateTime(dt));
	}
	public static string userFriendlyShortDateTime(DateTime dt){
        return dt.ToString("MMM") + " " + dt.ToString("dd") + ", " + dt.Year + " " + dt.ToString("hh") + ":" + dt.ToString("mm") + " " + dt.ToString("tt");
	}
	public static string formatDateTime(string dt){
        return formatDateTime(Convert.ToDateTime(dt));
	}
	public static string formatDateTime(DateTime dt){
        return dt.ToString("MM") + "-" + dt.ToString("dd") + "-" + dt.Year;
	}
	public static void writeCookie(string key, string value){
		HttpCookie cookie = new HttpCookie(key);
        cookie.Value = value;
        cookie.Expires = DateTime.Now.AddMonths(3);
        HttpContext.Current.Response.Cookies.Add(cookie);
	}
	public static string readCookie(string key) {
		HttpCookie cookie = HttpContext.Current.Request.Cookies[key];
		return (cookie != null) ? cookie.Value : "";
	}
	public static string sanitize(string input) {
		Regex rgx = new Regex("[^a-zA-Z0-9 @.-]");
		return rgx.Replace(input, "");
	}
	public static string ds2json(DataSet ds){ 
		return (ds.Tables[0].Rows.Count > 0)? JsonConvert.SerializeObject(ds.Tables[0]): "{\"err\":\"No record found\"}";
	}
    public static string obj2json(object obj, bool ignoreNull = true)
    {
        if (!ignoreNull)
        {
            JsonSerializerSettings sett = new JsonSerializerSettings();
            sett.NullValueHandling = NullValueHandling.Include;
            return JsonConvert.SerializeObject(obj, sett);
        }
        return JsonConvert.SerializeObject(obj);
    }
	public static string _get(string param) {
		return HttpContext.Current.Request.QueryString[param];
	}
	public static void echo(string content) {
		HttpContext.Current.Response.Write(content);
	}
	public static void die(){
		HttpContext.Current.Response.End();
	}
	private static object dataFix(string data)
	{ return isNull(data) ? null : data; }
	
	public static string getCommandArgument(object sender){
		return ((LinkButton)sender).CommandArgument;
	}
    public static object getStatus(object status)
    {
        return toStr(status) == "1" ? "Visible" : "Hidden";
    }
    public static object getAdmin(object status)
    {
        return toStr(status) == "1" ? "Yes" : "---";
    }
    public static byte[] GetBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }

    public static DataTable RemoveNulls(DataTable dt)
    {
        for (int a = 0; a < dt.Rows.Count; a++)
        {
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if (dt.Rows[a][i] == DBNull.Value)
                {
                    dt.Rows[a][i] = "";
                }
            }
        }

        return dt;
    }
    public static string fixPath(object path)
    {
        string _path = rbm.toStr(path);
        _path = _path.Replace(HttpContext.Current.Server.MapPath("/"), "");
        _path = _path.Replace(@"\", "/");
        return "/" + _path;
    }
    public static string MD5(string str)
    {
        byte[] textBytes = Encoding.Default.GetBytes(str);
        try
        {
            MD5CryptoServiceProvider cryptHandler = new MD5CryptoServiceProvider();
            byte[] hash = cryptHandler.ComputeHash(textBytes);
            string ret = "";
            foreach (byte a in hash)
            {
                if (a < 16)
                    ret += "0" + a.ToString("x");
                else
                    ret += a.ToString("x");
            }
            return ret;
        }
        catch
        {
            return "";
        }
    }

	public static string getURL()
	{
		string scheme = HttpContext.Current.Request.Url.Scheme; // will get http, https, etc.
		string host = HttpContext.Current.Request.Url.Host; // will get the domain
		string port = toStr(HttpContext.Current.Request.Url.Port); // will get the port
		return scheme + "://" + host + ":" + port;
	}

	#endregion
    public class Error
    {
        public DateTime TimeStamp { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }

        public Error()
        {
            this.TimeStamp = DateTime.Now;
        }

        public Error(string Message)
            : this()
        {
            this.Message = Message;
        }

        public Error(System.Exception ex)
            : this(ex.Message)
        {
            this.StackTrace = ex.StackTrace;
        }

        public override string ToString()
        {
            return this.Message + this.StackTrace;
        }
    }
    public class Packages
    {
        public struct stPackages
        {
            public string id, channelID, package_boxID, version, build_file, package_file, is_public, vanity_url, publish_status, compile_date, package_date, submit_date, release_date;
        }

        public static DataSet list()
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand("Select * From packages", conn))
                {
                    using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static DataSet listPackageBoxes()
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand("Select * From package_boxes", conn))
                {
                    using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static DataSet listByChannel(string channelID)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand("Select *, (Select name from package_boxes Where id = package_boxID) as packageBox From packages where channelID = @channelID", conn))
                {
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static stPackages getData(string channelID)
        {
            stPackages obj = new stPackages();
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Select * From packages Where channelID = @channelID", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    using (MySqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.channelID = Convert.ToString(dr["channelID"]);
                            obj.package_boxID = Convert.ToString(dr["package_boxID"]);
                            obj.version = Convert.ToString(dr["version"]);
                            obj.build_file = Convert.ToString(dr["build_file"]);
                            obj.package_file = Convert.ToString(dr["package_file"]);
                            obj.is_public = Convert.ToString(dr["is_public"]);
                            obj.vanity_url = Convert.ToString(dr["vanity_url"]);
                            obj.publish_status = Convert.ToString(dr["publish_status"]);
                            obj.compile_date = Convert.ToString(dr["compile_date"]);
                            obj.package_date = Convert.ToString(dr["package_date"]);
                            obj.submit_date = Convert.ToString(dr["submit_date"]);
                            obj.release_date = Convert.ToString(dr["release_date"]);
                        }
                    }
                }
            }
            return obj;
        }
        public static void add(stPackages obj)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Insert Into packages ( channelID, version, build_file, compile_date) Values( @channelID, @version, @build_file, @compile_date)", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", obj.channelID);
                    cmd.Parameters.AddWithValue("@version", obj.version);
                    cmd.Parameters.AddWithValue("@build_file", obj.build_file);
                    cmd.Parameters.AddWithValue("@compile_date", mySqlDate(DateTime.Now));
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void update(stPackages obj)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update packages Set  channelID=@channelID, package_boxID=@package_boxID, version=@version, build_file=@build_file, package_file=@package_file, is_public=@is_public, vanity_url=@vanity_url, publish_status=@publish_status, compile_date=@compile_date, package_date=@package_date, submit_date=@submit_date, release_date=@release_date Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@channelID", obj.channelID);
                    cmd.Parameters.AddWithValue("@package_boxID", obj.package_boxID);
                    cmd.Parameters.AddWithValue("@version", obj.version);
                    cmd.Parameters.AddWithValue("@build_file", obj.build_file);
                    cmd.Parameters.AddWithValue("@package_file", obj.package_file);
                    cmd.Parameters.AddWithValue("@is_public", obj.is_public);
                    cmd.Parameters.AddWithValue("@vanity_url", obj.vanity_url);
                    cmd.Parameters.AddWithValue("@publish_status", obj.publish_status);
                    cmd.Parameters.AddWithValue("@compile_date", obj.compile_date);
                    cmd.Parameters.AddWithValue("@package_date", obj.package_date);
                    cmd.Parameters.AddWithValue("@submit_date", obj.submit_date);
                    cmd.Parameters.AddWithValue("@release_date", obj.release_date);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
    public class contact_form
    {
        public struct stContact_form
        {
            public string id, name, email, message, subscribe;
        }
        public static DataSet list()
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand("Select * From contact_form", conn))
                {
                    using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static stContact_form getData(string id)
        {
            stContact_form obj = new stContact_form();
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Select * From contact_form Where id = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (MySqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.name = Convert.ToString(dr["name"]);
                            obj.email = Convert.ToString(dr["email"]);
                            obj.message = Convert.ToString(dr["message"]);
                            obj.subscribe = Convert.ToString(dr["subscribe"]);

                        }
                    }
                }
            }
            return obj;
        }
        public static void add(stContact_form obj)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Insert Into contact_form ( name, email, message,subscribe ) Values( @name, @email, @message, @subscribe)", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@name", obj.name);
                    cmd.Parameters.AddWithValue("@email", obj.email);
                    cmd.Parameters.AddWithValue("@message", obj.message);
                    cmd.Parameters.AddWithValue("@subscribe", obj.subscribe);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

    }

    public class Members
    {
        public struct stMembers
        {
            public string id, apiUserID, authToken, username, status, firstName, lastName, email, isAdmin, saveDate;
        }
        public struct Admin
        {
            public string authToken,id;
            public bool isAdmin;
        }
        public static DataSet list()
        {
            StringBuilder sql = new StringBuilder("Select * from members order by saveDate");

            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(toStr(sql), conn))
                {
                    using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static stMembers add(API.stSignup stSignup)
        {
            var obj = new stMembers();
            obj.apiUserID = stSignup.apiUserID;
            obj.authToken = stSignup.apiAuthToken;
            obj.username = stSignup.username;
            obj.firstName = stSignup.firstname;
            obj.lastName = stSignup.lastname;
            obj.email = stSignup.email;
            obj.isAdmin = "0";
            obj.status = "1";
            obj.saveDate = mySqlDate(DateTime.Now);

            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Insert Into members ( apiUserID, authToken, username, status, firstName, lastName, email, isAdmin, saveDate ) Values( @apiUserID, @authToken, @username, @status, @firstName, @lastName, @email, @isAdmin, @saveDate);select last_insert_id()", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@apiUserID", obj.apiUserID);
                    cmd.Parameters.AddWithValue("@authToken", obj.authToken);
                    cmd.Parameters.AddWithValue("@username", obj.username);
                    cmd.Parameters.AddWithValue("@status", obj.status);
                    cmd.Parameters.AddWithValue("@firstName", obj.firstName);
                    cmd.Parameters.AddWithValue("@lastName", obj.lastName);
                    cmd.Parameters.AddWithValue("@email", obj.email);
                    cmd.Parameters.AddWithValue("@isAdmin", obj.isAdmin);
                    cmd.Parameters.AddWithValue("@saveDate", obj.saveDate);
                    conn.Open();
                    obj.id = toStr(cmd.ExecuteScalar());
                }
            }
            rbm.Log(DateTime.Now + "\tAction:" + "Create Roku User" + "\tData:" + rbm.obj2json(obj));
            return obj;
        }

        public static stMembers getData(string id)
        {
            stMembers obj = new stMembers();
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Select * From members Where id = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (MySqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.apiUserID = Convert.ToString(dr["apiUserID"]);
                            obj.authToken = Convert.ToString(dr["authToken"]);
                            obj.username = Convert.ToString(dr["username"]);
                            obj.status = Convert.ToString(dr["status"]);
                            obj.firstName = Convert.ToString(dr["firstName"]);
                            obj.lastName = Convert.ToString(dr["lastName"]);
                            obj.email = Convert.ToString(dr["email"]);
                            obj.isAdmin = Convert.ToString(dr["isAdmin"]);
                            obj.saveDate = Convert.ToString(dr["saveDate"]);
                        }
                    }
                }
            }
            return obj;
        }

        public static stMembers getDataByApiID(string id)
        {
            stMembers obj = new stMembers();
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Select * From members Where apiUserID = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (MySqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.apiUserID = Convert.ToString(dr["apiUserID"]);
                            obj.authToken = Convert.ToString(dr["authToken"]);
                            obj.username = Convert.ToString(dr["username"]);
                            obj.status = Convert.ToString(dr["status"]);
                            obj.firstName = Convert.ToString(dr["firstName"]);
                            obj.lastName = Convert.ToString(dr["lastName"]);
                            obj.email = Convert.ToString(dr["email"]);
                            obj.isAdmin = Convert.ToString(dr["isAdmin"]);
                            obj.saveDate = Convert.ToString(dr["saveDate"]);
                        }
                    }
                }
            }
            return obj;
        }
        public static stMembers getMemberByUsername(string username)
        {
            stMembers obj = new stMembers();
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Select * From members Where username = @username", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@username", username);
                    using (MySqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.apiUserID = Convert.ToString(dr["apiUserID"]);
                            obj.authToken = Convert.ToString(dr["authToken"]);
                            obj.username = Convert.ToString(dr["username"]);
                            obj.status = Convert.ToString(dr["status"]);
                            obj.firstName = Convert.ToString(dr["firstName"]);
                            obj.lastName = Convert.ToString(dr["lastName"]);
                            obj.email = Convert.ToString(dr["email"]);
                            obj.isAdmin = Convert.ToString(dr["isAdmin"]);
                            obj.saveDate = Convert.ToString(dr["saveDate"]);
                        }
                    }
                }
            }
            return obj;
        }

        public static stMembers getMemberByEmail(string email)
        {
            stMembers obj = new stMembers();
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Select * From members Where email = @email", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@email", email);
                    using (MySqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.apiUserID = Convert.ToString(dr["apiUserID"]);
                            obj.authToken = Convert.ToString(dr["authToken"]);
                            obj.username = Convert.ToString(dr["username"]);
                            obj.status = Convert.ToString(dr["status"]);
                            obj.firstName = Convert.ToString(dr["firstName"]);
                            obj.lastName = Convert.ToString(dr["lastName"]);
                            obj.email = Convert.ToString(dr["email"]);
                            obj.isAdmin = Convert.ToString(dr["isAdmin"]);
                            obj.saveDate = Convert.ToString(dr["saveDate"]);
                        }
                    }
                }
            }
            return obj;
        }

        public static void updateAuthToken(string id, string authToken)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update members Set authToken=@authToken Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@authToken", authToken);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void disableUserByAPI(string apiUserID)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update members Set status=0 Where apiUserID=@apiUserID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@apiUserID", apiUserID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            rbm.Log(DateTime.Now + "\tAction:" + "disableUserByAPI" + "\tData:" + rbm.obj2json(apiUserID));

        }
        public static void disableUser(string id)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update members Set status=0 Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            rbm.Log(DateTime.Now + "\tAction:" + "disableUser" + "\tData:" + rbm.obj2json(id));

        }
        public static void enableUser(string id)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update members Set status=1 Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            rbm.Log(DateTime.Now + "\tAction:" + "enableUser" + "\tData:" + rbm.obj2json(id));

        }
    }

    public class Channel 
    {
        public struct stChannel
        {
            public string id, userID, title, description, side_icon_sd, side_icon_hd, focus_icon_sd, focus_icon_hd, overhang_sd, overhang_hd, logo_sd, logo_hd, splash_sd, splash_hd, splash_bg_color, createdOn, buildVersion, lastBuild;
        }
        public struct stTitleDescription
        {
            public string id, title, description;
        }
        //image options for json
        public struct stChannelImageOptions
        {
            public string title, option, path;
            public bool selected;
        }
        public struct stListImageOptions
        {
            List<stChannelImageOptions> image_options;
        }
        public static void NotifyAdmin(string channelID)
        {
            var msg = new rbm.mail.stEmail();
            msg.to = rbm.toStr(rbm.getSett("build_admin"));
            msg.body = "Channel " + channelID + " is ready for testing and deployment.";
            msg.from = "no-reply@rightbrainmedia.com";
            msg.fromName = "ROKU - Build Notifications";

            msg.isHTML = false;
            msg.host = rbm.getSett("smtp_host");
            msg.port = rbm.getSett("smtp_port");
            msg.user = rbm.getSett("smtp_user");
            msg.password = rbm.getSett("smtp_password");

            rbm.mail.send(msg);
        }

        public static List<stChannelImageOptions> channelImageOptions(string channelID, bool category = false)
        {
            var image_options = new stChannelImageOptions();
            var lst = new List<stChannelImageOptions>();
            string[] arr = { "side_icon_sd", "side_icon_hd", "focus_icon_sd", "focus_icon_hd", "overhang_sd", "overhang_hd", "logo_sd", "logo_hd", "splash_sd", "splash_hd" };
            string path = category ? "/_category-images/" : "/_channel-images/"; 
            for (int i = 0; i < arr.Length; i++)
            {
                string option = arr[i];
                using (MySqlConnection conn = new MySqlConnection(getConn()))
                {
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand("Select @option as 'option', CONCAT('" + path + "',Cast(id as CHAR(500)), '/', "+ option + " ) as 'path' From channel Where id=@channelID", conn))
                    {
                        cmd.Parameters.AddWithValue("@channelID", channelID);
                        cmd.Parameters.AddWithValue("@option", option);

                        using (MySqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                image_options.title = option.ToUpperInvariant().Replace("_", " ").Replace("SD", "Standard Res").Replace("HD", "High Res");
                                image_options.option = option;
                                image_options.path = Convert.ToString(dr["path"]);
                                image_options.selected = false;
                                lst.Add(image_options);
                            }
                        }
                    }
                }
            }
            return lst;
        }
        //end - image options for json
        public static DataSet list(string userID)
        {
            StringBuilder sql = new StringBuilder("Select *, CONCAT('/_channel-images/',Cast(id as CHAR(500)), '/', focus_icon_sd) as sd_path, CONCAT('/_channel-images/',Cast(id as CHAR(500)), '/', focus_icon_hd) as hd_path From channel Where userID=@userID");
       
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(toStr(sql), conn))
                {
                        cmd.Parameters.AddWithValue("@userID", userID); 
                    using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static DataSet listForAdmin()
        {

            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand("select c.id, c.title, c.description, c.focus_icon_hd,createdOn, buildVersion, lastBuild,  (Select concat(m.firstName, ' ', m.lastName) From members m where m.`status`=1 and m.id = c.userID) as 'owner' from channel c order by c.title", conn))
                {
                    using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static string add(string userID)
        {
            string id = "";
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Insert Into channel ( userID, title, description, side_icon_sd, side_icon_hd, focus_icon_sd, focus_icon_hd, overhang_sd, overhang_hd, logo_sd, logo_hd, splash_sd, splash_hd, splash_bg_color, createdOn, buildVersion, lastBuild ) Values( @userID, @title, @description, @side_icon_sd, @side_icon_hd, @focus_icon_sd, @focus_icon_hd, @overhang_sd, @overhang_hd, @logo_sd, @logo_hd, @splash_sd, @splash_hd, @splash_bg_color, @createdOn, @buildVersion, @lastBuild);select last_insert_id()", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@userID", userID);
                    cmd.Parameters.AddWithValue("@title", "Your Roku Channel");
                    cmd.Parameters.AddWithValue("@description", "Your Roku channel description");
                    cmd.Parameters.AddWithValue("@side_icon_sd", "side_icon_sd.png");
                    cmd.Parameters.AddWithValue("@side_icon_hd", "side_icon_hd.png");
                    cmd.Parameters.AddWithValue("@focus_icon_sd", "focus_icon_sd.png");
                    cmd.Parameters.AddWithValue("@focus_icon_hd", "focus_icon_hd.png");
                    cmd.Parameters.AddWithValue("@overhang_sd", "overhang_sd.png");
                    cmd.Parameters.AddWithValue("@overhang_hd", "overhang_hd.png");
                    cmd.Parameters.AddWithValue("@logo_sd", "logo_sd.png");
                    cmd.Parameters.AddWithValue("@logo_hd", "logo_hd.png");
                    cmd.Parameters.AddWithValue("@splash_sd", "default_splash_sd.png");
                    cmd.Parameters.AddWithValue("@splash_hd", "default_splash_hd.png");
                    cmd.Parameters.AddWithValue("@splash_bg_color", "#000000");
                    cmd.Parameters.AddWithValue("@createdOn", mySqlDate(DateTime.Now));
                    cmd.Parameters.AddWithValue("@buildVersion", "0");
                    cmd.Parameters.AddWithValue("@lastBuild", "");


                    conn.Open();
                    id = toStr(cmd.ExecuteScalar());
                }
            }
            defaultImages(id);
            rbm.Log(DateTime.Now + "\tAction:" + "Default Channel" + "\tData:" + rbm.obj2json(userID));
            return id;
        }

        public static void defaultImages(string channelID)
        {
            string sDirectory = Path.Combine(HttpRuntime.AppDomainAppPath, "_channel-images/" + channelID + "/");
            if (!System.IO.Directory.Exists(sDirectory)) System.IO.Directory.CreateDirectory(sDirectory);
            string SourcePath = System.IO.Path.Combine(HttpRuntime.AppDomainAppPath, "_channel-images/0/");
            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*", SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(SourcePath, sDirectory), true);
        }

        public static stChannel getData(string id)
        {
            stChannel obj = new stChannel();
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Select * From channel Where id = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (MySqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.userID = Convert.ToString(dr["userID"]);
                            obj.title = Convert.ToString(dr["title"]);
                            obj.description = Convert.ToString(dr["description"]);
                            obj.side_icon_sd = Convert.ToString(dr["side_icon_sd"]);
                            obj.side_icon_hd = Convert.ToString(dr["side_icon_hd"]);
                            obj.focus_icon_sd = Convert.ToString(dr["focus_icon_sd"]);
                            obj.focus_icon_hd = Convert.ToString(dr["focus_icon_hd"]);
                            obj.overhang_sd = Convert.ToString(dr["overhang_sd"]);
                            obj.overhang_hd = Convert.ToString(dr["overhang_hd"]);
                            obj.logo_sd = Convert.ToString(dr["logo_sd"]);
                            obj.logo_hd = Convert.ToString(dr["logo_hd"]);
                            obj.splash_sd = Convert.ToString(dr["splash_sd"]);
                            obj.splash_hd = Convert.ToString(dr["splash_hd"]);
                            obj.splash_bg_color = Convert.ToString(dr["splash_bg_color"]);
                            obj.buildVersion = Convert.ToString(dr["buildVersion"]);

                        }
                    }
                }
            }
            return obj;
        }

        public static stTitleDescription saveTitleAndDescription(stTitleDescription obj)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update channel Set title=@title, description=@description Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@title", obj.title);
                    cmd.Parameters.AddWithValue("@description", obj.description);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            return obj;
        }
        public static void SideIconSD(string channelID, string fileName)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update channel Set side_icon_sd=@fileName Where id=@channelID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    cmd.Parameters.AddWithValue("@fileName", fileName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void SideIconHD(string channelID, string fileName)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update channel Set side_icon_hd=@fileName Where id=@channelID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    cmd.Parameters.AddWithValue("@fileName", fileName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void FocusIconSD(string channelID, string fileName)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update channel Set focus_icon_sd=@fileName Where id=@channelID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    cmd.Parameters.AddWithValue("@fileName", fileName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void FocusIconHD(string channelID, string fileName)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update channel Set focus_icon_hd=@fileName Where id=@channelID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    cmd.Parameters.AddWithValue("@fileName", fileName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void OverhangSD(string channelID, string fileName)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update channel Set overhang_sd=@fileName Where id=@channelID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    cmd.Parameters.AddWithValue("@fileName", fileName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void OverhangHD(string channelID, string fileName)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update channel Set overhang_hd=@fileName Where id=@channelID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    cmd.Parameters.AddWithValue("@fileName", fileName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void LogoSD(string channelID, string fileName)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update channel Set logo_sd=@fileName Where id=@channelID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    cmd.Parameters.AddWithValue("@fileName", fileName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void LogoHD(string channelID, string fileName)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update channel Set logo_hd=@fileName Where id=@channelID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    cmd.Parameters.AddWithValue("@fileName", fileName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void SplashSD(string channelID, string fileName)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update channel Set splash_sd=@fileName Where id=@channelID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    cmd.Parameters.AddWithValue("@fileName", fileName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void SplashHD(string channelID, string fileName)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update channel Set splash_hd=@fileName Where id=@channelID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    cmd.Parameters.AddWithValue("@fileName", fileName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void SplashColor(string channelID, string color)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update channel Set splash_bg_color=@color Where id=@channelID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    cmd.Parameters.AddWithValue("@color", color);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void UpdateBuildVersion(string channelID, string buildVersion)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update channel Set buildVersion=@buildVersion Where id=@channelID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    cmd.Parameters.AddWithValue("@buildVersion", buildVersion);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void UpdateLastBuild(string channelID, string lastBuild)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update channel Set lastBuild=@lastBuild Where id=@channelID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    cmd.Parameters.AddWithValue("@lastBuild", lastBuild);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void DeleteChannel(string id)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Delete from channel where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }

        }
    }

    public class Categories {
        public struct stCategories
        {
            public string id, channelID, title, description, category_sd, category_hd, ordr;
        }

        public static DataSet list(string channelID)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand("Select c.*,false as 'selected', false as 'edit_sd', false as 'edit_hd', CONCAT('/_category-images/',Cast(channelID as CHAR(500)), '/', category_sd) as sd_path, CONCAT('/_category-images/',Cast(channelID as CHAR(500)), '/', Cast(c.id as CHAR(500)), '/', category_hd) as hd_path From categories c Where channelID=@channelID order by ordr", conn))
                {
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }
        public static DataSet saveTitleAndDescription(string channelID, string id, string title, string description)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update categories Set title=@title, description=@description Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@title", title);
                    cmd.Parameters.AddWithValue("@description", description);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            return list(channelID);
        }
        public static void update(stCategories obj)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update categories Set id=@id, channelID=@channelID, title=@title, description=@description, category_sd=@category_sd, category_hd=@category_hd, ordr=@ordr Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@channelID", obj.channelID);
                    cmd.Parameters.AddWithValue("@title", obj.title);
                    cmd.Parameters.AddWithValue("@description", obj.description);
                    cmd.Parameters.AddWithValue("@category_sd", obj.category_sd);
                    cmd.Parameters.AddWithValue("@category_hd", obj.category_hd);
                    cmd.Parameters.AddWithValue("@ordr", obj.ordr);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void updateOder(string categoryID, string ordr)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update categories Set ordr=@ordr Where id=@categoryID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@categoryID", categoryID);
                    cmd.Parameters.AddWithValue("@ordr", ordr);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void CategorySD(string id, string fileName)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update categories Set category_sd=@fileName Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@fileName", fileName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void CategoryHD(string id, string fileName)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update categories Set category_hd=@fileName Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@fileName", fileName);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void add(string channelID)
        {
            var obj = new rbm.Categories.stCategories();
            obj.channelID = channelID;
            obj.title = "New Category";
            obj.description = "";
            obj.category_sd = "";
            obj.category_hd = "";
            obj.ordr = "0";
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Insert Into categories ( channelID, title, description, category_sd, category_hd, ordr ) Values( @channelID, @title, @description, @category_sd, @category_hd, @ordr)", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", obj.channelID);
                    cmd.Parameters.AddWithValue("@title", obj.title);
                    cmd.Parameters.AddWithValue("@description", obj.description);
                    cmd.Parameters.AddWithValue("@category_sd", obj.category_sd);
                    cmd.Parameters.AddWithValue("@category_hd", obj.category_hd);
                    cmd.Parameters.AddWithValue("@ordr", obj.ordr);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void delete(string id)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Delete From categories Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static stCategories getData(string id)
        {
            stCategories obj = new stCategories();
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Select * From categories Where id = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (MySqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.channelID = Convert.ToString(dr["channelID"]);
                            obj.title = Convert.ToString(dr["title"]);
                            obj.description = Convert.ToString(dr["description"]);
                            obj.category_sd = Convert.ToString(dr["category_sd"]);
                            obj.category_hd = Convert.ToString(dr["category_hd"]);
                            obj.ordr = Convert.ToString(dr["ordr"]);
                        }
                    }
                }
            }
            return obj;
        }
    }

    public class RokuPlaylists
    {
        public struct stPlaylists
        {
            public string id, categoryID, api_id, title, description, ordr;
        }

        public static DataSet list()
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand("Select * From playlists", conn))
                {
                    using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static DataSet listByCategoryID(string categoryID)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand("Select * From playlists where categoryID=@categoryID order by ordr", conn))
                {
                    cmd.Parameters.AddWithValue("@categoryID", categoryID);

                    using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static stPlaylists getData(string id)
        {
            stPlaylists obj = new stPlaylists();
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Select * From playlists Where id = @id", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@id", id);
                    using (MySqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.categoryID = Convert.ToString(dr["categoryID"]);
                            obj.api_id = Convert.ToString(dr["api_id"]);
                            obj.title = Convert.ToString(dr["title"]);
                            obj.description = Convert.ToString(dr["description"]);
                            obj.ordr = Convert.ToString(dr["ordr"]);
                        }
                    }
                }
            }
            return obj;
        }

        public static void add(stPlaylists obj)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Insert Into playlists ( categoryID, api_id, title, description, ordr ) Values( @categoryID, @api_id, @title, @description, @ordr)", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@categoryID", obj.categoryID);
                    cmd.Parameters.AddWithValue("@api_id", obj.api_id);
                    cmd.Parameters.AddWithValue("@title", obj.title);
                    cmd.Parameters.AddWithValue("@description", obj.description);
                    cmd.Parameters.AddWithValue("@ordr", obj.ordr);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void setOrder(string id, string ordr)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update playlists Set ordr=@ordr Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@ordr", ordr);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void update(stPlaylists obj)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update playlists Set  categoryID=@categoryID, api_id=@api_id, title=@title, description=@description, ordr=@ordr Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@categoryID", obj.categoryID);
                    cmd.Parameters.AddWithValue("@api_id", obj.api_id);
                    cmd.Parameters.AddWithValue("@title", obj.title);
                    cmd.Parameters.AddWithValue("@description", obj.description);
                    cmd.Parameters.AddWithValue("@ordr", obj.ordr);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void updateTitleAndDescription(string id, string title, string description)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update playlists Set title=@title, description=@description Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@title", title);
                    cmd.Parameters.AddWithValue("@description", description);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void delete(string id)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Delete From playlists Where id=@id", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", id);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }

    public class Themes 
    {
        public struct stTheme
        {
            public string id, channelID, category_bg_color, category_txt_one, category_txt_two, category_list_style, video_bg_color, video_txt_one, video_txt_two, video_list_style;
        }
        
        public static DataSet listThemeOptions(string channelID)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand("Select * From theme Where channelID = @channelID", conn))
                {
                    cmd.Parameters.AddWithValue("@channelID", channelID);

                    using (MySqlDataAdapter da = new MySqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public static stTheme GetChannelTheme(string channelID)
        {
            stTheme obj = new stTheme();
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Select * From theme Where channelID = @channelID", conn))
                {
                    conn.Open();
                    cmd.Parameters.AddWithValue("@channelID", channelID);
                    using (MySqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            obj.id = Convert.ToString(dr["id"]);
                            obj.channelID = Convert.ToString(dr["channelID"]);
                            obj.category_bg_color = Convert.ToString(dr["category_bg_color"]);
                            obj.category_txt_one = Convert.ToString(dr["category_txt_one"]);
                            obj.category_txt_two = Convert.ToString(dr["category_txt_two"]);
                            obj.category_list_style = Convert.ToString(dr["category_list_style"]);
                            obj.video_bg_color = Convert.ToString(dr["video_bg_color"]);
                            obj.video_txt_one = Convert.ToString(dr["video_txt_one"]);
                            obj.video_txt_two = Convert.ToString(dr["video_txt_two"]);
                            obj.video_list_style = Convert.ToString(dr["video_list_style"]);
                        }
                    }
                }
            }
            return obj;
        }

        public static void CreateChannelTheme(stTheme obj)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Insert Into theme ( channelID, category_bg_color, category_txt_one, category_txt_two, category_list_style, video_bg_color, video_txt_one, video_txt_two, video_list_style ) Values( @channelID, @category_bg_color, @category_txt_one, @category_txt_two, @category_list_style, @video_bg_color, @video_txt_one, @video_txt_two, @video_list_style)", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", obj.id);
                    cmd.Parameters.AddWithValue("@channelID", obj.channelID);
                    cmd.Parameters.AddWithValue("@category_bg_color", obj.category_bg_color);
                    cmd.Parameters.AddWithValue("@category_txt_one", obj.category_txt_one);
                    cmd.Parameters.AddWithValue("@category_txt_two", obj.category_txt_two);
                    cmd.Parameters.AddWithValue("@category_list_style", "flat-category");
                    cmd.Parameters.AddWithValue("@video_bg_color", obj.category_bg_color);
                    cmd.Parameters.AddWithValue("@video_txt_one", obj.category_txt_one);
                    cmd.Parameters.AddWithValue("@video_txt_two", obj.category_txt_two);
                    cmd.Parameters.AddWithValue("@video_list_style", "flat-category");
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            rbm.Log(DateTime.Now + "\tAction:" + "CreateChannelTheme" + "\tData:" + rbm.obj2json(obj));

        }

        public static void UpdateChannelTheme(stTheme obj)
        {
            using (MySqlConnection conn = new MySqlConnection(getConn()))
            {
                using (MySqlCommand cmd = new MySqlCommand("Update theme Set  category_bg_color=@category_bg_color, category_txt_one=@category_txt_one, category_txt_two=@category_txt_two, category_list_style=@category_list_style, video_bg_color=@video_bg_color, video_txt_one=@video_txt_one, video_txt_two=@video_txt_two, video_list_style=@video_list_style Where channelID=@channelID", conn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@channelID", obj.channelID);
                    cmd.Parameters.AddWithValue("@category_bg_color", obj.category_bg_color);
                    cmd.Parameters.AddWithValue("@category_txt_one", obj.category_txt_one);
                    cmd.Parameters.AddWithValue("@category_txt_two", obj.category_txt_two);
                    cmd.Parameters.AddWithValue("@category_list_style", "flat-category");
                    cmd.Parameters.AddWithValue("@video_bg_color", obj.category_bg_color);
                    cmd.Parameters.AddWithValue("@video_txt_one", obj.category_txt_one);
                    cmd.Parameters.AddWithValue("@video_txt_two", obj.category_txt_two);
                    cmd.Parameters.AddWithValue("@video_list_style", "flat-category");
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }

    public static void Log(string line)
    {
        string dom = HttpContext.Current.Request.Url.ToString();
        //if (dom.Contains("localhost")||dom.Contains("usss.com"))
        //    return;
		
		try
		{
            using (TextWriter tw = new StreamWriter(HttpContext.Current.Server.MapPath("/_log.txt"), true))
			{
				tw.WriteLine("" + line + "");
				tw.Close();
			}

            FileInfo f = new FileInfo(HttpContext.Current.Server.MapPath("/_log.txt"));
			if (f.Length > 50*1024*1024) // > 50 Mb
			{
                if (!Directory.Exists(HttpContext.Current.Server.MapPath("/_logs")))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("/_logs"));
				
				DateTime dt = DateTime.Now;
                File.Move(HttpContext.Current.Server.MapPath("/_log.txt"), HttpContext.Current.Server.MapPath("/_logs/log-" + dt.Month.ToString("D2") + dt.Day.ToString("D2") + dt.Year + "-" + dt.Hour.ToString("D2") + dt.Minute.ToString("D2") + ".txt"));
			}
		}
		catch (Exception)
		{ }

    }

    public class CustomTasks
    {
        public struct stFileAndPath
        {
            public string filename, path;
        }

        public static void CreateChannelImages(string channelID, string filename) 
        {
            string path = "/_channel-images/" + toStr(channelID) + "/" + filename;
            //uploaded best is 336x210
            //side icon sd = 108 x 69
            //side icon hd = 248 x 140
            //focus icon sd= 248 x 140
            float focus_hd = 1;
            float focus_sd = .72f;
            float side_sd = .32f;
            float side_hd = .72f;
            string ext = Path.GetExtension(HttpContext.Current.Server.MapPath(path));
            try
            {
                //get actual image dimensions
                System.Drawing.Image img =
                    System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path));
                var r = new ResizeSettings();
                double ratio = (double)img.Height / img.Width;
                r.Width = (int)Math.Round(focus_hd * img.Width) > 336 ? 336: (int)Math.Round(focus_hd * img.Width);
                r.Height = (int)Math.Round(focus_hd * img.Width) > 336 ? (int)(336*(img.Height/img.Width)): (int)Math.Round(focus_hd * img.Height);
                ImageBuilder.Current.Build(HttpContext.Current.Server.MapPath(path),
                    HttpContext.Current.Server.MapPath("/_channel-images/" + channelID + "/resized_focus_hd" + ext), r);
                rbm.Channel.FocusIconHD(channelID, "resized_focus_hd" + ext);
            }
            catch (Exception)
            {

            }
            //Create Focus Standard Definition
            try
            {
                //get actual image dimensions
                System.Drawing.Image img =
                    System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path));
                var r = new ResizeSettings();
                double ratio = (double)img.Height / img.Width;
                r.Width = (int)Math.Round(focus_sd * img.Width) > 248 ? 248 : (int)Math.Round(focus_sd * img.Width);
                r.Height = (int)Math.Round(focus_sd * img.Width) > 248 ? (int)(248*(img.Height/img.Width)) : (int)Math.Round(focus_sd * img.Height);
                ImageBuilder.Current.Build(HttpContext.Current.Server.MapPath(path),
                    HttpContext.Current.Server.MapPath("/_channel-images/" + channelID + "/resized_focus_sd" + ext), r);
                rbm.Channel.FocusIconSD(channelID, "resized_focus_sd" + ext);
            }
            catch (Exception)
            {

            }
            try
            {
                //get actual image dimensions
                System.Drawing.Image img =
                    System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path));
                var r = new ResizeSettings();
                double ratio = (double)img.Height / img.Width;

                r.Width = (int)Math.Round(side_sd * img.Width) > 108 ? 108 : (int)Math.Round(side_sd * img.Width);
                r.Height = (int)Math.Round(side_sd * img.Width) > 69 ? (int)(108*(img.Height/img.Width)) : (int)Math.Round(side_sd * img.Height);
                ImageBuilder.Current.Build(HttpContext.Current.Server.MapPath(path),
                    HttpContext.Current.Server.MapPath("/_channel-images/" + channelID + "/resized_side_sd" + ext), r);
                rbm.Channel.SideIconSD(channelID, "resized_side_sd" + ext);
            }
            catch (Exception)
            {

            }
            try
            {
                //get actual image dimensions
                System.Drawing.Image img =
                    System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path));
                var r = new ResizeSettings();
                double ratio = (double)img.Height / img.Width;

                r.Width = (int)Math.Round(side_hd * img.Width) > 248 ? 248 : (int)Math.Round(side_hd * img.Width);
                r.Height = (int)Math.Round(side_hd * img.Width) > 248 ? (int)(248*(img.Height/img.Width)) : (int)Math.Round(side_hd * img.Height);
                ImageBuilder.Current.Build(HttpContext.Current.Server.MapPath(path),
                    HttpContext.Current.Server.MapPath("/_channel-images/" + channelID + "/resized_side_hd" + ext), r);
                rbm.Channel.SideIconHD(channelID, "resized_side_hd" + ext);
            }
            catch (Exception)
            {

            }
        }
        public static void CreateSplashImages(string channelID, string filename) 
        { 
            //HD: 1280x720 (Keep in mind the action safe zone is1150X578)
            //SD: 720x480 (Action zone is 648x432)
            string path = "/_channel-images/" + toStr(channelID) + "/" + filename;
            float splash_hd = 1;
            float splash_sd = .6f;
            string ext = Path.GetExtension(HttpContext.Current.Server.MapPath(path));
            try
            {
                //get actual image dimensions
                System.Drawing.Image img =
                    System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path));
                var r = new ResizeSettings();
                double ratio = (double)img.Height / img.Width;

                r.Width = (int)Math.Round(splash_hd * img.Width) > 1150 ? 1150: (int)Math.Round(splash_hd * img.Width);
                r.Height = (int)Math.Round(splash_hd * img.Width) > 1150 ? (int)(1150* (img.Height / img.Width)) : (int)Math.Round(splash_hd * img.Height);
                ImageBuilder.Current.Build(HttpContext.Current.Server.MapPath(path),
                    HttpContext.Current.Server.MapPath("/_channel-images/" + channelID + "/resized_splash_hd" + ext), r);
                rbm.Channel.SplashHD(channelID, "resized_splash_hd" + ext);
            }
            catch (Exception)
            {

            }
            try
            {
                //get actual image dimensions
                System.Drawing.Image img =
                    System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path));
                var r = new ResizeSettings();
                double ratio = (double)img.Height / img.Width;

                r.Width = (int)Math.Round(splash_sd * img.Width) > 648 ? 648 : (int)Math.Round(splash_sd * img.Width);
                r.Height = (int)Math.Round(splash_sd * img.Width) > 648 ? (int)(648 * (img.Height / img.Width)) : (int)Math.Round(splash_sd * img.Height);
                ImageBuilder.Current.Build(HttpContext.Current.Server.MapPath(path),
                    HttpContext.Current.Server.MapPath("/_channel-images/" + channelID + "/resized_splash_sd" + ext), r);
                rbm.Channel.SplashSD(channelID, "resized_splash_sd" + ext);
            }
            catch (Exception)
            {

            }
        }
        public static void CreateCategoryImages(string categoryID, string filename)
        {
            //HD: 336 x 262
            //SD: 304 x 237
            //ratio best is 1.283
            var obj = rbm.Categories.getData(categoryID);
            string path = "/_category-images/" + obj.channelID + "/" + filename;

            float category_hd = 1;
            float category_sd = .9f;
            string ext = Path.GetExtension(HttpContext.Current.Server.MapPath(path));

            //Category HD
            try
            {
                //get actual image dimensions
                System.Drawing.Image img =
                    System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path));
                var r = new ResizeSettings();
                double ratio = (double)img.Height / img.Width;

                r.Width = (int)Math.Round(category_hd * img.Width) > 336 ? 336 : (int)Math.Round(category_hd * img.Width);
                r.Height = (int)Math.Round(category_hd * img.Width) > 336 ? (int)(336 * (img.Height / img.Width)) : (int)Math.Round(category_hd * img.Height);
                if (!Directory.Exists(HttpContext.Current.Server.MapPath("/_category-images/" + obj.channelID + "/" + categoryID + "/")))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("/_category-images/" + obj.channelID + "/" + categoryID + "/"));
                }
                ImageBuilder.Current.Build(HttpContext.Current.Server.MapPath(path),
                    HttpContext.Current.Server.MapPath("/_category-images/" + obj.channelID + "/" + categoryID + "/resized_category_hd" + ext), r);
                rbm.Categories.CategoryHD(categoryID, "resized_category_hd" + ext);
            }
            catch (Exception)
            {

            }
            //Category SD
            try
            {
                //get actual image dimensions
                System.Drawing.Image img =
                    System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path));
                var r = new ResizeSettings();
                double ratio = (double)img.Height / img.Width;

                r.Width = (int)Math.Round(category_sd * img.Width) > 304 ? 304 : (int)Math.Round(category_sd * img.Width);
                r.Height = (int)Math.Round(category_sd * img.Width) > 304 ? (int)(304 * (img.Height / img.Width)) : (int)Math.Round(category_sd * img.Height);
                if (!Directory.Exists(HttpContext.Current.Server.MapPath("/_category-images/" + obj.channelID + "/" + categoryID + "/")))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("/_category-images/" + obj.channelID + "/" + categoryID + "/"));
                }
                ImageBuilder.Current.Build(HttpContext.Current.Server.MapPath(path),
                    HttpContext.Current.Server.MapPath("/_category-images/" + obj.channelID + "/" + categoryID + "/resized_category_sd" + ext), r);
                rbm.Categories.CategorySD(categoryID, "resized_category_sd" + ext);
            }
            catch (Exception)
            {

            }
        }
        public static void CreateLogoImages(string channelID, string filename)
        {
            //HD: 236 x 96
            //SD: 174 x 64
            string path = "/_channel-images/" + channelID + "/" + filename;

            float logo_hd = 1;
            float logo_sd = .73f;
            string ext = Path.GetExtension(HttpContext.Current.Server.MapPath(path));

            //Category HD
            try
            {
                //get actual image dimensions
                System.Drawing.Image img =
                    System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path));
                var r = new ResizeSettings();
                double ratio = (double)img.Height / img.Width;
                r.Width = (int)Math.Round(logo_hd * img.Width) > 236 ? 236 : (int)Math.Round(logo_hd * img.Width);
                r.Height = (int)Math.Round(logo_hd * img.Width) > 236 ? (int)(236 * (img.Height / img.Width)) : (int)Math.Round(logo_hd * img.Height);
                ImageBuilder.Current.Build(HttpContext.Current.Server.MapPath(path),
                    HttpContext.Current.Server.MapPath("/_channel-images/" + channelID + "/resized_logo_hd" + ext), r);
                rbm.Channel.LogoHD(channelID, "resized_logo_hd" + ext);
            }
            catch (Exception)
            {

            }
            //Category SD
            try
            {
                //get actual image dimensions
                System.Drawing.Image img =
                    System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path));
                var r = new ResizeSettings();
                double ratio = (double)img.Height / img.Width;

                r.Width = (int)Math.Round(logo_sd * img.Width) > 174 ? 174 : (int)Math.Round(logo_sd * img.Width);
                r.Height = (int)Math.Round(logo_sd * img.Width) > 174 ? (int)(174 * (img.Height / img.Width)) : (int)Math.Round(logo_sd * img.Height);
                ImageBuilder.Current.Build(HttpContext.Current.Server.MapPath(path),
                    HttpContext.Current.Server.MapPath("/_channel-images/" + channelID + "/resized_logo_sd" + ext), r);
                rbm.Channel.LogoSD(channelID, "resized_logo_sd" + ext);
            }
            catch (Exception)
            {

            }
        }
        public static void CreateOverhangImages(string channelID, string filename)
        {
            //HD: 1280 x 165
            //SD: 720 x 110
            string path = "/_channel-images/" + channelID + "/" + filename;

            float overhang_hd = 1;
            float overhang_sd = .73f;
            string ext = Path.GetExtension(HttpContext.Current.Server.MapPath(path));

            //overhang_hd HD
            try
            {
                //get actual image dimensions
                System.Drawing.Image img =
                    System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path));
                var r = new ResizeSettings();
                double ratio = (double)img.Height / img.Width;
                r.Width = (int)Math.Round(overhang_hd * img.Width) > 1280 ? 1280 : (int)Math.Round(overhang_hd * img.Width);
                r.Height = (int)Math.Round(overhang_hd * img.Width) > 1280 ? (int)(1280 * (img.Height / img.Width)) : (int)Math.Round(overhang_hd * img.Height);
                ImageBuilder.Current.Build(HttpContext.Current.Server.MapPath(path),
                    HttpContext.Current.Server.MapPath("/_channel-images/" + channelID + "/resized_overhang_hd" + ext), r);
                rbm.Channel.OverhangHD(channelID, "resized_overhang_hd" + ext);
            }
            catch (Exception)
            {

            }
            //overhang_sd SD
            try
            {
                //get actual image dimensions
                System.Drawing.Image img =
                    System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(path));
                var r = new ResizeSettings();
                double ratio = (double)img.Height / img.Width;

                r.Width = (int)Math.Round(overhang_sd * img.Width) > 720 ? 720 : (int)Math.Round(overhang_sd * img.Width);
                r.Height = (int)Math.Round(overhang_sd * img.Width) > 720 ? (int)(720 * (img.Height / img.Width)) : (int)Math.Round(overhang_sd * img.Height);
                ImageBuilder.Current.Build(HttpContext.Current.Server.MapPath(path),
                    HttpContext.Current.Server.MapPath("/_channel-images/" + channelID + "/resized_overhang_sd" + ext), r);
                rbm.Channel.OverhangSD(channelID, "resized_overhang_sd" + ext);
            }
            catch (Exception)
            {

            }
        }


    }

    public class mail
    {
        public struct stEmail
        {
            public string from, fromName, replyTo, to, cc, bcc, subject, body, host, port, user, password, attachmentFilename;
            public bool isHTML;
        }

        public static Boolean send(stEmail email)
        {
            try
            {

                SmtpClient client = new SmtpClient
                {
                    Host = email.host,
                    Port = Convert.ToInt32(email.port),
                    Credentials = new NetworkCredential(email.user, email.password)
                };


                MailMessage message = new MailMessage();

                if (!string.IsNullOrEmpty(email.to))
                {
                    if (email.to.Contains(";"))
                    {
                        string[] arr = email.to.Split(';');
                        foreach (string mail in arr)
                        {
                            message.To.Add(mail);
                        }
                    }
                    else
                        message.To.Add(email.to);
                }

                if (!string.IsNullOrEmpty(email.cc))
                {
                    if (email.cc.Contains(";"))
                    {
                        string[] arr = email.cc.Split(';');
                        foreach (string mail in arr)
                        {
                            message.CC.Add(mail);
                        }
                    }
                    else
                        message.CC.Add(email.cc);
                }

                if (!string.IsNullOrEmpty(email.bcc))
                {
                    if (email.bcc.Contains(";"))
                    {
                        string[] arr = email.bcc.Split(';');
                        foreach (string mail in arr)
                        {
                            message.Bcc.Add(mail);
                        }
                    }
                    else
                        message.Bcc.Add(email.bcc);
                }
                if (email.attachmentFilename != null)
                {
                    string path = getSett("build_path") + email.attachmentFilename;
                    Attachment attachment = new Attachment(rbm.getPath(path), MediaTypeNames.Application.Octet);
                    ContentDisposition disposition = attachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime(rbm.getPath(path));
                    disposition.ModificationDate = File.GetLastWriteTime(rbm.getPath(path));
                    disposition.ReadDate = File.GetLastAccessTime(rbm.getPath(path));
                    disposition.FileName = Path.GetFileName(rbm.getPath(path));
                    disposition.Size = new FileInfo(rbm.getPath(path)).Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    message.Attachments.Add(attachment);
                }
                message.From = string.IsNullOrEmpty(email.fromName) ? new MailAddress(email.@from) : new MailAddress(email.@from, email.fromName);
                message.ReplyToList.Add(email.from);
                message.Subject = email.subject;
                message.IsBodyHtml = email.isHTML;
                message.Body = email.body;
                client.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                email.body = "Subject: " + email.subject + "<br>To: " + email.to + "<br>CC: " + email.cc + "<br>BCC: " + email.bcc + "<br><br>Error: " + ex.Message + "<br><br>" + ex.StackTrace;
                email.to = "michael@rightbrainmedia.com";
                email.cc = email.bcc = "";
                email.subject = "Error on mail sending";

                send(email);

                return false;
            }
        }

    }





}