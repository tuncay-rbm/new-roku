﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;

public partial class rbm
{
    public class ooyala
    {
        public class Meta
        {
            public string video_codec { get; set; }
            public object video_height { get; set; }
            public object video_width { get; set; }
            public object video_bitrate { get; set; }
            public object video_framerate { get; set; }
            public string video_ratio { get; set; }
            public string audio_codec { get; set; }
            public object audio_bitrate { get; set; }
            public object audio_samplerate { get; set; }
            public string audio_channels { get; set; }
            public object author { get; set; }
            public string duration { get; set; }
            public string description { get; set; }
        }

        public class Playlist
        {
            public string title { get; set; }
            public string permalink { get; set; }
            public string filename { get; set; }
            public string image { get; set; }
            public string hls { get; set; }
            public object rtsp { get; set; }
            public object rtmp { get; set; }
            public Meta meta { get; set; }
        }

        public class VideoList
        {
            public object width { get; set; }
            public object height { get; set; }
            public string title { get; set; }
            public string link { get; set; }
            public List<Playlist> playlist { get; set; }
        }
        public static string getVideoFeed(string subCatID)
        {
            StringBuilder xml = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + Environment.NewLine + "<feed>" + Environment.NewLine);
            if (1==1)
            {
                List<ooyala.Playlist> playlist = getOoyalaPlaylist();
                if (playlist != null)
                {
                    xml.Append("\t<resultLength>" + playlist.Count + "</resultLength>" + Environment.NewLine);
                    xml.Append("\t<endIndex>" + playlist.Count + "</endIndex>" + Environment.NewLine);

                    foreach (ooyala.Playlist asset in playlist)
                    {
                        //asset.meta.duration = milliSecondToSecond(asset.meta.duration);
                        xml.Append("\t<item sdImg=\"" + asset.image.Replace("https", "http") + "\" hdImg=\"" + asset.image.Replace("https", "http") + "\">" + Environment.NewLine);
                        xml.Append("\t\t<title>" + asset.title + "</title>" + Environment.NewLine);
                        xml.Append("\t\t<contentId>1</contentId>" + Environment.NewLine);
                        xml.Append("\t\t<contentType>episode</contentType>" + Environment.NewLine);
                        xml.Append("\t\t<contentQuality>SD</contentQuality>" + Environment.NewLine);
                        xml.Append("\t\t\t<streamFormat>hls</streamFormat>" + Environment.NewLine);
                        xml.Append("\t\t<media>" + Environment.NewLine);
                        xml.Append("\t\t\t<streamQuality>SD</streamQuality>" + Environment.NewLine);
                        xml.Append("\t\t\t<streamBitrate>" + asset.meta.video_bitrate + "</streamBitrate>" + Environment.NewLine);
                        xml.Append("\t\t\t<streamUrl>" + Environment.NewLine);
                        xml.Append(asset.hls + Environment.NewLine);
                        //live stream test...
                        //xml.Append("http://cdn-dev-live-egress.rbmtv.com/rbmv5-live-20/_definst_/mp4:rokutesting/playlist.m3u8" + Environment.NewLine);
                        xml.Append("</streamUrl>" + Environment.NewLine);
                        xml.Append("\t\t</media>" + Environment.NewLine);
                        xml.Append("\t\t<synopsis>" + Environment.NewLine);
                        xml.Append("\t\t\t" + asset.meta.description + Environment.NewLine);
                        xml.Append("\t\t</synopsis>" + Environment.NewLine);
                        xml.Append("\t\t<genres></genres>" + Environment.NewLine);
                        xml.Append("\t\t<runtime>" + asset.meta.duration + "</runtime>" + Environment.NewLine);
                        xml.Append("\t</item>" + Environment.NewLine);
                    }
                    xml.Append("</feed>");
                }
                else
                {
                    xml.Append("\t<resultLength>0</resultLength>" + Environment.NewLine);
                    xml.Append("\t<endIndex>0</endIndex>" + Environment.NewLine);
                    xml.Append("</feed>");

                }
                return xml.ToString();
            }

        }
        public static List<ooyala.Playlist> getOoyalaPlaylist()
        {
            try
            {
                //string url = "http://cdn-api.ooyala.com/v2/syndications/74e6a9863f564d0f842294113ff308f1/feed?pcode=VxYTUyOsSlK3TbT8VPRF4WAsNcF2";
                //WebRequest request = WebRequest.Create(url);

                //HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //Stream dataStream = response.GetResponseStream();
               // StreamReader reader = new StreamReader(dataStream);
                string json = "";
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("/ooyalaFeed.txt")))
                {
                    json = reader.ReadToEnd();
 
                }
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                ooyala.VideoList obj = (ooyala.VideoList)Newtonsoft.Json.JsonConvert.DeserializeObject(json, typeof(ooyala.VideoList));
                return obj.playlist;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
	}
}