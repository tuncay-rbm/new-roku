﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using RBMLib;
using Stripe;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for auth
/// </summary>
public partial class rbm
{
    public class API
    {
        public struct stSignup
        {
            public string firstname, lastname, email, username, password, passwordconfirm, errMsg, packageID, cardNumber, cardCVC, cardExp, apiAuthToken, apiUserID, responseCode;
            public bool isValid;
            public List<string> errors { get; set; }
        }

        public struct stAPIErrors
        {
            public List<string> validation_errors { get; set; }
        }

        public struct stAPIError
        {
            public int code { get; set; }
            public stAPIErrors error { get; set; }
            public string message_code { get; set; }
        }

        public struct stAuthResponse
        {
            public stResponse response { get; set; }
        }

        public struct stResponse
        {
            public string token { get; set; }
        }

        public class EncodeProfile 
        {
            public struct stProfiles
            {
                public string id, memberID, api_profile_id, title, api_encode_group_id;
            }
            public struct stProfileGroup
            {
                public string id, memberID, api_group_id, title;
            }

            public static void add(stProfiles obj)
            {
                using (MySqlConnection conn = new MySqlConnection(getConn()))
                {
                    using (MySqlCommand cmd = new MySqlCommand("Insert Into profiles ( memberID, api_profile_id, title ) Values( @memberID, @api_profile_id, @title)", conn))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@memberID", obj.memberID);
                        cmd.Parameters.AddWithValue("@api_profile_id", obj.api_profile_id);
                        cmd.Parameters.AddWithValue("@title", obj.title);
                        conn.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                rbm.Log(DateTime.Now + "\tAction:" + "EncodeProfile.add" + "\tData:" + rbm.obj2json(obj));

            }
            public static void addProfileGroup(stProfileGroup obj)
            {
                using (MySqlConnection conn = new MySqlConnection(getConn()))
                {
                    using (MySqlCommand cmd = new MySqlCommand("Insert Into profile_groups ( memberID, api_group_id, title ) Values( @memberID, @api_group_id, @title)", conn))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@memberID", obj.memberID);
                        cmd.Parameters.AddWithValue("@api_group_id", obj.api_group_id);
                        cmd.Parameters.AddWithValue("@title", obj.title);
                        conn.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
                rbm.Log(DateTime.Now + "\tAction:" + "addProfileGroup" + "\tData:" + rbm.obj2json(obj));

            }
            //NOTE: the uploader is only allowing the Standard Roku encode
            public static string getProfileGroupID(string memberID)
            {
                string groupID = "";
                using (MySqlConnection conn = new MySqlConnection(getConn()))
                {
                    using (MySqlCommand cmd = new MySqlCommand("select api_profile_id from profiles Where memberID = @memberID and title = 'Standard'", conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@memberID", memberID);
                        using (MySqlDataReader dr = cmd.ExecuteReader())
                        {
                            if (dr.Read())
                            {
                                groupID = Convert.ToString(dr["api_profile_id"]);

                            }
                        }
                    }
                }
                return groupID;
            }
            //NOTE : the following is the ideal query, if V5 supported .smil in playlist
            //public static string getProfileGroupID()
            //{
            //    string memberID = getMemberID();
            //    string groupID = "";
            //    using (MySqlConnection conn = new MySqlConnection(getConn()))
            //    {
            //        using (MySqlCommand cmd = new MySqlCommand("Select api_group_id From profile_groups Where memberID = @memberID ", conn))
            //        {
            //            conn.Open();
            //            cmd.Parameters.AddWithValue("@memberID", memberID);
            //            using (MySqlDataReader dr = cmd.ExecuteReader())
            //            {
            //                if (dr.Read())
            //                {
            //                    groupID = Convert.ToString(dr["api_group_id"]);

            //                }
            //            }
            //        }
            //    }
            //    return groupID;
            //}
            public static List<string> getUserProfiles(string userID)
            {
                string memberID = userID;
                string profileID = "";
                List<string> lst = new List<string>();
                using (MySqlConnection conn = new MySqlConnection(getConn()))
                {
                    using (MySqlCommand cmd = new MySqlCommand("Select api_profile_id From profiles Where memberID = @memberID ", conn))
                    {
                        conn.Open();
                        cmd.Parameters.AddWithValue("@memberID", memberID);
                        cmd.Parameters.AddWithValue("@title", "Standard");

                        using (MySqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                profileID = Convert.ToString(dr["api_profile_id"]);
                                lst.Add(profileID);
                            }
                        }
                    }
                }

                return lst;
            }


            public class Encode_Profile
            {
                public int id { get; set; }
                public int user_id { get; set; }
                public string ec_profile_id { get; set; }
                public double name { get; set; }
                public string title { get; set; }
                public string extension { get; set; }
                public int video_width { get; set; }
                public int video_height { get; set; }
                public int video_bitrate { get; set; }
                public int video_framerate { get; set; }
                public int audio_bitrate { get; set; }
                public int audio_samplerate { get; set; }
                public int audio_channels { get; set; }
                public string video_aspect { get; set; }
                public int keyframe_interval { get; set; }
                public int upscale { get; set; }
                public int two_pass { get; set; }
                public string type { get; set; }
                public object order { get; set; }
                public string format { get; set; }
                public int @default { get; set; }
                public int default_ftp { get; set; }
                public string preset_name { get; set; }
                public object watermark_file { get; set; }
                public string watermark_position { get; set; }
                public object trim_in { get; set; }
                public object trim_out { get; set; }
                public object deleted_at { get; set; }
                public string created_at { get; set; }
                public string updated_at { get; set; }
            }

            public class Response
            {
                public string added { get; set; }
                public List<Encode_Profile> profiles { get; set; }
            }

            public class ProfileResponse
            {
                public int code { get; set; }
                public Response response { get; set; }
                public string message_code { get; set; }
            }
            public static void CreateRokuLow(string userID) 
            {
                string response = "";
                string token = rbm.getToken();
                string url = rbm.getSett("createEncodeProfile");
                try
                {
                    using (WebClient client = new WebClient())
                    {
                        response = Encoding.Default.GetString(client.UploadValues(url + token, "POST",
                            new NameValueCollection
                              {
                                {"title", "Roku Low"},
                                {"extension", ".mp4"},
                                {"video_width", "480"},
                                {"video_height", "320"},
                                {"video_bitrate", "996"},
                                {"video_framerate", "30"},
                                {"audio_bitrate", "128"},
                                {"audio_samplerate", "44100"},
                                {"audio_channels", "2"},
                                {"video_aspect", "preserve"},
                                {"keyframe_interval", "250"},
                                {"upscale", "1"},
                                {"two_pass", "0"},
                                {"type", "video"},
                                {"default", "0"},
                                {"default_ftp", "0"},
                                {"preset_name", "h264"}
                              }
                        ));
                        if (!isNull(response)) {
                            ProfileResponse obj = (ProfileResponse)JsonConvert.DeserializeObject(response, typeof(ProfileResponse));
                            rbm.Log(DateTime.Now + "\tAction:" + "CreateRokuLow" + "\tData:" + rbm.obj2json(obj));

                            var profile = new stProfiles();
                            profile.api_profile_id = toStr(obj.response.profiles[0].id);
                            profile.memberID = userID;
                            profile.title = "Standard";
                            profile.api_encode_group_id = getProfileGroupID(userID);

                            add(profile);                            
                        }
                    }

                }
                catch (Exception wex)
                {
                    rbm.Log(DateTime.Now + "\tAction:" + "CreateRokuLow" + "\tData:" + wex.ToString());


                }
            }
            public static void CreateRokuMedium(string userID)
            {
                string response = ""; 

                string token = rbm.getToken();
                string url = rbm.getSett("createEncodeProfile");
                try
                {
                    using (WebClient client = new WebClient())
                    {
                        response = Encoding.Default.GetString(client.UploadValues(url + token, "POST",
                            new NameValueCollection
                              {
                                {"title", "Roku Medium"},
                                {"extension", ".mp4"},
                                {"video_width", "720"},
                                {"video_height", "480"},
                                {"video_bitrate", "1200"},
                                {"video_framerate", "30"},
                                {"audio_bitrate", "128"},
                                {"audio_samplerate", "44100"},
                                {"audio_channels", "2"},
                                {"video_aspect", "preserve"},
                                {"keyframe_interval", "250"},
                                {"upscale", "1"},
                                {"two_pass", "0"},
                                {"type", "video"},
                                {"default", "0"},
                                {"default_ftp", "0"},
                                {"preset_name", "h264"}
                              }
                        ));
                        if (!isNull(response))
                        {
                            ProfileResponse obj = (ProfileResponse)JsonConvert.DeserializeObject(response, typeof(ProfileResponse));
                            rbm.Log(DateTime.Now + "\tAction:" + "CreateRokuMedium" + "\tData:" + rbm.obj2json(obj));

                            var profile = new stProfiles();
                            profile.api_profile_id = toStr(obj.response.profiles[0].id);
                            profile.memberID = userID;
                            profile.title = "Better";
                            profile.api_encode_group_id = getProfileGroupID(userID);
                            add(profile);
                        }
                    }
                }
                catch (Exception wex)
                {
                    rbm.Log(DateTime.Now + "\tAction:" + "CreateRokuMedium" + "\tData:" + wex.ToString());

                }
            }
            public static void CreateRokuHigh(string userID)
            {
                string response = "";
                string token = rbm.getToken();
                string url = rbm.getSett("createEncodeProfile");
                try
                {
                    using (WebClient client = new WebClient())
                    {
                        response = Encoding.Default.GetString(client.UploadValues(url + token, "POST",
                            new NameValueCollection
                              {
                                {"title", "Roku High"},
                                {"extension", ".mp4"},
                                {"video_width", "1280"},
                                {"video_height", "720"},
                                {"video_bitrate", "2400"},
                                {"video_framerate", "30"},
                                {"audio_bitrate", "128"},
                                {"audio_samplerate", "44100"},
                                {"audio_channels", "2"},
                                {"video_aspect", "preserve"},
                                {"keyframe_interval", "250"},
                                {"upscale", "1"},
                                {"two_pass", "0"},
                                {"type", "video"},
                                {"default", "0"},
                                {"default_ftp", "0"},
                                {"preset_name", "h264"}
                              }
                        ));
                        if (!isNull(response))
                        {
                            ProfileResponse obj = (ProfileResponse)JsonConvert.DeserializeObject(response, typeof(ProfileResponse));
                            rbm.Log(DateTime.Now + "\tAction:" + "CreateRokuHigh" + "\tData:" + rbm.obj2json(obj));

                            var profile = new stProfiles();
                            profile.api_profile_id = toStr(obj.response.profiles[0].id);
                            profile.memberID = userID;
                            profile.title = "Best";
                            profile.api_encode_group_id = getProfileGroupID(userID);

                            add(profile);
                        }
                    }
                }
                catch (Exception wex)
                {
                    rbm.Log(DateTime.Now + "\tAction:" + "CreateRokuHigh" + "\tData:" + wex.ToString());

                }
            }

            public static void CreateRokuGroup(string userID) {
                Random random = new Random();
                int randomNumber = random.Next(0, 1000);
                string response = "";
                string token = rbm.getToken();
                string url = rbm.getSett("encoding_groups");
                try
                {
                    using (WebClient client = new WebClient())
                    {
                        response = Encoding.Default.GetString(client.UploadValues(url + token, "POST",
                            new NameValueCollection
                              {
                                  {"name","Roku - Standard_" + Convert.ToString(randomNumber)},
                                  {"abr", "1"}
                              }
                        ));
                        if (!isNull(response))
                        {
                            dynamic obj = JsonConvert.DeserializeObject(response);
                            rbm.Log(DateTime.Now + "\tAction:" + "CreateRokuGroup" + "\tData:" + lib.obj2json(obj));
                            var group = new stProfileGroup();
                            group.api_group_id = obj.response.encoding_groups[0].id;
                            group.memberID = userID;
                            group.title = "Roku - Standard_" + Convert.ToString(randomNumber);
                            addProfileGroup(group);
                        }
                    }
                }
                catch (Exception wex)
                {
                    rbm.Log(DateTime.Now + "\tAction:" + "CreateRokuGroup" + "\tData:" + wex.ToString());
                }
            }


            public static void PushProfilesToGroup(string userID) 
            {
                string response = "";
                string token = rbm.getToken();
                List<string> lst = getUserProfiles(userID);
                string low = lst[0].ToString();
                string med = lst[1].ToString();
                string high = lst[2].ToString();
                string url = rbm.getSett("push_profiles") + "/" + getProfileGroupID(userID) + "/profiles?token=" + token + "&profile_ids=[" + low + "," + med + "," + high + "]";
                try
                {
                    using (WebClient client = new WebClient())
                    {
                        response = Encoding.Default.GetString(client.UploadValues(url, "PUT",
                            new NameValueCollection
                            {

                            }
                        ));
                        if (!isNull(response))
                        {

                            dynamic obj = JsonConvert.DeserializeObject(response);
                            rbm.Log(DateTime.Now + "\tAction:" + "PushProfilesToGroup" + "\tData:" + rbm.obj2json(obj));

                            //var group = new stProfileGroup();
                            //group.api_group_id = obj.response.encoding_groups[0].id;
                            //group.memberID = rbm.getMemberID();
                            //group.title = "Roku-Standard";
                            //addProfileGroup(group);
                        }
                    }
                }
                catch (Exception wex)
                {

                    rbm.Log(DateTime.Now + "\tAction:" + "PushProfilesToGroup" + "\tData:" + wex.ToString());

                    //rbm.Log.add("v5 - Push Profiles to Group", wex.ToString(), rbm.toStr(HttpContext.Current.Request.UrlReferrer), rbm.toStr(HttpContext.Current.Request.UserAgent));
                }
            }
        }
        public static string UpdateRbmUser(string token, string id, string firstName, string lastName, string permissions)
        {
            string json;
            string url = rbm.getSett("update_user") + id + "?token=" + token + "&first_name=" + firstName + "&last_name=" + lastName + "&permissions=" + permissions;

            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "PUT";
                using (WebResponse response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        json = reader.ReadToEnd(); // do something fun...
                    }
                }
            }
            catch (Exception ex)
            {
                json = ex.ToString();

            }
            rbm.Log(DateTime.Now + "\tAction:" + "UpdateRbmUser" + "\tData:" + json);

            return json;
        }
        public static string ResetPassword(string email)
        {
            var obj = rbm.Members.getMemberByEmail(email);
            string url = rbm.getSett("reset_password") + "?email=" + email + "&username=" + obj.username;
            string json;
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "POST";
                using (WebResponse response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        json = reader.ReadToEnd(); // do something fun...
                    }
                }

            }
            catch (Exception ex)
            {
                json = ex.ToString();

            }
            rbm.Log(DateTime.Now + "\tAction:" + "ResetPassword" + "\tData:" + json);

            return json;
        }
        public static string UpdatePassword(string email, string hash, string password)
        {
            string json = "";
            var obj = rbm.Members.getMemberByEmail(email);
            if (!isNull(obj)) 
            {
                string url = rbm.getSett("save_new_password") + "?email=" + email + "&hash=" + hash + "&password=" + password + "&username=" + obj.username;
                try
                {
                    WebRequest request = WebRequest.Create(url);
                    request.Method = "POST";
                    using (WebResponse response = request.GetResponse())
                    {
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            json = reader.ReadToEnd(); // do something fun...
                        }
                    }
                }
                catch (Exception ex)
                {
                    json = ex.Message.ToString();
                }
                rbm.Log(DateTime.Now + "\tAction:" + "UpdatePassword" + "\tData:" + json);
            }
            return json;
        }
        public static stSignup ValidateSignupFields(stSignup obj)
        {
            obj.isValid = true;
            obj.errMsg = "Success";
            if (obj.firstname == "" || rbm.isNull(obj.firstname))
            {
                obj.isValid = false;
                obj.errMsg = "firstname is required";

            }
            if (obj.lastname == "" || rbm.isNull(obj.lastname))
            {
                obj.isValid = false;
                obj.errMsg = "lastname is required";

            }
            if (obj.email == "" || rbm.isNull(obj.email))
            {
                obj.isValid = false;
                obj.errMsg = "email is required";

            }
            if (obj.username == "" || rbm.isNull(obj.username))
            {
                obj.isValid = false;
                obj.errMsg = "username is required";

            }
            if (obj.password == "" || rbm.isNull(obj.password))
            {
                obj.isValid = false;
                obj.errMsg = "password is required";

            }
            if (obj.passwordconfirm == "" || rbm.isNull(obj.passwordconfirm))
            {
                obj.isValid = false;
                obj.errMsg = "passwordconfirm is required";

            }
            if (obj.password.ToString().Length < 9)
            {
                obj.isValid = false;
                obj.errMsg = "Password must be at least 9 characters in length";
            }
            if (obj.username.ToString().Length < 9)
            {
                obj.isValid = false;
                obj.errMsg = "Username must be at least 9 characters in length";
            }
            if (obj.password != obj.passwordconfirm)
            {
                obj.isValid = false;
                obj.errMsg = "The password fields must match";
            }
            if (!ValidEmail(obj.email))
            {
                obj.isValid = false;
                obj.errMsg = "Valid email is required";
            }
            //
            return obj;
        }
        public static bool ValidEmail(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        public static stSignup AlreadyExists(stSignup obj)
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    string newAccountUrl = RBMLib.lib.getSett("rbmtvNewAccount");
                    string _debug = Encoding.Default.GetString(client.UploadValues(newAccountUrl, "POST",
                     new NameValueCollection
                          {
                             { "username", obj.username },
                             { "password", obj.password  },
                             { "password_confirmation", obj.passwordconfirm },
                             { "email", obj.email  }
                          }
                    ));
                    obj.isValid = true;
                    obj.errMsg = _debug;
                }
                catch (WebException wex)
                {
                    obj.responseCode = wex.Status.ToString();
                    obj.isValid = false;
                    string retn = string.Empty;
                    if (wex.Response != null)
                    {
                        using (var errorResponse = (HttpWebResponse)wex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {
                                string json = reader.ReadToEnd();
                                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                                try
                                {
                                    StringBuilder str = new StringBuilder();
                                    API.stAPIError stErr = (API.stAPIError)Newtonsoft.Json.JsonConvert.DeserializeObject(json, typeof(API.stAPIError));
                                    API.stAPIErrors erObj = stErr.error;
                                    str.Append(wex.Response.ToString());
                                    foreach (var err in erObj.validation_errors)
                                    {
                                        str.Append(";" + err.ToString());
                                    }
                                    obj.errMsg = rbm.toStr(str);
                                }
                                catch (Exception)
                                {
                                    obj.errMsg = json;
                                }
                            }
                        }
                    }
                }
            }
            var lst = new List<string>();
            if (!obj.isValid)
            {
                string[] messages = rbm.toStr(obj.errMsg).Split(';');
                foreach (var msg in messages)
                {
                    if (msg == "The email has already been taken.") {
                        lst.Add(msg);                    
                    }
                }
                obj.errors = lst;
            }
            else
            {
                lst = null;
            }
            rbm.Log(DateTime.Now + "\tAction:" + "AlreadyExists" + "\tData:" + lib.obj2json(obj));

            return obj;
        }
        public static string UpdateCreditCard(string api_userID, string authToken, string planID, string cardNumber, string cardCVC, string cardEXP)
        {
            string billing = ConfigurationManager.AppSettings["billing"];
            string[] expiration = cardEXP.Split('/');
            var token = new StripeTokenCreateOptions();
            token.CardCvc = cardCVC;
            token.CardExpirationMonth = expiration[0].Trim();
            token.CardExpirationYear = expiration[1].Trim();
            token.CardNumber = cardNumber;
            var token_service = new StripeTokenService(ConfigurationManager.AppSettings["rbmKey"]);
            StripeToken stripe_token = token_service.Create(token);
            using (WebClient client = new WebClient())
            {
                try
                {
                    string url = billing.Replace("[USERID]", api_userID);
                    string _debug = Encoding.Default.GetString(client.UploadValues(url, "POST",
                     new NameValueCollection
                          {
                             { "stripe_token", stripe_token.Id },
                             { "plan_id", planID  },
                             { "token", authToken }
                                        }
                    ));
                    rbm.Log(DateTime.Now + "\tAction:" + "UpdateCreditCard" + "\tData:" + _debug);

                    return _debug;
                }
                catch (WebException wex)
                {
                    string json = "";
                    if (wex.Response != null)
                    {
                        rbm.Log(DateTime.Now + "\tAction:" + "UpdateCreditCard" + "\tData:" + wex.Response.ToString());

                        using (var errorResponse = (HttpWebResponse)wex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {
                                json = reader.ReadToEnd();
                                try
                                {
                                    return json;
                                }
                                catch (Exception)
                                {
                                    return wex.ToString();
                                }
                            }
                        }

                    }

                    return json;
                }
            }
        }
        public static stSignup CreateUser(stSignup obj)
        {
            string[] expiration = obj.cardExp.Split('/');
            var token = new StripeTokenCreateOptions();
            token.CardCvc = obj.cardCVC;
            token.CardExpirationMonth = expiration[0].Trim();
            token.CardExpirationYear = expiration[1].Trim();
            token.CardNumber = obj.cardNumber;
            var token_service = new StripeTokenService(RBMLib.lib.getSett("rbmKey"));
            StripeToken stripe_token = token_service.Create(token);
            //make call to rbm
            rbm.Log(DateTime.Now + "\tAction:" + "Stripe Token" + "\tData:" + rbm.obj2json(stripe_token));

            using (WebClient client = new WebClient())
            {
                try
                {
                    string newAccountUrl = RBMLib.lib.getSett("rbmtvNewAccount");
                    string _debug = Encoding.Default.GetString(client.UploadValues(newAccountUrl, "POST",
                     new NameValueCollection
                          {
                             { "username", obj.username },
                             { "password", obj.password  },
                             { "password_confirmation", obj.passwordconfirm },
                             { "email", obj.email  },
                             { "first_name", obj.firstname },
                             { "last_name", obj.lastname },
                             { "plan_id", obj.packageID },
                             { "stripe_token", stripe_token.Id},
                             { "saas_type", "roku"}
                          }
                    ));
                    obj.isValid = true;
                    obj.errMsg = _debug;

                }
                catch (WebException wex)
                {
                    obj.responseCode = wex.Status.ToString();
                    obj.isValid = false;
                    string retn = string.Empty;
                    if (wex.Response != null)
                    {
                        rbm.Log(DateTime.Now + "\tAction:" + "CreateUser" + "\tData:" + wex.Response.ToString());

                        using (var errorResponse = (HttpWebResponse)wex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {
                                string json = reader.ReadToEnd();
                                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                                try
                                {
                                    StringBuilder str = new StringBuilder();
                                    API.stAPIError stErr = (API.stAPIError)Newtonsoft.Json.JsonConvert.DeserializeObject(json, typeof(API.stAPIError));
                                    API.stAPIErrors erObj = stErr.error;
                                    str.Append(wex.Response.ToString());

                                    foreach (var err in erObj.validation_errors)
                                    {
                                        str.Append(";" + err.ToString());
                                    }

                                    obj.errMsg = rbm.toStr(str);
                                }
                                catch (Exception)
                                {
                                    obj.errMsg = json;
                                }
                            }
                        }
                    }
                }
            }
            var lst = new List<string>();
            if (!obj.isValid)
            {
                string[] messages = rbm.toStr(obj.errMsg).Split(';');
                foreach (var msg in messages)
                {
                    lst.Add(msg);
                }
                obj.errors = lst;
            }
            else
            {
                lst = null;
            }
            rbm.Log(DateTime.Now + "\tAction:" + "Create RBM User" + "\tData:" + rbm.obj2json(obj));
            return obj;
        }
        public static stSignup AuthenticateUser(stSignup obj)
        {
            string chkDate = DateTime.Today.ToString("yyy-MM-dd");
            string chk = rbm.MD5("rbm-acl-" + chkDate);
            string email = obj.email;
            string password = obj.password;
            string authToken = obj.apiAuthToken;
            //authorize the rbmtv account
            try
            {
                WebRequest authRequest = WebRequest.Create(rbm.getSett("authRequest"));
                String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(email + ":" + password));
                authRequest.Headers.Add("Authorization", "Basic " + encoded);
                authRequest.Method = "GET";
                HttpWebResponse authResponse = (HttpWebResponse)authRequest.GetResponse();
                Stream authStream = authResponse.GetResponseStream();
                StreamReader authReader = new StreamReader(authStream);
                string authJson = authReader.ReadToEnd();
                obj.errMsg = authJson;
                dynamic d = JsonConvert.DeserializeObject(authJson);
                obj.isValid = true;
                obj.apiAuthToken = d.response.token;
            }
            catch (Exception ex1)
            {
                obj.isValid = false;
                obj.errMsg = ex1.ToString();

            }
            rbm.Log(DateTime.Now + "\tAction:" + "AuthenticateUser" + "\tData:" + rbm.obj2json(obj));

            return obj;
        }
        public static stSignup AccountDetails(stSignup obj)
        {
            string json;
            string url = rbm.getSett("account_details") + "?token=" + obj.apiAuthToken;
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "GET";
                using (WebResponse response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        json = reader.ReadToEnd(); // do something fun...
                    }
                }
                dynamic d = JsonConvert.DeserializeObject(json);
                obj.apiUserID = d.response.user.id;
                obj.isValid = true;
            }
            catch (Exception ex)
            {
                obj.isValid = false;
                obj.errMsg = ex.ToString();

            }
            rbm.Log(DateTime.Now + "\tAction:" + "AccountDetails" + "\tData:" + rbm.obj2json(obj));

            return obj;
        }
        public static string get_rbm_user(string token)
        {
            string json;
            string url = rbm.getSett("account_details") + "?token=" + token;
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "GET";
                using (WebResponse response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        json = reader.ReadToEnd(); // do something fun...
                    }
                }
                return toStr(json);
            }
            catch (Exception ex)
            {
                json = ex.ToString();
                return json;
            }
        }
        public static string Encoded_Assets(string token, string method)
        {
            string json;
            string url = rbm.getSett("encoded_assets");
            switch (method)
            {
                case "GET":
                    url = url + "?token=" + token;
                    break;
            }
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = method;
                using (WebResponse response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        json = reader.ReadToEnd(); // do something fun...
                    }
                }
            }
            catch (Exception ex)
            {
                json = ex.ToString();
            }
            return json;
        }
        public static string Update_Encoded_Asset( string token, string title, string assetID, string description)
        {
            string json;
            string url = rbm.getSett("encoded_assets");

            url = url + "/" + assetID + "?token=" + token + "&title=" + title + "&description=" + description;
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "PUT";
                using (WebResponse response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        json = reader.ReadToEnd(); // do something fun...
                    }
                }
            }
            catch (Exception ex)
            {
                json = ex.ToString();
            }
            rbm.Log(DateTime.Now + "\tAction:" + "Update_Encoded_Asset" + "\tData:" + json);

            return json;
        }
        public static string Single_Encoded_Asset(string token, string assetID)
        {
            string json;
            string url = rbm.getSett("encoded_assets");
            url = url + "/" + assetID +  "?token=" + token;
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "GET";
                using (WebResponse response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        json = reader.ReadToEnd(); // do something fun...
                    }
                }
            }
            catch (Exception ex)
            {
                json = ex.ToString();
            }
            rbm.Log(DateTime.Now + "\tAction:" + "Single_Encoded_Asset" + "\tData:" + json);

            return json;
        }
        public static string Delete_Original_Asset(string id, string token)
        {
            string json;
            string url = rbm.getSett("original_assets");
            url = url + "/" + id + "?token=" + token;
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "DELETE";
                using (WebResponse response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        json = reader.ReadToEnd(); // do something fun...
                    }
                }
            }
            catch (Exception ex)
            {
                json = ex.ToString();
            }
            rbm.Log(DateTime.Now + "\tAction:" + "Delete_Original_Asset" + "\tData:" + json);

            return json;
        }

        public static string Original_Assets(string token, string method) 
        {
            string json;
            string url = rbm.getSett("original_assets");
            switch(method){
                case "GET":
                    url = url + "?token=" + token;
                    break;

            }
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = method;
                using (WebResponse response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        json = reader.ReadToEnd(); // do something fun...
                    }
                }
            }
            catch (Exception ex)
            {
                json = ex.ToString();
            }
            rbm.Log(DateTime.Now + "\tAction:" + "Original_Assets" + "\tData:" + json);

            return json;
        }

        public class Playlists
        {
            public class PlaylistResponse
            {
                public int code { get; set; }
                public string response { get; set; }
                public string message_code { get; set; }
            }
            public static string GetPlaylists(string token, string method, string id = "")
            {

                string json = "";
                string url = rbm.getSett("playlists");
                switch (method)
                {
                    case "GET":
                        url = url + "?token=" + token;
                        try
                        {
                            WebRequest request = WebRequest.Create(url);
                            request.Method = method;
                            using (WebResponse response = request.GetResponse())
                            {
                                using (var reader = new StreamReader(response.GetResponseStream()))
                                {
                                    json = reader.ReadToEnd(); // do something fun...
                                }
                            }
                        }
                        catch (WebException ex)
                        {
                            if (ex.Response != null)
                            {
                                using (var errorResponse = (HttpWebResponse)ex.Response)
                                {
                                    using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                                    {
                                        string stuff = reader.ReadToEnd();
                                        var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                                        try
                                        {
                                            Playlists.PlaylistResponse stErr = (Playlists.PlaylistResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(json, typeof(Playlists.PlaylistResponse));
                                            json = Newtonsoft.Json.JsonConvert.SerializeObject(stErr);
                                        }
                                        catch (Exception ex1)
                                        {
                                            json = Newtonsoft.Json.JsonConvert.SerializeObject(ex1);
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case "DELETE":
                        url = url + "?token=" + token + "&id=" + id;
                        try
                        {
                            WebRequest request = WebRequest.Create(url);
                            request.Method = method;
                            using (WebResponse response = request.GetResponse())
                            {
                                using (var reader = new StreamReader(response.GetResponseStream()))
                                {
                                    json = reader.ReadToEnd(); // do something fun...
                                }
                            }
                            //rbm.RokuPlaylists.delete(id);
                        }
                        catch (Exception ex)
                        {
                            json = ex.ToString();
                        }
                        break;
                    case "POST":
                        try
                        {
                            var web_request = (HttpWebRequest)WebRequest.Create(url);
                            ASCIIEncoding encoding = new ASCIIEncoding();
                            string postData = "token=" + token + "&title=NewPlaylist&type=manual";
                            byte[] data = encoding.GetBytes(postData);
                            web_request.Method = "POST";
                            web_request.ContentType = "application/x-www-form-urlencoded";
                            web_request.ContentLength = data.Length;
                            using (Stream stream = web_request.GetRequestStream())
                            {
                                stream.Write(data, 0, data.Length);
                            }
                            HttpWebResponse web_response = (HttpWebResponse)web_request.GetResponse();
                            string responseString = new StreamReader(web_response.GetResponseStream()).ReadToEnd();
                            json = responseString;
                        }
                        catch (WebException ex)
                        {
                            if (ex.Response != null)
                            {
                                using (var errorResponse = (HttpWebResponse)ex.Response)
                                {
                                    using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                                    {
                                        string stuff = reader.ReadToEnd();
                                        var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                                        try
                                        {
                                            StringBuilder str = new StringBuilder();
                                            API.stAPIError stErr = (API.stAPIError)Newtonsoft.Json.JsonConvert.DeserializeObject(json, typeof(API.stAPIError));
                                            json = Newtonsoft.Json.JsonConvert.SerializeObject(stErr);
                                        }
                                        catch (Exception)
                                        {
                                            json = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                                        }
                                    }
                                }
                            }
                        }

                        break;
                }
                rbm.Log(DateTime.Now + "\tAction:" + "GetPlaylists" + "\tData:" + json);

                return json;

            }
            public static string CreatePlaylist(string token, string title)
            {
                string json = "";
                string url = rbm.getSett("playlists");
                try
                {
                    var web_request = (HttpWebRequest)WebRequest.Create(url);
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    string postData = "token=" + token + "&title=" + title + "&type=manual";
                    byte[] data = encoding.GetBytes(postData);
                    web_request.Method = "POST";
                    web_request.ContentType = "application/x-www-form-urlencoded";
                    web_request.ContentLength = data.Length;
                    using (Stream stream = web_request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                    HttpWebResponse web_response = (HttpWebResponse)web_request.GetResponse();
                    string responseString = new StreamReader(web_response.GetResponseStream()).ReadToEnd();
                    dynamic d = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                    if (d.code == 200)
                    {

                        return Convert.ToString(d);
                    }
                    else {
                        rbm.Log(DateTime.Now + "\tAction:" + "CreatePlaylist" + "\tData:" + responseString);

                        return "0";
                    }
                }
                catch (WebException ex)
                {
                    if (ex.Response != null)
                    {
                        using (var errorResponse = (HttpWebResponse)ex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {
                                string stuff = reader.ReadToEnd();
                                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                                try
                                {
                                    StringBuilder str = new StringBuilder();
                                    API.stAPIError stErr = (API.stAPIError)Newtonsoft.Json.JsonConvert.DeserializeObject(json, typeof(API.stAPIError));
                                    json = Newtonsoft.Json.JsonConvert.SerializeObject(stErr);
                                }
                                catch (Exception)
                                {
                                    json = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                                }
                            }
                        }
                    }
                    rbm.Log(DateTime.Now + "\tAction:" + "CreatePlaylist" + "\tData:" + json);

                    return json;
                }
            }
            public static string SavePlaylistTitle(string token, string id, string title)
            {
                string json = "";
                string url = rbm.getSett("playlists");
                try
                {
                    var web_request = (HttpWebRequest)WebRequest.Create(url + "/" + id );
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    string postData = "token=" + token + "&title=" + title;
                    byte[] data = encoding.GetBytes(postData);
                    web_request.Method = "PUT";
                    web_request.ContentType = "application/x-www-form-urlencoded";
                    web_request.ContentLength = data.Length;
                    using (Stream stream = web_request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                    HttpWebResponse web_response = (HttpWebResponse)web_request.GetResponse();
                    string responseString = new StreamReader(web_response.GetResponseStream()).ReadToEnd();
                    json = responseString;
                    return json;
                }
                catch (WebException ex)
                {
                    if (ex.Response != null)
                    {
                        using (var errorResponse = (HttpWebResponse)ex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {
                                string stuff = reader.ReadToEnd();
                                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                                try
                                {
                                    StringBuilder str = new StringBuilder();
                                    API.stAPIError stErr = (API.stAPIError)Newtonsoft.Json.JsonConvert.DeserializeObject(json, typeof(API.stAPIError));
                                    json = Newtonsoft.Json.JsonConvert.SerializeObject(stErr);
                                }
                                catch (Exception)
                                {
                                    json = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
                                }
                            }
                        }
                    }
                    rbm.Log(DateTime.Now + "\tAction:" + "SavePlaylistTitle" + "\tData:" + json);

                    return json;
                }
            }
            public static string GetThisPlaylist(string token, string id)
            {
                string json = "";
                string url = rbm.getSett("playlists");
                try
                {
                    WebRequest request = WebRequest.Create(url + "/" + id +"?token=" + token);
                    request.Method = "GET";
                    using (WebResponse response = request.GetResponse())
                    {
                        using (var reader = new StreamReader(response.GetResponseStream()))
                        {
                            json = reader.ReadToEnd(); // do something fun...
                        }
                    }
                    return json;
                }
                catch (WebException ex)
                {
                    if (ex.Response != null)
                    {
                        using (var errorResponse = (HttpWebResponse)ex.Response)
                        {
                            using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                            {
                                string stuff = reader.ReadToEnd();
                                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                                try
                                {
                                    Playlists.PlaylistResponse stErr = (Playlists.PlaylistResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(json, typeof(Playlists.PlaylistResponse));
                                    json = Newtonsoft.Json.JsonConvert.SerializeObject(stErr);
                                }
                                catch (Exception ex1)
                                {
                                    json = Newtonsoft.Json.JsonConvert.SerializeObject(ex1);
                                }
                            }
                        }
                    }
                    rbm.Log(DateTime.Now + "\tAction:" + "GetThisPlaylist" + "\tData:" + json);

                    return json;
                }
            }

        }
        

        public static string GetPackages()
        {
            // Create a request for the URL. 		
            string packagesUrl = System.Configuration.ConfigurationManager.AppSettings["rbmtvPackages"];

            WebRequest request = WebRequest.Create(packagesUrl);

            // Get the response.
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();

            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content. 
            string responseFromServer = reader.ReadToEnd();

            // Cleanup the streams and the response.
            reader.Close();
            dataStream.Close();
            response.Close();
            // Display the content.
            return (responseFromServer);
        }
        public static string cancel_account(string token, string apiUserID)
        {
            string url = rbm.getSett("cancel_account").Replace("[USERID]", apiUserID);
            string json = "";
            using (WebClient client = new WebClient())
            {
                try
                {
                    json = Encoding.Default.GetString(client.UploadValues(url, "PUT",
                        new NameValueCollection
                            {
                                { "confirmation" , ""},
                                { "token", ""}
                            }
                    ));
                    rbm.Members.disableUserByAPI(apiUserID);
                }
                catch (Exception ex)
                {
                    rbm.Log(DateTime.Now + "\tAction:" + "cancel_account" + "\tData:" + ex.ToString());

                    return ex.ToString();
                }
            }
            return json;
        }

        public static string update_password(string token, string apiUserID, string password, string newPassword)
        {
            string url = rbm.getSett("update_password").Replace("[USERID]", apiUserID);
            string json = "";
            using (WebClient client = new WebClient())
            {
                try
                {
                    json = Encoding.Default.GetString(client.UploadValues(url, "PUT",
                        new NameValueCollection
                            {
                                { "current_password" , password},
                                { "password", newPassword},
                                { "password_confirmation", newPassword},
                                { "token", token}

                            }
                    ));
                }
                catch (Exception ex)
                {
                    json = ex.ToString();
                }
            }
            rbm.Log(DateTime.Now + "\tAction:" + "update_password" + "\tData:" + json);

            return json;
        }


        public static string upcoming_package_id(string token, string packageID, string settingID)
        {
            string url = rbm.getSett("upcoming_package_id").Replace("[SETTINGID]", settingID);
            string json = "";
            using (WebClient client = new WebClient())
            {
                try
                {
                    json = Encoding.Default.GetString(client.UploadValues(url, "PUT",
                        new NameValueCollection
                            {
                                { "name" , "upcoming_package_id"},
                                { "token", token},
                                { "value", packageID}
                            }
                    ));
                }
                catch (Exception ex)
                {
                    json = ex.ToString();
                }
            }
            rbm.Log(DateTime.Now + "\tAction:" + "upcoming_package_id" + "\tData:" + json);

            return json;
        }


        public static string Encoded_Assets_By_Playlist(string token, string playlistID)
        {
            string json;
            string url = rbm.getSett("playlists");
            url = url + "/" + playlistID + "/encoded_assets?token=" + token;
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "GET";
                using (WebResponse response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        json = reader.ReadToEnd(); // do something fun...
                    }
                }
            }
            catch (Exception ex)
            {
                json = ex.ToString();
            }
            rbm.Log(DateTime.Now + "\tAction:" + "Encoded_Assets_By_Playlist" + "\tData:" + json);

            return json;
        }

        public static string Save_Playlist_Selections(string token, string playlistID, string videos)
        {
            //verify integer values
            string[] vids = videos.Split(new string[] { "," }, StringSplitOptions.None);
            StringBuilder str = new StringBuilder("[");
            for (int i = 0; i < vids.Length; i++)
            {
                try
                {
                    int x = Convert.ToInt32(vids[i]);
                    str.Append(Convert.ToString(x) + ",");
                }
                catch (Exception)
                {

                }
            }
            str.Append("]");
            videos = Convert.ToString(str);
            videos = videos.Remove(videos.LastIndexOf(","), 1);
            //post new list to v5
            string url = RBMLib.lib.getSett("playlists") + "/" + playlistID + "/encoded_assets";
            string json = "";
            using (WebClient client = new WebClient())
            {
                try
                {
                    json = Encoding.Default.GetString(client.UploadValues(url, "PUT",
                        new NameValueCollection
                            {
                                { "token", token},
                                { "encoded_assets_ids", videos},
                            }
                    ));
                }
                catch (Exception ex)
                {
                    json = ex.ToString();
                }
            }
            rbm.Log(DateTime.Now + "\tAction:" + "Save_Playlist_Selections" + "\tData:" + json);

            return json;
        }

        public static string SendEmail(string apiUserID, string subject, string body, string title) 
        {
            
            string chkDate = DateTime.Today.ToString("yyy-MM-dd");
            string chk = rbm.MD5("rbm-custom-email-" + chkDate);
            string json = "";
            string token = "51daa71801c18c9de6f2fb9d4076250f";
            string url = rbm.getSett("custom_email").Replace("[USERID]", apiUserID) + "?auth=" + token + "&subject=" + subject + "&body=" + body + "&title=" + title;
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "POST";
                using (WebResponse response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        json = reader.ReadToEnd(); // do something fun...
                    }
                }
            }
            catch (Exception ex)
            {
                json = ex.ToString();
            }
            return json;
        
        }

        public static string emailtest(string userID)
        {
            var obj = rbm.Members.getData(userID);

            //string subject = HttpUtility.UrlEncode("Roku Channelbuilder Notifications");
            //string body = HttpUtility.UrlEncode("We recieved and are processing your request.");
            //string title = HttpUtility.UrlEncode("Compile Request.");

            string subject = "Roku-Channelbuilder";
            string body = "We recieved and are processing your request";
            string title = "Compile-Request";
            string chkDate = DateTime.Now.ToString("MM-dd-yyyy");

            string token = rbm.MD5("rbm-custom-email-" + chkDate);
            //string token = "51daa71801c18c9de6f2fb9d4076250f";

            string json = "";

            string url = rbm.getSett("custom_email").Replace("[USERID]", obj.apiUserID) + "?auth=" + token + "&subject=" + subject + "&body=" + body + "&title=" + title;
            try
            {
                WebRequest request = WebRequest.Create(url);
                request.Method = "POST";
                using (WebResponse response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        json = reader.ReadToEnd(); // do something fun...
                    }
                }
            }
            catch (Exception ex)
            {
                json = ex.ToString();
            }
            return json;
        }

        public static string UserCompileNotification(string userID)
        {
            var obj = rbm.Members.getData(userID);
            string subject = "Roku-Channelbuilder";
            string body = "Processing-Request";
            string title = "Compile-Request";
            string chkDate = DateTime.Now.ToString("MM-dd-yyyy");

            string token = rbm.MD5("rbm-custom-email-" + chkDate);
            obj.authToken = token;
            string json = "";
            string url = rbm.getSett("custom_email").Replace("[USERID]", obj.apiUserID) + "?auth=" + token;
            rbm.Log(DateTime.Now + "\tAction:" + "UserCompileNotification" + "\tData:" + rbm.obj2json(obj));

            var vals = new NameValueCollection
                            {
                                { "subject" , subject},
                                { "body", body},
                                { "title", title}
                            };
            using (WebClient client = new WebClient())
            {
                try
                {
                    json = Encoding.Default.GetString(client.UploadValues(url, "POST",
                        new NameValueCollection
                            {
                                { "subject" , subject},
                                { "body", body},
                                { "title", title}
                            }
                    ));
                }
                catch (Exception ex)
                {
                    json = ex.ToString();
                }
            }
            rbm.Log(DateTime.Now + "\tAction:" + "UserCompileNotification" + "\tData:" + rbm.obj2json(json));

            return json;
        }
    }

}