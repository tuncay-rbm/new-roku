﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace RBMLib
{
    public class lib
    {
        #region commonFunctions
        public static string getConn(string connName = "sqlConn")
        { return ConfigurationManager.ConnectionStrings[connName].ConnectionString; }
        public static string getSett(string sett)
        { return ConfigurationManager.AppSettings[sett]; }
        public static string toStr(object obj)
        { return Convert.ToString(obj); }
        public static int toInt(object obj)
        {
            try
            {
                return Convert.ToInt32(obj);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static double toDbl(object obj)
        {
            try
            {
                return Convert.ToDouble(obj);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static string discardTime(object obj)
        {
            DateTime tmp = toDateTime(obj);
            return tmp.ToShortDateString();
        }
        public static string weekDay(object obj, bool shortDayName = true)
        {
            DateTime tmp = toDateTime(obj);
            return shortDayName ? tmp.DayOfWeek.ToString().Substring(0, 3) : tmp.DayOfWeek.ToString();
        }
        public static DateTime toDateTime(object obj)
        {
            try
            {
                return Convert.ToDateTime(obj);
            }
            catch (Exception)
            {
                return new DateTime();
            }
        }
        public static Boolean isNull(object obj)
        {
            try
            { return string.IsNullOrEmpty(Convert.ToString(obj)) || Convert.ToString(obj) == "null"; }
            catch (Exception)
            { return false; }
        }
        public static void fill(DropDownList lst, DataSet ds, string valueField, string textField)
        {
            lst.DataTextField = textField;
            lst.DataValueField = valueField;
            lst.DataSource = ds;
            lst.DataBind();
        }
        public static bool isImage(HttpPostedFile file)
        { return ((file != null) && Regex.IsMatch(file.ContentType, "image/\\S+") && (file.ContentLength > 0)); }
        public static string userFriendlyShortDate(object dt)
        {
            return isNull(toStr(dt)) ? "" : userFriendlyShortDate(Convert.ToDateTime(dt));
        }
        public static string userFriendlyShortDate(DateTime dt)
        {
            return dt.ToString("MMMM") + " " + dt.Day + ", " + dt.Year;
        }
        public static string mySqlDate(DateTime date)
        { return date.Year + "-" + date.Month + "-" + date.Day; }
        public static string mySqlDate(string dt)
        { return mySqlDate(Convert.ToDateTime(dt)); }
        public static string mySqlDate()
        { return mySqlDate(DateTime.Now); }
        public static string mySqlDateTime(DateTime date)
        { return date.Year + "-" + date.Month + "-" + date.Day + " " + date.Hour + ":" + date.Minute + ":" + date.Second; }
        public static string mySqlDateTime(string dt)
        { return mySqlDateTime(Convert.ToDateTime(dt)); }
        public static string mySqlDateTime()
        { return mySqlDateTime(DateTime.Now); }
        public static bool validateEmail(string email)
        {
            var regex = new Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
            return regex.IsMatch(email);
        }
        public static void writeCookie(string key, string value, int days = 30)
        {
            HttpCookie cookie = new HttpCookie(key) { Value = value, Expires = DateTime.Now.AddDays(days) };
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
        public static string readCookie(string key)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[key];
            return (cookie != null) ? cookie.Value : "";
        }
        public static string encrypt(string ToEncrypt)
        {
            byte[] toEncryptArray = Encoding.UTF8.GetBytes(ToEncrypt);
            TripleDESCryptoServiceProvider tDes = new TripleDESCryptoServiceProvider
            {
                Key = Encoding.UTF8.GetBytes(getSett("encryption_key")),
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            ICryptoTransform cTransform = tDes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tDes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        public static string decrypt(string cypherString)
        {
            byte[] toDecryptArray = Convert.FromBase64String(cypherString);
            TripleDESCryptoServiceProvider tDes = new TripleDESCryptoServiceProvider
            {
                Key = Encoding.UTF8.GetBytes(getSett("encryption_key")),
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            ICryptoTransform cTransform = tDes.CreateDecryptor();
            try
            {
                byte[] resultArray = cTransform.TransformFinalBlock(toDecryptArray, 0, toDecryptArray.Length);

                tDes.Clear();
                return Encoding.UTF8.GetString(resultArray, 0, resultArray.Length);
            }
            catch (Exception)
            {
                return "";
            }
        }
        public static string GetIPAddress()
        {
            HttpContext context = HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ipAddress))
            {
                ipAddress = context.Request.ServerVariables["REMOTE_ADDR"];
            }

            return ipAddress;
        }
        public static string sanitize(string input)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9 @.-]");
            return rgx.Replace(input, "");
        }
        public static string commArg(object btn)
        {
            return ((LinkButton)btn).CommandArgument;
        }
        public static void runMySQL(string sql, string connStr)
        {
            using (MySqlConnection conn = new MySqlConnection(connStr))
            {
                using (MySqlCommand cmd = new MySqlCommand(sql, conn))
                {
                    cmd.CommandType = CommandType.Text;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void runMsSQL(string sql, string connStr)
        {
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.CommandType = CommandType.Text;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void createTextFile(string path, string content)
        {
            using (TextWriter tw = new StreamWriter(path, false))
            {
                tw.WriteLine(content);
                tw.Close();
            }
        }
        public static string round(double num)
        {
            return Math.Round(num, 2).ToString();
        }

        public static string Left(string txt, int lenght)
        {
            return txt.Length < lenght ? txt : txt.Substring(0, lenght);
        }

        #endregion

        #region API/JSON Functions
        public static void write(string txt)
        {
            HttpContext.Current.Response.Write(txt);
        }

        public static void msg(string msg)
        { write("{\"msg\":\"" + msg + "\"}"); }

        public static string message(string msg)
        { return "{\"msg\":\"" + msg + "\"}"; }

        public static string get(string key)
        { return (isNull(HttpContext.Current.Request.QueryString[key])) ? "" : HttpContext.Current.Request.QueryString[key]; }

        public static string form(string key)
        { return (isNull(HttpContext.Current.Request.Form[key])) ? "" : HttpContext.Current.Request.Form[key]; }

        public static string ds2json(DataSet ds)
        {
            return ds.Tables[0].Rows.Count > 0 ? JsonConvert.SerializeObject(ds.Tables[0]) : message("No record found");
        }

        public static string obj2json(object obj, bool ignoreNull = true)
        {
            if (!ignoreNull)
            {
                JsonSerializerSettings sett = new JsonSerializerSettings();
                sett.NullValueHandling = NullValueHandling.Include;
                return JsonConvert.SerializeObject(obj, sett);
            }
            return JsonConvert.SerializeObject(obj);
        }

        public static string getVal(string key)
        {
            string data = (!isNull(form(key))) ? form(key) : get(key);
            return (data == "undefined" || data == "null") ? string.Empty : data;
        }

        public static int getValInt(string key)
        {
            return (!isNull(form(key))) ? toInt(form(key)) : toInt(get(key));
        }

        public static DateTime getValDateTime(string key)
        {
            return (!isNull(form(key))) ? toDateTime(form(key)) : toDateTime(get(key));
        }

        public static string MD5(string str)
        {
            byte[] textBytes = Encoding.Default.GetBytes(str);
            try
            {
                MD5CryptoServiceProvider cryptHandler = new MD5CryptoServiceProvider();
                byte[] hash = cryptHandler.ComputeHash(textBytes);
                string ret = "";
                foreach (byte a in hash)
                {
                    if (a < 16)
                        ret += "0" + a.ToString("x");
                    else
                        ret += a.ToString("x");
                }
                return ret;
            }
            catch
            {
                return "";
            }
        }
        #endregion

        #region overrides
        public class tDList : List<string>
        {
            public bool HasDuplicates()
            {
                List<string> lst = this;
                for (int i = 0; i < lst.Count; i++)
                {
                    for (int j = i + 1; i < lst.Count; i++)
                        if (lst[i] == lst[j]) return true;
                }
                return false;
            }
        }
        #endregion

        #region email
        public class mail
        {
            public struct stEmail
            {
                public string from, fromName, replyTo, to, cc, bcc, subject, body;
                public bool isHTML;
            }

            public static Boolean send(stEmail email)
            {
                try
                {
                    if (isNull(email.from))
                        email.from = getSett("smtpFrom");

                    if (isNull(email.fromName))
                        email.from = getSett("smtpFromName");

                    SmtpClient client = new SmtpClient
                    {
                        Host = getSett("smtpHost"),
                        Port = 25,
                        Credentials = new NetworkCredential(getSett("smtpUser"), getSett("smtpPass"))
                    };

                    MailMessage message = new MailMessage();

                    if (!string.IsNullOrEmpty(email.to))
                    {
                        if (email.to.Contains(";"))
                        {
                            string[] arr = email.to.Split(';');
                            foreach (string mail in arr)
                            {
                                message.To.Add(mail);
                            }
                        }
                        else
                            message.To.Add(email.to);
                    }

                    if (!string.IsNullOrEmpty(email.cc))
                    {
                        if (email.cc.Contains(";"))
                        {
                            string[] arr = email.cc.Split(';');
                            foreach (string mail in arr)
                            {
                                message.CC.Add(mail);
                            }
                        }
                        else
                            message.CC.Add(email.cc);
                    }

                    if (!string.IsNullOrEmpty(email.bcc))
                    {
                        if (email.bcc.Contains(";"))
                        {
                            string[] arr = email.bcc.Split(';');
                            foreach (string mail in arr)
                            {
                                message.Bcc.Add(mail);
                            }
                        }
                        else
                            message.Bcc.Add(email.bcc);
                    }
                    message.From = string.IsNullOrEmpty(email.fromName) ? new MailAddress(email.@from) : new MailAddress(email.@from, email.fromName);
                    message.ReplyToList.Add(email.from);
                    message.Subject = email.subject;
                    message.IsBodyHtml = email.isHTML;
                    message.Body = email.body;
                    client.Send(message);
                    return true;
                }
                catch (Exception ex)
                {
                    email.body = "Subject: " + email.subject + "<br>To: " + email.to + "<br>CC: " + email.cc + "<br>BCC: " + email.bcc + "<br><br>Error: " + ex.Message + "<br><br>" + ex.StackTrace;
                    email.to = "tuncay@rbm.tv";
                    email.cc = email.bcc = "";
                    email.subject = "Error on mail sending";

                    send(email);

                    return false;
                }
            }
        }
        #endregion
    }
}
