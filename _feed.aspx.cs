﻿using System;

public partial class _feed : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		Response.Clear();
		Response.AddHeader("Content-type", "text/xml");
        if (rbm.MD5(rbm._get("id") + "-rbm-" + rbm._get("memberID")) == rbm._get("chksum") || rbm.MD5(rbm._get("id") + "-rbm-") == rbm._get("chksum") || rbm._get("chksum") == "rbm" || (1 == 1))
	    {
		    switch (rbm._get("action"))
		    {
                case "ooyalaFeed":
                    Response.Write(rbm.ooyala.getVideoFeed(rbm._get("id")));
                    break;
				case "videoFeed":
                    //Response.Write(rbm.ooyala.getVideoFeed(rbm._get("id")));
					
                    Response.Write(rbm.feed.getVideoFeed(rbm._get("id")));
				    break;
				case "categoryFeed":
					Response.Write(rbm.feed.getCategoryFeed(rbm._get("id")).Replace("&","&amp;"));
				    break;
		    }
	    }

	    Response.End();
    }
}