﻿<%@ WebHandler Language="C#" Class="api" %>

using System;
using System.Web;
using System.Web.UI;
using RBMLib;
public class api : IHttpHandler
{
    public void ProcessRequest(HttpContext context)
    {
        string action = rbm.toStr(context.Request["action"]).Trim();
        string filename = "";
        try
        {
            context.Response.Clear();
            if (!rbm.isNull(HttpContext.Current.Request.Files["file"]))
            {
                if (!rbm.isNull(lib.form("categoryID")))
                    filename = saveFile(lib.form("channelID"), true);
                else
                    filename = saveFile(lib.form("channelID"));
            }
            switch (action)
            {
                case "package_boxes":
                    lib.write(lib.ds2json(rbm.Packages.listPackageBoxes()));
                    break;
                case "build_details":
                    lib.write(lib.ds2json(rbm.Packages.listByChannel(lib.get("channelID"))));
                    break;
                case "commit_new_password":
                    lib.write(rbm.API.UpdatePassword(lib.get("email"), lib.get("hash"), lib.get("password")));
                    break;
                case "send_custom_email":
                    lib.write(rbm.API.SendEmail("201", "TestingSubject", "TestingBody", "TestingTitle"));
                    break;
                case "send_compile_message":
                    lib.write(rbm.API.UserCompileNotification(lib.get("userID")));
                    //lib.write(rbm.API.emailtest(lib.get("userID")));
                    break;
                case "compile_channel":
                    rbm.Channel.NotifyAdmin(lib.get("channelID"));
                    break;
                case "drop_a_line":
                    rbm.contact_form.add((rbm.contact_form.stContact_form)Newtonsoft.Json.JsonConvert.DeserializeObject(HttpUtility.UrlDecode(lib.form("data")), typeof(rbm.contact_form.stContact_form)));
                    break;
                case "category_order":
                    rbm.Categories.updateOder(lib.get("categoryID"), lib.get("ordr"));
                    lib.write(lib.get("ordr"));
                    break;
                case "cancel_account":
                    lib.write(rbm.API.cancel_account(lib.get("token"), lib.get("apiUserID")));
                    
                    break;
                case "update_password":
                    lib.write(rbm.API.update_password(lib.get("token"), lib.get("apiUserID"), lib.get("password"), lib.get("newPassword")));
                    break;
                case "upcoming_package_id":
                    lib.write(rbm.API.upcoming_package_id(lib.get("token"), lib.get("packageID"), lib.get("settingID")));
                    break;
                case "update_playlist_ordr":
                    rbm.RokuPlaylists.setOrder(lib.get("id"), lib.get("ordr"));
                    break;
                case "signup":
                    var obj = rbm.API.ValidateSignupFields((rbm.API.stSignup)Newtonsoft.Json.JsonConvert.DeserializeObject(HttpUtility.UrlDecode(rbm._get("json")), typeof(rbm.API.stSignup)));
                    lib.write(rbm.obj2json(obj));
                    break;
                case "reset_password":
                    lib.write(rbm.API.ResetPassword(lib.get("email")));
                    break;
                case "get_packages":
                    lib.write(rbm.API.GetPackages());
                    break;
                case "already_exists":
                    var objExists = rbm.API.AlreadyExists((rbm.API.stSignup)Newtonsoft.Json.JsonConvert.DeserializeObject(lib.form("data"), typeof(rbm.API.stSignup)));
                    if (objExists.errors.Count > 0)
                    {
                        lib.write(rbm.obj2json(objExists.errors));
                    }
                    else
                    {
                        lib.write(null);
                    }
                    break;
                case "get_rbm_user":
                    string json = rbm.API.get_rbm_user(rbm._get("token"));
                    lib.write(json);
                    break;
                case "update_rbm_user":
                    string updateUser = rbm.API.UpdateRbmUser(lib.get("token"), lib.get("id"), lib.get("first_name"), lib.get("last_name"), lib.get("permissions"));
                    lib.write(lib.toStr(updateUser));
                    break;
                case "enable_user":
                    rbm.Members.enableUser(lib.get("id"));
                    break;
                case "disable_user":
                    rbm.Members.disableUser(lib.get("id"));
                    break;
                case "admin_users":
                    lib.write(rbm.ds2json(rbm.Members.list()));
                    break;
                case "admin_channels":
                    lib.write(rbm.ds2json(rbm.Channel.listForAdmin()));
                    break;
                case "get_channel":
                    lib.write(rbm.obj2json(rbm.Channel.getData(rbm._get("channelID"))));
                    break;   
                case "add_channel":
                    var channelID = rbm.Channel.add(rbm._get("userID"));
                    break;
                case "delete_channel":
                    rbm.Channel.DeleteChannel(rbm._get("id"));
                    break;
                case "list_channels":
                    lib.write(rbm.ds2json(rbm.Channel.list(rbm._get("userID"))));
                    break;
                case "splash_sd":
                    rbm.Channel.SplashSD(lib.form("channelID"), filename);
                    lib.write("{\"splash_sd\": \"/_channel-images/" + lib.form("channelID") + "/" + filename + "\"}");
                    break;
                case "splash_hd":
                    rbm.Channel.SplashHD(lib.form("channelID"), filename);
                    lib.write("{\"splash_hd\": \"/_channel-images/" + lib.form("channelID") + "/" + filename + "\"}");
                    break;
                case "list_images":
                    lib.write(rbm.obj2json(rbm.Channel.channelImageOptions(rbm._get("channelID"))));
                    break;
                case "side_icon_sd":
                    rbm.Channel.SideIconSD(lib.form("channelID"), filename);
                    lib.write("/_channel-images/" + rbm._get("channelID") + "/" + filename);
                    break;
                case "side_icon_hd":
                    rbm.Channel.SideIconHD(rbm._get("channelID"), filename);
                    lib.write("/_channel-images/" + rbm._get("channelID") + "/" + filename);
                    break;
                case "focus_icon_sd":
                    rbm.Channel.FocusIconSD(rbm._get("channelID"), filename);
                    lib.write("/_channel-images/" + rbm._get("channelID") + "/" + filename);
                    break;
                case "focus_icon_hd":
                    rbm.CustomTasks.CreateChannelImages(context.Request["channelID"], filename);
                    lib.write("/_channel-images/" + context.Request["channelID"] + "/" + filename);
                    break;
                case "logo_sd":
                    rbm.Channel.LogoSD(rbm._get("channelID"), filename);
                    lib.write("/_channel-images/" + rbm._get("channelID") + "/" + filename);
                    break;
                case "logo_hd":
                    rbm.CustomTasks.CreateLogoImages(lib.form("channelID"), filename);                    
                    lib.write("/_channel-images/" + lib.form("channelID") + "/" + filename);
                    break;
                case "overhang_sd":
                    rbm.Channel.OverhangSD(rbm._get("channelID"), filename);
                    lib.write("/_channel-images/" + rbm._get("channelID") + "/" + filename);
                    break;
                case "overhang_hd":
                    rbm.CustomTasks.CreateOverhangImages(lib.form("channelID"), filename);                    
                    lib.write("/_channel-images/" + lib.form("channelID") + "/" + filename);
                    break;
                case "save_channel_changes":
                    dynamic d = Newtonsoft.Json.JsonConvert.DeserializeObject(lib.form("data"));
                    var stTitle = new rbm.Channel.stTitleDescription();
                    stTitle.id = d.id;
                    stTitle.title = d.title;
                    stTitle.description = d.description;
                    rbm.Channel.saveTitleAndDescription(stTitle);
                    rbm.Channel.SplashColor(lib.toStr(d.id), lib.toStr(d.splash_bg_color));
                    lib.write(lib.form("data"));
                    break;
                case "create_theme":
                    var createTheme = (rbm.Themes.stTheme)Newtonsoft.Json.JsonConvert.DeserializeObject(lib.form("data"), typeof(rbm.Themes.stTheme));
                    rbm.Themes.CreateChannelTheme(createTheme);
                    break;
                case "update_theme":
                    var updateTheme = (rbm.Themes.stTheme)Newtonsoft.Json.JsonConvert.DeserializeObject(lib.form("data"), typeof(rbm.Themes.stTheme));
                    rbm.Themes.UpdateChannelTheme(updateTheme);
                    break;
                case "get_theme":
                    lib.write(rbm.obj2json(rbm.Themes.GetChannelTheme(rbm._get("channelID"))));
                    break;
                case "save_theme":
                    var theme = (rbm.Themes.stTheme)Newtonsoft.Json.JsonConvert.DeserializeObject(lib.form("data"), typeof(rbm.Themes.stTheme));
                    rbm.Themes.UpdateChannelTheme(theme);
                    break;
                case "list_categories":
                    lib.write(rbm.ds2json(rbm.Categories.list(rbm._get("channelID"))));
                    break;
                case "delete_category":
                    rbm.Categories.delete(rbm._get("id"));
                    lib.write(rbm.ds2json(rbm.Categories.list(rbm._get("channelID"))));
                    break;
                case "add_category":
                    rbm.Categories.add(rbm._get("channelID"));
                    lib.write(rbm.ds2json(rbm.Categories.list(rbm._get("channelID"))));
                    break;
                case "save_category_title":
                    lib.write(rbm.ds2json(rbm.Categories.saveTitleAndDescription(rbm._get("channelID"), rbm._get("categoryID"), rbm._get("title"), rbm._get("description"))));
                    break;
                case "category_sd":
                    rbm.Categories.CategorySD(lib.form("categoryID"), filename);
                    lib.write("{\"category_sd\": \"/_category-images/" + rbm._get("channelID") + "/" + filename + "\"}");
                    break;
                case "category_hd":
                    rbm.CustomTasks.CreateCategoryImages(lib.form("categoryID"), filename);                    
                    lib.write("{\"hd_path\": \"/_category-images/" + lib.form("channelID") + "/" + filename + "\"}");
                    break;
                case "save_category_changes":
                    dynamic category_d = Newtonsoft.Json.JsonConvert.DeserializeObject(lib.form("data"));
                    string chID = rbm.toStr(category_d.channelID);
                    string catID = rbm.toStr(category_d.id);
                    string catTitle = rbm.toStr(category_d.title);
                    string catDes = rbm.toStr(category_d.description);
                    System.Data.DataSet ds = rbm.Categories.saveTitleAndDescription(chID,catID, catTitle, catDes);
                    break;
                case "get_original_assets":
                    lib.write(rbm.API.Original_Assets(rbm._get("token"), "GET"));
                    break;
                case "get_playlists":
                    lib.write(rbm.API.Playlists.GetPlaylists(rbm._get("token"), "GET"));
                    break;
                case "get_roku_playlists":
                    lib.write(rbm.ds2json(rbm.RokuPlaylists.listByCategoryID(lib.get("categoryID"))));
                    break;
                case "create_roku_playlist":
                    var new_roku_playlist = (rbm.RokuPlaylists.stPlaylists)Newtonsoft.Json.JsonConvert.DeserializeObject(lib.form("data"), typeof(rbm.RokuPlaylists.stPlaylists));
                    rbm.RokuPlaylists.add(new_roku_playlist);
                    break;
                case "delete_roku_playlist":
                    rbm.RokuPlaylists.delete(lib.get("id"));
                    break;
                case "update_roku_playlist":
                    dynamic playlist_p = Newtonsoft.Json.JsonConvert.DeserializeObject(lib.form("data"));
                    rbm.RokuPlaylists.updateTitleAndDescription(rbm.toStr(playlist_p.id), rbm.toStr(playlist_p.title), rbm.toStr(playlist_p.description));
                    break;
                case "create_playlist":
                    dynamic playlist_d = Newtonsoft.Json.JsonConvert.DeserializeObject(lib.form("data"));
                    string token = Convert.ToString(playlist_d.authToken);
                    string title = Convert.ToString(playlist_d.title);
                    dynamic response = Newtonsoft.Json.JsonConvert.DeserializeObject(rbm.API.Playlists.CreatePlaylist(token, title));
                    context.Response.Write(Convert.ToString(response.response.playlists));
                    break;
                case "save_playlist_title":
                    lib.write(rbm.API.Playlists.SavePlaylistTitle(rbm._get("token"), rbm._get("playlistID"), rbm._get("title")));
                    break;
                case "get_this_playlist":
                    lib.write(rbm.API.Playlists.GetThisPlaylist(rbm._get("token"), rbm._get("playlistID")));
                    break;
                case "post_playlists":
                    lib.write(rbm.API.Playlists.GetPlaylists(rbm._get("token"), "POST"));
                    break;
                case "new_rbm_playlist":
                    break;
                case "get_encoded_assets":
                    lib.write(rbm.API.Encoded_Assets(rbm._get("token"), "GET"));
                    break;
                case "delete_original_asset":
                    lib.write(rbm.API.Delete_Original_Asset(lib.get("id"), lib.get("token")));
                    break;
                case "update_encoded_asset":
                    dynamic asset = Newtonsoft.Json.JsonConvert.DeserializeObject(context.Request["data"]);
                    lib.write(rbm.API.Update_Encoded_Asset(lib.toStr(asset.authToken), lib.toStr(asset.title), lib.toStr(asset.id), lib.toStr(asset.meta.description)));
                    //lib.write(lib.toStr(asset.meta.description));
                    break;
                case "save_playlist_selections":
                    lib.write(rbm.API.Save_Playlist_Selections(rbm._get("token"), rbm._get("playlistID"), rbm._get("videos")));
                    
                    break;
                case "get_playlist_videos":
                    lib.write(rbm.API.Encoded_Assets_By_Playlist(rbm._get("token"), rbm._get("id")));
                    break;
                default:
                    lib.write("ROKU API");
                    break;
            }
            rbm.Log(DateTime.Now + "\tAction:" + action + "\tPosts:" + getPosts());
        }
        catch (Exception ex)
        {
            context.Response.StatusCode = 500;
            lib.write("{\"error\":[{\"message\":\"Server error\",\"compiler_message\":\"" + ex.Message + "\",\"inner_exception\":\"" + ex.InnerException + "\",\"code\":500}]}");
        }
 
        context.Response.End();       
    }
    public bool IsReusable
    {
        get { return false; }
    }

    protected string saveFile(string channelID, bool category=false) 
    {
        string dir = "_channel-images/" + channelID + "/";
        if (category) {
            dir = "_category-images/" + channelID + "/";
        }
        HttpPostedFile file = HttpContext.Current.Request.Files["file"];
        string sDirectory = System.IO.Path.Combine(HttpRuntime.AppDomainAppPath, dir);
        if (!System.IO.Directory.Exists(sDirectory)) System.IO.Directory.CreateDirectory(sDirectory);
        //refreshPage = System.IO.File.Exists(HttpContext.Current.Server.MapPath("/" + imagePath)) == true ? true : false;
        System.Threading.Thread.Sleep(100);
        file.SaveAs(sDirectory + file.FileName);
        return file.FileName;
    }
    private string getPosts()
    {
        
        System.Text.StringBuilder posts = new System.Text.StringBuilder(); string sp = "";
        for (int i = 0; i < HttpContext.Current.Request.Form.Count; i++)
        {
            posts.Append(sp + HttpContext.Current.Request.Form.GetKey(i) + ": " + HttpContext.Current.Request.Form[i]); sp = ", ";
        }
        for (int i = 0; i < HttpContext.Current.Request.QueryString.Count; i++)
        {
            posts.Append(sp + HttpContext.Current.Request.QueryString.GetKey(i) + ": " + HttpContext.Current.Request.QueryString[i]); sp = ", ";
            
        }
        return posts.ToString();
    }

}