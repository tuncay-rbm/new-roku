﻿<%@ Control Language="C#" ClassName="home" %>
<!-- 404 Error -->

<div id="landing">
	<div class="LandingContainer">
		<div class="VideoOverlay">
			<div class="LandingCnt">
				<a href="/"><img alt="Roku Channel Builder" src="../images/logo.png" alt="Roku Channel Builder" class="img-responsive LandingLogo"></a>
				<div class="PgError">
					<h1>404</h1>
					<h3>Page Not Found</h3>
					<p><a href="/">Click Here</a> if you are not redirected to our homepage within 10 seconds</p>
				</div>
			</div>
			<div class="VideoCnt">
				<video autoplay loop muted poster="video_img.jpg" id="bgvid">
				  <source src="../videos/video_bg.webm" type="video/webm">
				  <source src="../videos/video_bg.mp4" type="video/mp4">
				  <source src="../videos/video_bg.ogv" type="video/ogg" />
				</video>
			</div> 
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		setTimeout(function() {window.location.replace("/")}, 10000);
	});
</script>