﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>
<script runat="server">

	protected void Page_Load(object sender, EventArgs e){

	}
    protected void lnkLogin_Click(object sender, EventArgs e)
    {
        bool email = txtUsername.Text == rbm.getSett("admin_user");
        bool password = txtPassword.Text == rbm.getSett("admin_password");
        
            if (email && password)
            {

                var user = new rbm.Members.Admin();
                user.id = "0";
                user.authToken = rbm.getAdminToken();
                user.isAdmin = true;
                Session["user"] = user;
                Response.Redirect("/admin/channel-list");
            }
            else
            {
                lblErrLogin.Text = "Invalid credentials, please try again.";
                lblErrLogin.Visible = true;
            }  
        
    }
    
</script>
<asp:Label ID="meta_title" runat="server" Text="Roku-Admin" Visible="false" />

<!-- LOGIN START -->
<div>

    <div id="landing">
      <div class="LandingContainer">
        <div class="VideoOverlay">
          <div class="LandingCnt">
            <a href="/"><img alt="Roku Channel Builder" src="images/logo.png" alt="Roku Channel Builder" class="img-responsive LandingLogo"></a>
            
            <div class="CustomForm">
                <asp:Panel ID="form1" runat="server">
                    <div class="form-group">
                        <label>Email</label>
                        <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" Placeholder="Email" />
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblPass" runat="server" AssociatedControlID="txtPassword">Password</asp:Label>
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control" Placeholder="Password" />  
                    </div>
                    <div class="form-group">
                        <asp:Button ID="lnkLogin" title="Login" CssClass="btn btn-default btn-block" runat="server" OnClick="lnkLogin_Click" Text="Sign In"></asp:Button>
                    </div> 
                    <div class="form-group">
                        <asp:Label ID="lblErrLogin" runat="server" Text="Please check your username and password!" Visible="False" CssClass="error"></asp:Label>
                    </div>
                </asp:Panel>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- end landing -->

</div>
