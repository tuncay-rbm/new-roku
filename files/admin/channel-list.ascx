﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            bindData();             
        }
    }

    protected void bindData() 
    {
        
    }

</script>
<inc:module runat="server" ID="login_check"></inc:module>
<asp:Label ID="meta_title" runat="server" Text="Channel Admin" Visible="false" />

<inc:module runat="server" ID="script">
    <script type="text/javascript" src="/js/angular.min.js"></script>
    <script type="text/javascript" src="/js/ui-bootstrap.js"></script>
    <script type="text/javascript" src="/js/directives/dirPagination.js"></script>
    <script type="text/javascript" src="/js/angularFileUpload.js"></script>
    <link rel='stylesheet' href='/css/loadingBar.css' type='text/css' media='all' />
    <script type='text/javascript' src="/js/loadingBar.js"></script>
	<script type="text/javascript" src="/js/controllers/AdminChannels.js"></script>
    <link rel='stylesheet' href='/css/jquery-ui-1.10.4.custom.min.css' type='text/css' media='all' />

</inc:module>

<div class="container" data-ng-app="roku" data-ng-controller="AdminChannelsController">
        <asp:HiddenField runat="server" ID="hdUserID" />
                <div class="col-md-12" data-ng-hide="editMode">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Search:</label>
                            <input type="text" class="form-control" data-ng-model="keyword.title" />
                        </div>
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th></th>
                                <th data-ng-click="sort('title')">NAME</th>
                                <th data-ng-click="sort('id')">CHANNEL ID</th>
                                <th data-ng-click="sort('createdOn')">CREATED ON</th>
                                <th>Version</th>
                                <th>Last Build</th>
                            </tr>
                        </thead>                    
                        <tbody>
                            <tr dir-paginate="channel in channels | filter: keyword.title| orderBy:sortField:reverse | itemsPerPage: 10" current-page="currentPage" >
                                <td style="width:90px"><img data-ng-src="/_channel-images/{{channel.id}}/{{channel.focus_icon_hd}}" width="100%" height="50px" /></td>
                                <td>{{channel.title}}</td>
                                <td>{{channel.id}}</td>
                                <td>{{channel.createdOn | date: 'MM/dd/yyyy'}}</td>
                                <td>{{channel.buildVersion}}</td>
                                <td>{{channel.lastBuild}}</td>

                                <td class="RightTxt">
                                    <%--<a title="Channel Details" data-ng-click="viewDetails(channel)" class="btn btn-default btn-xs"><span class="fa fa-info" aria-hidden="true"></span></a>--%>
                                    <a title="Build Details" data-ng-click="viewDetails(channel)" class="btn btn-info btn-xs"><span class="fa fa-cog"></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <dir-pagination-controls></dir-pagination-controls>
                        <div class="clearboth">&nbsp;</div>
                    </div>
                </div>
            <div class="col-md-12" data-ng-hide="!editMode">
                 <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th data-ng-click="sort('title')">CHANNEL NAME</th>
                                <th data-ng-click="sort('version')">VERSION</th>
                                <th data-ng-click="sort('compile_date')">COMPILED ON</th>
                                <th data-ng-click="sort('build_file')">ZIP FILE</th>

                                <th>Package Device</th>
                                <th>Package File</th>
                                <th>Packaged On</th>
                                <th>Submitted On</th>

                                <th>Public/Private</th>
                                <th>Channel Store URL</th>
                                <th>Store Status</th>
                                <th>Release Date</th>

                                <td></td>


                            </tr>
                        </thead>                    
                        <tbody>
                            <tr data-ng-repeat="build in selectedChannel.builds">
                                <td>{{selectedChannel.title}}</td>
                                <td>{{build.version}}</td>
                                <td>{{build.compile_date | date: 'MM/dd/yyyy'}}</td>
                                <td>{{build.build_file}}</td>

                                <td>{{build.packageBox}}</td>
                                <td>{{build.package_file}}</td>
                                <td>{{build.package_date}}</td>
                                <td>{{build.submit_date}}</td>

                                <td>{{build.is_public != 1 ? 'Private' : 'Public'}}</td>
                                <td>{{build.vanity_url}}</td>
                                <td>{{build.publish_status}}</td>
                                <td>{{build.release_date}}</td>

                                <td>
                                    <a title="Build Details" data-ng-click="viewBuild(build)" class="btn btn-info btn-xs"><span class="fa fa-cog"></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                                    <div class="pull-right">
                        <a data-ng-click="editMode = false" class="btn btn-default">Back</a>
                        <div class="clearboth">&nbsp;</div>
                    </div>
            </div>
                        <div class="clearboth">&nbsp;</div>


</div>




