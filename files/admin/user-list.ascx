﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>
<inc:module runat="server" ID="login_check"></inc:module>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            var admin = (rbm.Members.Admin)Session["user"];
            hdAuthToken.Value = admin.authToken;
        }
        catch (Exception)
        {

            Response.Redirect("/admin");
        }

    }

    protected void bindData() 
    {
        
    }

</script>
<asp:Label ID="meta_title" runat="server" Text="User Admin" Visible="false" />

<inc:module runat="server" ID="script">
    <script type="text/javascript" src="/js/angular.min.js"></script>
    <script type="text/javascript" src="/js/ui-bootstrap.js"></script>
    <script type="text/javascript" src="/js/directives/dirPagination.js"></script>
    <script type="text/javascript" src="/js/angularFileUpload.js"></script>
    <link rel='stylesheet' href='/css/loadingBar.css' type='text/css' media='all' />
    <script type='text/javascript' src="/js/loadingBar.js"></script>
	<script type="text/javascript" src="/js/controllers/AdminUsers.js"></script>
</inc:module>

<div class="container" data-ng-app="roku" data-ng-controller="AdminUsersController">
        <asp:HiddenField runat="server" ID="hdUserID" />
        <asp:HiddenField runat="server" ID="hdAuthToken" />
                <div class="col-md-12">
                    <div class="row">
                    <div class="col-md-6">
                        <label>Search:</label>
                        <input type="text" class="form-control" data-ng-model="keyword.title" />
                    </div>
                    </div>

    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th data-ng-click="sort('apiUserID')">RBM USER-ID</th>
                                <th data-ng-click="sort('username')">RBM USERNAME</th>

                                <th data-ng-click="sort('email')">EMAIL</th>
                                <th data-ng-click="sort('saveDate')">CREATED ON: </th>
                                <th data-ng-click="sort('status')">STATUS: </th>

                                <th></th>
                            </tr>
                        </thead>                    
                        <tbody>
                            <tr dir-paginate="user in users | filter: keyword.title| orderBy:sortField:reverse | itemsPerPage: 25" current-page="currentPage" >

                                <td>{{user.apiUserID}}</td>

                                <td>{{user.username}}</td>
                                <td>{{user.email}}</td>
                                <td>{{user.saveDate | date: 'MM/dd/yyyy'}}</td>
                                <td>
                                    <a title="Disable User" data-ng-click="disableUser(user)" data-ng-if="user.status===1">(x)</a>
                                    <a title="Enable User" data-ng-click="activateUser(user)" data-ng-if="user.status!=1">(+)</a>
                                    {{user.status === 1 ? 'Active' : 'Disabled'}}

                                </td>

                                <td class="RightTxt">
                                    <%--<a title="Channel Details" data-ng-click="viewDetails(channel)" class="btn btn-default btn-xs"><span class="fa fa-info" aria-hidden="true"></span></a>--%>
                                    <%--<a title="Login As User" data-ng-click="loginAsUser(user)" class="btn btn-info btn-xs"><span class="fa fa-cog"></span></a>--%>
                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <dir-pagination-controls></dir-pagination-controls>
                        <div class="clearboth">&nbsp;</div>
                    </div>
                </div>

</div>




