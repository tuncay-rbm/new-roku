﻿<%@ Control Language="C#" ClassName="header" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        phAfterLogin.Visible = !(Session["user"] == null);
        lnkChannels.NavigateUrl = "/admin/channel-list";
        //lnkMedia.NavigateUrl = "/media?id=" + user.id + "&chk=" + rbm.MD5(rbm.getSett("hashkey") + user.id);
        lnkAccount.NavigateUrl = "/admin/user-list";
        hdPage.Value =RBMLib.lib.get("page");

    }

    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        Response.Redirect("/login");
    }

</script>

<asp:PlaceHolder ID="phAfterLogin" runat="server" Visible="False">
    <asp:HiddenField runat="server" ID="hdPage" />
	<!-- Backend Header Start -->
    <div id="header">
            <div class="HeaderLeft">
                <div class="CntLogo">
                    <a href="/" target="_self"><img src="../../images/logo.png" width="160" alt="Roku Channel Builder"/></a>
                </div>
            </div>
            <div class="HeaderRight">
                <ul class="menu">
                    <li class="logout"><a href="/logout" title="Logout"><span class="fa fa-power-off"></span></a></li>
                    <%--<li id="liStats" class="header-link"><a href="/statistics" title="Statistics"><span class="fa fa-bar-chart-o"></span><br />STATISTICS</a></li>--%>
                    <%--<li id="liMedia" class="header-link"><asp:HyperLink runat="server" ID="lnkMedia" CssClass="header-link"><span class="fa fa-file-video-o"></span><br />MEDIA</asp:HyperLink></li>--%>
                    <li id="liChannels" class="header-link"><asp:HyperLink runat="server" ID="lnkChannels" CssClass="header-link"><span class="fa fa-th-large"></span><br />CHANNELS</asp:HyperLink></li>
                    <li id="liAccount" class="header-link"><asp:HyperLink runat="server" ID="lnkAccount" CssClass="header-link"><span class="fa fa-user"></span><br />USERS</asp:HyperLink></li>
                </ul>
                <div class="clearboth">&nbsp;</div>
            </div>
            <div class="clearboth">&nbsp;</div>
        </div>
</asp:PlaceHolder>
<script>
    $(function () {
        var page = $('#hdPage').val();
        console.log('page = ', page);
        switch (page) {
            case 'user-list':
                $('.header-link').removeClass('active');
                $('#liAccount').addClass('active');
                break;
            case 'channel-list':
                $('.header-link').removeClass('active');
                $('#liChannels').addClass('active');
                break;

        }

    })
</script>
<div class="clearboth">&nbsp;</div>

<!-- Content Area Start -->
<div class="MainBg">