﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>


<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
    }
</script>

<div data-ng-controller="rokuPackagesController" class="cnt_wrapper FullWidth">
    <div data-ng-repeat="package in packages" class="HomePrice">
        <!-- PACKAGES -->
        <div class="CenterTxt">
                <h1>{{package.name}}</h1>
                <span class="fa fa-caret-down"></span><br />
                <div class="price">
                    <span>$</span> <strong>{{package.fee}}</strong><br />
                    <small>per month</small>
                </div>
                <div class="info">
                    <h2><strong>{{package.bandwidth + ' GB ' }}</strong>Bandwidth</h2>
                    <h2><strong>{{package.storage + ' GB ' }}</strong>Storage</h2>
                </div>
                <a href="/" class="btn btn-default">Select This Plan</a>
        </div>
        <div class="clearboth">&bnsp;</div>
    </div>
</div>


<script>
    landing.controller('rokuPackagesController', rokuPackagesController);
    function rokuPackagesController($scope, $http) {
        $scope.getPackages = function () {
            var url = 'api.ashx?action=get_packages';
            $http({
                url: url,
                method: 'GET',
                headers: { 'Content-Type': 'text/plain' }
            }).
            success(function (data, status, headers, config) {
                $scope.packages = [];
                angular.forEach(data.response.packages, function (item) {
                    $scope.packages.push(item);
                })
            }).
            error(function (data, status, headers, config) {
            });
        };
        $scope.getPackages();

    };
</script>