﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>
<script runat="server">
	protected void Page_Load(object sender, EventArgs e){
        
	}
</script>

<inc:module runat="server" ID="style">
    <link href="/css/index.css" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</inc:module>

<inc:module runat="server" ID="script">
    <script type="text/javascript" src="/js/angular.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/landing.js"></script>

</inc:module>

<asp:Label ID="meta_title" runat="server" Text="Roku Channel Builder" Visible="false" />

<body data-ng-app="landing" data-spy="scroll" data-target=".navigation">

    <div class="navigation">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand" id="LogoSmall"><img alt="Roku Channel Builder" src="images/logo_small.png" class="img-responsive"></a>
          </div>
          <div class="navbar-collapse collapse" id="navbar">
            <ul class="nav navbar-nav navbar-right">
              <li class="active no-show"><a href="home" id="MenuHome">Home</a></li>
              <li><a href="#about" id="MenuAbout">About</a></li>
              <li><a href="#features" id="MenuFeatures">Features</a></li>
              <li><a href="#plans" id="MenuPlans">Plans</a></li>
              <li><a href="#contact" id="MenuContact">Contact</a></li>
              <li><a href="/login" id="MenuLogin">Login</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </div>

    <div id="home">
    <a name="home" id="HomeCnt"></a>
      <div class="HomeContainer">
        <div class="HomeVideoOverlay">
          <div class="HomeCnt">
            <a href="/" id="logo"><img alt="Roku Channel Builder" src="images/logo.png" class="img-responsive logo"></a>
            <a href="/signup" target="_blank" title="Sign Up" class="btn" id="SignUp">GET YOUR CHANNEL NOW</a>
            <div class="ArrowDown">
              <a href="#about" title="Learn More" class="LearnMore" id="LearnMore"></a>
            </div>
          </div>
          <div class="VideoCnt">
            <video autoplay loop muted poster="images/video_img.jpg" id="homebgvid">
              <source src="videos/video_bg.webm" type="video/webm">
              <source src="videos/video_bg.mp4" type="video/mp4">
              <source src="videos/video_bg.ogv" type="video/ogg" />
            </video>
          </div> 
        </div>
      </div>
    </div>
    <!-- end home -->

      <div id="about" class="white_bg">
      <a name="about" id="AboutCnt"></a> 
        <div class="container">
          <div class="title_wrapper">
            <h1>Want your own channel, but don't know where to start?</h1>
            <h2>Look no further than <strong>Roku Channel Builder</strong> - it's the simple and affordable solution to get you on Roku!</h2>
          </div>
          <div class="cnt_wrapper">
            <div class="row">
              <div class="col-md-3 feature">
                <i class="fa fa-exchange blue_color"></i>
                <h3>Easy-to-use</h3>
                <p>The Roku Channel Builder makes creating your very own Roku Channel quick and easy.</p>
                <p>Name your channel, add your content and go!</p>
              </div>
              <div class="col-md-3 feature">
                <i class="fa fa-pencil-square-o red_color"></i>
                <h3>Make it your own</h3>
                <p>Add your logo and quickly update your channel to match your branding.</p>
                <p>The Roku Channel Builder makes managing your channel fun and effortless!</p>
              </div>
              <div class="col-md-3 feature">
                <i class="fa fa-files-o green_color"></i>
                <h3>Unlimited Videos</h3>
                <p>Add as many videos and categories as you want. You can even add an unlimited amount of subcategories...</p>
                <p>The sky is the limit!</p>
              </div>
              <div class="col-md-3 feature">
                <i class="fa fa-cogs grey_color"></i>
                <h3>Custom Plans</h3>
                <p>With pricing plans that equip you to get started right away, your channel will be up and running in no time.</p>
                <p>You can choose to build and download everything you need, or let us help you all the way!</p>
              </div>
              <div class="clearboth">&nbsp;</div>
            </div>
          </div>
        </div>
      </div>
      <!-- end about -->

      <div id="tagline" align="center">
        <div class="tagline_bg">
          <div class="container">
            <h1><span class="blue_color">Roku Channel Builder</span> makes building &amp; managing your own <span class="blue_color">Roku Channel</span> quick &amp; easy.</h1>
            <a href="#plans" title="Get Your Channel Now" class="btn" id="GetChannel">Get Your Channel Now</a>
          </div>
        </div>
      </div>
      <!-- end tagline -->

      <div id="features" class="white_bg">
      <a name="features" id="FeaturesCnt"></a> 
        <div class="container">
          <div class="title_wrapper">
            <h1>Build &amp; manage your very own Roku Channel quick &amp; easy</h1>
            <h2>With real-time updating and support for both Live and On Demand video streaming, you can rest easy, knowing that your content is available as soon as you add it to your channel.</h2>
          </div>
          <div class="cnt_wrapper">
            <div class="row">
              <div class="col-md-6">
                <h3>Manage In Real-Time</h3>
                <p>The real-time functionality is one of the most exciting features that really makes Roku Channel Builder an especially helpful resource. Once your channel is live, you can continue to use Roku Channel Builder to manage your content. Because it is feed-based, your changes are pushed live immediately - there's no additional installation or re-submission to Roku!</p>
              </div>
              <div class="col-md-6 graphic">
                <img alt="Manage In Real-Time" src="images/icon_1.png" class="img-responsive">
              </div>
              <div class="clearboth">&nbsp;</div>
            </div>
            <div class="row cnt_margin_top">
              <div class="col-md-6 graphic">
                <img alt="Supports Both Live and Video On Demand" src="images/icon_2.png" class="img-responsive">
              </div>
              <div class="col-md-6">
                <h3>Supports Both Live &amp; Video On Demand (VOD)</h3>
                <p>With Roku Channel Builder, you can publish all of your video on demand (VOD) and stream all of your live video content, directly to your custom channel! The best part is that you can manage all of your content right within Roku Channel Builder and after your initial installation, you never have to re-submit to Roku - any updates will be live right away!</p>
              </div>
              <div class="clearboth">&nbsp;</div>
            </div>
            <div class="row cnt_margin_top">
              <div class="col-md-6">
                <h3>Use Your Own Custom Design</h3>
                <p>Upload your own logo and use your company's custom colors to build your Roku channel. Roku Channel Builder makes it simple for you to use your own custom design so that your channel matches your brand.</p>
              </div>
              <div class="col-md-6 graphic">
                <img alt="Use Your Own Custom Design" src="images/icon_3.png" class="img-responsive">
              </div>
              <div class="clearboth">&nbsp;</div>
            </div>
            <div class="row cnt_margin_top">
              <div class="col-md-6 graphic">
                <img alt="Custom Development Options" src="images/icon_4.png" class="img-responsive">
              </div>
              <div class="col-md-6">
                <h3>Custom Development Options</h3>
                <p>If you need help adding custom features to your Roku channel, custom development is available. Please <a href="#contact" title="contact us" id="ContactUs">contact us</a> to learn more!</p>
              </div>
              <div class="clearboth">&nbsp;</div>
            </div>
          </div>   
        </div>     
      </div>
      <!-- end features -->

      <div id="plans" class="white_bg">
      <a name="plans" id="PlansCnt"></a> 
        <div class="container">
          <div class="title_wrapper">
            <h1>Run your Roku Channel in no time</h1>
            <h2>Our Pricing Plans will equip you to get started right away.<br />Choose to build and download everything you need, or let us us help you all the way!</h2>
          </div>
           <inc:module runat="server" ID="roku_packages"></inc:module>
        </div>  
        <p class="TxtCenter SmallMarginTop"><i>* Storage and Delivery is provided by RBM.tv</i></p>      
      </div>
      <!-- end plans -->

      <div id="contact" class="white_bg">
      <a name="contact" id="ContactCnt"></a> 
        <div class="container">
          <div class="title_wrapper">
            <h1>We would like to hear from you!</h1>
            <h2>We welcome your questions, comments, inquiries and other communications.<br />
            To contact us, please call or send us an email.</h2>
          </div>
          <div class="cnt_wrapper">
            <div class="row">
              <inc:module runat="server" ID="drop_a_line"></inc:module>
              <div class="col-md-6">
                <h3>About Us</h3>
                <p><strong><a href="http://www.rightbrainmedia.com" title="Right Brain Media" target="_blank" id="RBM">Right Brain Media, Inc</a></strong> is a pioneer in digital media and web technology offering products, services and solutions to businesses, ministries and non-profit organizations.</p>
                <div class="ContactInfo">
                  <p><strong><i class="fa fa-phone blue_dark_color"></i> &nbsp; Call Us:</strong> &nbsp; &nbsp; &nbsp;(407) 909 9718</p>
                  <p><strong><i class="fa fa-envelope blue_dark_color"></i> &nbsp; Email Us:</strong> &nbsp; <a href="mailto:sales@rightbrainmedia.com?subject=Contact Sales" id="ContactSales">sales@rightbrainmedia.com</a></p>
                  <p><strong><i class="fa fa-map-marker blue_dark_color"></i> &nbsp; Visit Us:</strong> &nbsp; &nbsp; &nbsp; <a href="http://goo.gl/maps/0IiCf" target="_blank" id="Address">130 S. Orange Av., Suite 210 Orlando, FL 32801</a></p>
                  <p><strong><i class="fa fa-comments blue_dark_color"></i> &nbsp; Connect:</strong> &nbsp; &nbsp;<a href="https://www.facebook.com/rightbrainmediatv" target="_blank" class="facebook" id="facebook">Facebook</a> &nbsp; | &nbsp; <a href="https://twitter.com/rightbrainmedia" target="_blank" class="twitter" id="twitter">Twitter</a> &nbsp; | &nbsp; <a href="http://www.linkedin.com/company/right-brain-media" target="_blank" class="linkedin" id="linkedin">LinkedIn</a> &nbsp; | &nbsp; <a href="https://plus.google.com/+RightBrainMediaOrlando/posts" target="_blank" class="googleplus" id="google">Google+</a> &nbsp; | &nbsp; <a href="http://rightbrainmedia.us10.list-manage.com/subscribe/post?u=c782354112ba3110f0ad4823f&id=56d4405abb" target="_blank" id="newsletter">Newsletter</a></p>
                </div>
                <div class="TwitterFeed">
                  <h3><i class="fa fa-twitter"></i> &nbsp; Right Brain Tweets</h3>
                  <div id="tweets"></div>
                </div>
              </div>
              <div class="clearboth">&nbsp;</div>
            </div>
          </div>
        </div>  
      </div>
      <!-- end contact -->

</body>