﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>
<%@ Import Namespace="RBMLib" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {
                var obj = (rbm.Members.stMembers)Session["user"];
                hdUserID.Value = obj.id;
                hdUsername.Value = obj.username;
                var catObj = rbm.Categories.getData(lib.get("categoryID"));
                hdCategoryID.Value = catObj.id;
                hdChannelID.Value = catObj.channelID;
                hdCategoryName.Value = catObj.title;
                hdAuthToken.Value = obj.authToken;
                //set back button
                lnkBackToCategories.NavigateUrl = "/category?channelID=" + catObj.channelID;
                lnkBackToCategories.Text = "/ Back To Categories";
            }
            catch
            {
                Response.Redirect("/logout");
            }
        }
    }


</script>

<inc:module ID="script" runat="server">
    <script type="text/javascript" src="/js/angular.min.js"></script>
    <script type="text/javascript" src="/js/ui-bootstrap.js"></script>
    <script type="text/javascript" src="/js/angularFileUpload.js"></script>
    <script type="text/javascript" src="/js/directives/dirPagination.js"></script>
    <script type="text/javascript" src="/js/controllers/PlaylistsController.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</inc:module>

<asp:Label ID="meta_title" runat="server" Text="Playlists" Visible="false" />

<inc:module runat="server" id="login_check" />

<div data-ng-app="roku" data-ng-controller="PlaylistController">

    <asp:HiddenField runat="server" ID="hdChannelID" />
    <asp:HiddenField runat="server"  ID="hdCategoryID"/>
    <asp:HiddenField runat="server" ID="hdUsername" />

    <asp:HiddenField runat="server" ID="hdUserID" />
    <asp:HiddenField runat="server" ID="hdAuthToken" />
    <asp:HiddenField runat="server" ID="hdCategoryName" />

    
    <div class="BlankBar">&nbsp;</div>
    <div class="PgTitle">
        <div class="FloatLeft">
            <h1><span class="fa fa-th-large"></span> &nbsp; Channels &nbsp; / &nbsp; Playlists &nbsp; <asp:hyperlink runat="server" ID="lnkBackToCategories" CssClass="back-button"></asp:hyperlink></h1>
        </div>
        <div class="FloatRight">
            <button type="button" class="btn-inst" data-toggle="popover" data-placement="left" data-content="Need to add How-To/Instructions for this page.">
              <span class="fa fa-question-circle"></span>
            </button>   
            <div class="clearboth">&nbsp;</div>  
        </div>
        <div class="clearboth">&nbsp;</div>
    </div>

    <div class="CntSubNav">
        <ul>
            <li><a data-ng-click="newPlaylist()" class="active">Create Playlist</a></li>
            <%--<li><a data-ng-click="playlistOrder()">Order Playlists</a></li>--%>
        </ul>
        <div class="clearboth">&nbsp;</div>
    </div>

    <div class="container-fluid TopBottomPad">

        <div class="col-md-12">
            <h2>{{categoryName}} Playlists</h2>
            <div class="table-responsive" data-ng-if="roku_playlists.length">
                <table class="table table-striped table-hover" data-ng-if="!addingVideos">
                    <thead>
                        <tr>
                            <th>TITLE</th>
                            <th>DESCRIPTION</th>
                            <th class="CenterTxt">ORDER UP</th>
                            <th class="CenterTxt">ORDER DOWN</th>
                            <th class="CenterTxt">VIDEOS</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr data-ng-repeat="playlist in roku_playlists">
                            <td>{{playlist.title}}</td>
                            <td>{{playlist.description}}</td>
                            <td class="CenterTxt">

                                <a data-ng-click="playlistUp($index, true)" class="sort">
                                    <span class="fa fa-arrow-up" aria-hidden="true"></span>
                                </a>
                            </td>
                            <td class="CenterTxt">
                                <a data-ng-click="playlistUp($index, false)" class="sort">
                                    <span class="fa fa-arrow-down" aria-hidden="true"></span>
                                </a>  

                            </td>
                            <td class="CenterTxt">{{playlist.videos.length}}</td>
                            <td class="RightTxt ">
                                <a title="Playlists" class="btn btn-info btn-xs" data-ng-click="addVideos(playlist)"><span class="fa fa-list" aria-hidden="true"></span></a>
                                <a title="Edit" class="btn btn-default btn-xs" data-ng-click="editPlaylist(playlist)"><span class="fa fa-pencil" aria-hidden="true"></span></a>
                                <a title="Delete" class="btn btn-danger btn-xs" data-ng-click="deletePlaylist(playlist)"><span class="fa fa-trash-o" aria-hidden="true"></span></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            <div class="pull-right" data-ng-if="orderChange">
                <a data-ng-click="cancelChanges()" class="btn btn-info">Cancel Changes</a>
                <a data-ng-click="savePlaylistOrder()" class="btn btn-default">Save Changes</a>
            </div>
            </div>
            <div class="table-responsive" data-ng-hide="roku_playlists.length">
                <p>
                    {{tips.no_playlists}}
                </p>
            </div>
        </div>

        <div class="col-md-12" data-ng-if="addingVideos">
            <div class="row">
                <div class="col-md-6">
                    <h3>
                        <div class="row">
                            <div class="col-md-5">
                                Your Video Library
                            </div>
                            <div class="col-md-6 pull-right Cnt-Search">
                                <input class="form-control" type="text" data-ng-model="keyword" placeholder="Search library"/>
                            </div>
                        </div>
                    </h3>
                    
                    <table class="table table-striped table-hover" data-ng-if="encoded_assets.length">
                        <thead>
                            <tr>
                                <th></th>
                                <th data-ng-click="sort('title')">NAME</th>
                                <th data-ng-click="sort('id')" class="CenterTxt">ADDED ON</th>
                                <%--<th class="CenterTxt">STATUS</th>--%>
                                <th class="CenterTxt complete" data-ng-click="sort('selected')">STATUS</th>

                                <th></th>
                            </tr>
                        </thead>                    
                        <tbody>
                            <tr dir-paginate="asset in encoded_assets | filter: keyword | orderBy:sortField:reverse | itemsPerPage: 10" current-page="currentPage" >
                                <td style="width:85px;height:50px">
                                    <img class="img-responsive" alt="{{asset.title}}" data-err-src="/images/NoImgAvailable.jpg" src="{{asset.image_path}}" />
                                </td>
                                <td>{{asset.title}}</td>
                                <td class="CenterTxt">{{asset.createDate | date: 'MM/dd/yyyy @ h:mma'}}</td>
                                <td class="CenterTxt complete">{{asset.selected ? 'Added' : ' '}}</td>
                                <td class="RightTxt">
                                    <a title="Edit" data-ng-click="editAsset(asset)" class="btn btn-default btn-xs"><span class="fa fa-pencil" aria-hidden="true"></span></a>
                                    <a title="Add" data-ng-click="addVideoToPlaylist(asset, $index)" class="btn btn-info btn-xs"><span class="fa fa-plus"></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <dir-pagination-controls></dir-pagination-controls>
                        <div class="clearboth">&nbsp;</div>
                    </div>
                    <table class="table table-striped table-hover" data-ng-show="encoded_assets.length === 0">
                        <tr>
                            <td>
                                {{tips.noEncodedAssets}}
                            </td>
                            <td>
                                <a data-ng-href="/media" class="btn btn-default">Media</a>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="col-md-5 pull-right">
                    <h3>{{selectedPlaylist.title}} Playlist Videos</h3>
                    <table class="table table-striped table-hover" data-ng-if="videosToOrder.length">
                        <thead>
                            <tr>
                                <th>NAME</th>
                                <%--<th class="CenterTxt">ORDER</th>--%>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr data-ng-repeat="video in videosToOrder | orderBy: 'order'">
                                <td>{{video.title}}--{{video.id}}--{{video.order}}</td> 
                                <%--<td class="CenterTxt">{{video.order}}</td>--%>
                                <td class="CenterTxt"><a data-ng-click="orderUp($index, true)" class="sort"><span class="fa fa-arrow-up" aria-hidden="true"></span></a></td>
                                <td class="CenterTxt"><a data-ng-click="orderUp($index, false)" class="sort"><span class="fa fa-arrow-down" aria-hidden="true"></span></a></td>
                                <td class="RightTxt"><a title="Delete" data-ng-click="removeVideoFromPlaylist(video)" class="btn btn-danger btn-xs"><span class="fa fa-trash-o"></span></a></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-striped table-hover" data-ng-if="videosToOrder.length === 0">
                        <tr>
                            <td>{{tips.noVideosToOrder}}</td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="clearboth">&nbsp;</div>
            <hr />
            <div class="pull-right">
                <a data-ng-click="cancelChanges()" class="btn btn-info">Back</a>
                <a data-ng-click="savePlaylistVideos()" class="btn btn-default">Save Changes</a>
            </div>
            <div class="clearboth">&nbsp;</div>
        </div>

    </div>
    <!-- end container-fluid -->

</div>
<!-- End Playlists -->

<script type="text/javascript">
    $("[data-toggle=popover]").popover();
</script>