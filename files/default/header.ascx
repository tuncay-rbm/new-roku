﻿<%@ Control Language="C#" ClassName="header" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] == null)
        {
            phBeforeLogin.Visible = true;
        }
        else {
            phAfterLogin.Visible = true;
            
            try
            {
                var user = (rbm.Members.stMembers)Session["user"];
                lnkChannels.NavigateUrl = "/channels";
                lnkMedia.NavigateUrl = "/media";
                lnkAccount.NavigateUrl = "/account";
                hdPage.Value = RBMLib.lib.get("section");
            }
            catch (Exception)
            {
                //Response.Redirect("/login");
            }
        }


    }

    protected void lnkLogout_Click(object sender, EventArgs e)
    {
        Response.Redirect("/login");
    }

</script>

<asp:PlaceHolder ID="phBeforeLogin" runat="server" Visible="False">

</asp:PlaceHolder>
<asp:PlaceHolder ID="phAfterLogin" runat="server" Visible="False">
    <asp:HiddenField runat="server" ID="hdPage" />

	<!-- Backend Header Start -->
    <div id="header">
            <div class="HeaderLeft">
                <div class="CntLogo">
                    <a href="/" target="_self"><img src="../../images/logo.png" width="160" alt="Roku Channel Builder"/></a>
                </div>
            </div>
            <div class="HeaderRight">
                <ul class="menu">
                    <li class="logout"><a href="/logout" title="Logout"><span class="fa fa-power-off"></span></a></li>
                    <%--<li id="liStats" class="header-link"><a href="/statistics" title="Statistics"><span class="fa fa-bar-chart-o"></span><br />STATISTICS</a></li>--%>
                    <li id="liMedia" class="header-link"><asp:HyperLink runat="server" ID="lnkMedia" CssClass="header-link"><span class="fa fa-file-video-o"></span><br />MEDIA</asp:HyperLink></li>
                    <li id="liChannels" class="header-link"><asp:HyperLink runat="server" ID="lnkChannels" CssClass="header-link"><span class="fa fa-th-large"></span><br />CHANNELS</asp:HyperLink></li>
                    <li id="liAccount" class="header-link"><asp:HyperLink runat="server" ID="lnkAccount" CssClass="header-link"><span class="fa fa-user"></span><br />ACCOUNT</asp:HyperLink></li>
                </ul>
                <div class="clearboth">&nbsp;</div>
            </div>
            <div class="clearboth">&nbsp;</div>
        </div>
</asp:PlaceHolder>
<script>
    $(function () {
        var page = $('#hdPage').val();
        switch (page) {
            case 'channels':
                $('.header-link').removeClass('active');
                $('#liChannels').addClass('active');
                break;
            case 'media':
                $('.header-link').removeClass('active');
                $('#liMedia').addClass('active');
                break;
            case 'statistics':
                $('.header-link').removeClass('active');
                $('#liStats').addClass('active');
                break;
            case 'account':
                $('.header-link').removeClass('active');
                $('#liAccount').addClass('active');
                break;
        }

    })
</script>
<div class="clearboth">&nbsp;</div>

<!-- Content Area Start -->
<div class="MainBg">