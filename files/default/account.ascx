﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {
                var obj = (rbm.Members.stMembers)Session["user"];
                hdUserID.Value = obj.id;
                hdApiUserID.Value = obj.apiUserID;
                var catObj = rbm.Categories.getData(RBMLib.lib.get("categoryID"));
                hdCategoryID.Value = catObj.id;
                hdChannelID.Value = catObj.channelID;
                hdAuthToken.Value = rbm.getToken();
            }
            catch
            {
                Response.Redirect("/logout");
            }
        }

    }

    protected void lnkSubmit_Click(object sender, EventArgs e)
    {
        string json = rbm.API.UpdateCreditCard(hdApiUserID.Value, hdAuthToken.Value, hdPackageID.Value, txtCardNumber.Text, txtCVC.Text, txtExpiration.Text);
        dynamic d = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
        if (d.code != 200)
        {
            lblErr.Text = "There was an error updating your account. Please verify your card information and try again or contact support: ";
        }
        else {
            lblErr.Text = "Your card information has been successfully updated. ";
            txtCardNumber.Text = "";
            txtCVC.Text = "";
            txtExpiration.Text = "";
        }
        hdShowPlans.Value = "1";
        lblErr.Visible = true;
    }
    
    
</script>
<inc:module runat="server" id="login_check" />

<inc:module ID="script" runat="server">
    <script type="text/javascript" src="js/jquery.payment.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="/js/angular.min.js"></script>
    <script type="text/javascript" src="/js/ui-bootstrap.js"></script>
    <script type="text/javascript" src="/js/controllers/AccountController.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</inc:module>

<asp:Label ID="meta_title" runat="server" Text="Account" Visible="false" />


<div data-ng-app="roku" data-ng-controller="AccountController">

<asp:HiddenField runat="server" ID="hdChannelID" />
<asp:HiddenField runat="server"  ID="hdCategoryID"/>
<asp:HiddenField runat="server" ID="hdUserID" />
<asp:HiddenField runat="server" ID="hdApiUserID" />

<asp:HiddenField runat="server" ID="hdAuthToken" />
<asp:HiddenField runat="server" ID="hdPackageID" />
    <asp:HiddenField runat="server" ID="hdShowPlans" Value="0" />

    <div class="BlankBar">&nbsp;</div>
    <div class="PgTitle">
        <div class="FloatLeft">
            <h1><span class="fa fa-user"></span> &nbsp; Account &nbsp; <asp:hyperlink runat="server" ID="lnkBackToChannels" CssClass="back-button"></asp:hyperlink></h1>
        </div>
        <div class="FloatRight">
            <button type="button" class="btn-inst" data-toggle="popover" data-placement="left" data-content="Need to add How-To/Instructions for this page.">
              <span class="fa fa-question-circle"></span>
            </button>   
            <div class="clearboth">&nbsp;</div>  
        </div>
        <div class="clearboth">&nbsp;</div>
    </div>

    <div class="CntSubNav">
        <ul>
            <li><a id="" class="btn btn-info btn-sm" data-ng-class="{active: selectedTab==1}" data-ng-click="viewTab(1)">User Information</a></li>
            <li><a id="" class="btn btn-info btn-sm" data-ng-class="{active: selectedTab==2}" data-ng-click="viewTab(2)">Plans &amp; Payment</a></li>
        </ul>
        <div class="clearboth">&nbsp;</div>
    </div>
    
    <div class="container-fluid TopBottomPad">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-right" data-ng-show="false">
                        <a data-ng-click="cancelModal()" class="btn btn-danger btn-sm">Cancel Account</a>
                    </div>
                    <div class="clearboth">&nbsp;</div>
                </div>
            </div>
        </div>

                <div class="col-md-12" data-ng-hide="selectedTab != 1">
                    <div class="row">
                        <div class="col-md-4">
                            <!-- FIRST NAME -->
                            <div class="form-group">
                                <label>First Name:</label>
                                <input type="text" data-ng-model="user.first_name" class="form-control">
                            </div>
                            <!-- USERNAME -->
                            <div class="form-group">
                                <label>Username:</label>
                                <input type="text" data-ng-model="user.username" data-ng-disabled="true" class="form-control" disabled="disabled">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- LAST NAME -->
                            <div class="form-group">
                                <label>Last Name:<!-- {{user.plan.id}} --></label>
                                <input type="text" data-ng-model="user.last_name" class="form-control">
                            </div>
                            <!-- EMAIL -->
                            <div class="form-group">
                                <label>Email:</label>
                                <input type="email" data-ng-model="user.email" data-ng-disabled="true" class="form-control" disabled="disabled">
                            </div>
                        </div>
                        <div class="clearboth">&nbsp;</div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div data-ng-hide="userSpin">
                                <a class="btn btn-default btn-sm" data-ng-click="update(user)">Update</a>
                                <p>{{nameUpdateMessage}}</p>
                            </div>
                            <div data-ng-hide="!userSpin">
                                <img src="/images/loading.gif" />
                            </div>
                            <div class="clearboth">&nbsp;</div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Current Password:</label>
                                <input type="password" data-ng-model="currentPassword" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                        </div>
                        <div class="clearboth">&nbsp;</div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>New Password:</label>
                                <input type="password" data-ng-model="newPassword" class="form-control" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Confirm Password:</label>
                                <input type="password" data-ng-model="confirmPassword" class="form-control"  />

                            </div>
                        </div>
                        <div class="clearboth">&nbsp;</div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" >
                            <div data-ng-hide="passwordSpin">
                                <a class="btn btn-default btn-sm" data-ng-click="updatePassword()" data-ng-disabled="noMatch">Update Password</a>
                            </div>
                            <div data-ng-hide="!passwordSpin">
                                <img src="/images/loading.gif" />

                            </div>
                            <div data-ng-if="noMatch" class="erro">
                                    {{passwordMessage}}
                                </div>
                            <div class="clearboth">&nbsp;</div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" data-ng-hide="selectedTab != 2">

                    <div  data-ng-repeat="package in packages" class="HomePrice">
                        <!-- PACKAGES -->
                        <div class="CenterTxt">
                            <h1 data-ng-if="selectedPackage.id===package.id">Current Plan</h1>
                            <h1>{{package.name}}</h1>
                            <span class="fa fa-caret-down"></span><br />
                        <div class="price">
                            <span>$</span> <strong>{{package.fee}}</strong><br />
                            <small>per month</small>
                        </div>
                        <div class="info">
                            <h2><strong>{{package.bandwidth + ' GB ' }}</strong>Bandwidth</h2>
                            <h2><strong>{{package.storage + ' GB ' }}</strong>Storage</h2>
                        </div>
                            <a data-ng-if="selectedPackage.id != package.id && upcomingPackage != package.id" data-ng-click="choosePackage(package)" class="btn btn-default">Change to {{package.name}}</a>
                        </div>

                        <div class="clearboth">&nbsp;</div>
                    </div>
                    <div class="clearboth">&nbsp;</div>

                <hr>

                            <!-- CARD INFO -->
                            <div id="cardForm">
                                <span id="pkgErr" class="error"></span>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Card Number:</label>
                                            <asp:TextBox ID="txtCardNumber" runat="server" placeholder ="Card Number" cssClass="form-control"></asp:TextBox>
                                            <span id="cardErr" class="error"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Security Code:</label>
                                        <asp:TextBox ID="txtCVC" runat="server" placeholder="Security Code" cssClass="form-control"></asp:TextBox>
                                        <span id="cvcErr" class="error"></span>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Expiration Date (MM/YY):</label>
                                        <asp:TextBox ID="txtExpiration" runat="server" placeholder="Expiration Date (MM/YY)" cssClass="form-control"></asp:TextBox>
                                        <span id="expErr" class="error"></span>
                                    </div>
                                </div>
                            </div>
                            <!-- END CARD INFO -->
                    <div class="form-group">
                                <asp:Label ID="lblErr" runat="server" class="error"></asp:Label>                          

                    </div>
                    <div class="form-group"  data-ng-hide="cardSpin" >

                                <asp:button runat="server" ID="lnkSubmit" CssClass="btn btn-default btn-sm" OnClick="lnkSubmit_Click" Text="Update Credit Card"></asp:button>
                    </div>
                    <div data-ng-hide="!cardSpin">
                        <img src="/images/loading.gif" />
                        <p>Updating your card information...</p>
                    </div>

    </div>
</div>
</div>
<script type="text/javascript">
    $("[data-toggle=popover]").popover();
</script>