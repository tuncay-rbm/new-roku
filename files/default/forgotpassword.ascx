﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="Stripe" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="RBMLib" %>

<%@ Import Namespace="System.Net.Mail" %>
<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!lib.isNull(lib.get("hash")))
        {
            hdHash.Value = lib.get("hash");

        }
        else {
            Response.Redirect("/login");
        }
        
    }

    protected void lnkSubmit_Click(object sender, EventArgs e)
    {

    }
</script>
<inc:module ID="script" runat="server">
    <script type="text/javascript" src="/js/angular.min.js"></script>
    <script type="text/javascript" src="/js/ui-bootstrap.js"></script>
</inc:module>
<asp:Label ID="meta_title" runat="server" Text="Reset Password" Visible="false" />




<div data-ng-app="roku" data-ng-controller="resetController">
    <asp:HiddenField runat="server" ID="hdHash" />
        <div id="landing">
      <div class="LandingContainer">
        <div class="VideoOverlay">
          <div class="LandingCnt">
            <a href="/"><img alt="Roku Channel Builder" src="/images/logo.png" alt="Roku Channel Builder" class="img-responsive LandingLogo"></a>




    <div class="CustomForm" data-ng-if="!success && !expired">
        <div class="form-group">
            <label>Email Address: </label>
            <input type="text" class="form-control" data-ng-model="reset.email" />
        </div>
        <div class="form-group">
            <label>New Password:</label>
            <input type="text" class="form-control" data-ng-model="reset.password" />
        </div>
        <div class="form-group">
            <a data-ng-click="submitNewPassword()" class="btn btn-default btn-block">Reset Password</a>
        </div>
    </div>
    <div class="CustomForm" data-ng-if="!success && expired">
        <div data-ng-if="!resetSent">
            <p>
                    Your password was not reset. Verify the email address and try again or submit a new reset request. 

            </p>
                   <div class="form-group">
                    <a data-ng-click="tryAgain()" class="btn btn-default btn-block">Try Again</a>
        </div>
                   <div class="form-group">
                    <a data-ng-click="forgotPassword()" class="btn btn-default btn-block">New Reset Request</a>
        </div>

        </div>
        <div data-ng-if="resetSent">
            An email with instructions to reset your password has been sent. 
        </div>
    </div>
    <div class="CustomForm" data-ng-if="success">
            Your password has been reset. 
            <a href="/login" class="btn btn-default">Login</a>
    </div>
          </div>


        </div>
      </div>
    </div>

</div>
<script>
    jQuery(function () {
        jQuery('#txtEmail').focus();
    })
    angular.module('roku', ['ui.bootstrap']).controller('resetController', resetController);
    function resetController($scope, $filter, $http, $modal) {
        //console.log('here');
        $scope.hash = jQuery('#hdHash').val();
        $scope.reset = {
            email: '',
            password: ''
        }
        $scope.success = false;
        $scope.expired = false;
        $scope.tryAgain = function () {
            $scope.reset = {
                email: '',
                password: ''
            }
            $scope.success = false;
            $scope.expired = false;
        }
        $scope.submitNewPassword = function () {
            var url = '/api.ashx?action=commit_new_password&email=' + $scope.reset.email + '&password=' + $scope.reset.password + '&hash=' + $scope.hash;
            $http({
                method: 'POST',
                url: url,
                headers: { 'Content-Type': 'text/plain' }
            }).
            success(function (data, status, headers, config) {
                if (data.code != 200) {
                    $scope.success = false;
                    $scope.expired = true;
                } else {
                    $scope.success = true;
                    $scope.expired = false;
                }
            }).
            error(function (data, status, headers, config) {
                $scope.success = false;
                $scope.expired = true;
            });


        }
        $scope.login = function () {
            window.location.href = '/login';
        }
        $scope.forgotPassword = function () {
            $scope.editModal('lg');
        };
        $scope.testEmail = function () {


            var url = 'api.ashx?action=send_custom_email&email';
            $http({
                method: 'POST',
                url: url,
                headers: { 'Content-Type': 'text/plain' }
            }).
            success(function (data, status, headers, config) {
                console.log(data);
            }).
            error(function (data, status, headers, config) {
                console.log(data);

            });
        }
        $scope.editModal = function (size) {
            var modalInstance = $modal.open({
                templateUrl: '/templates/ForgotPassword.html',
                controller: function ($scope, $modalInstance, $sce) {
                    $scope.email = '';
                    //https://dev-api.rbmtv.com/v5/forgot_password_request?email=asdf@asdf.com&username=asdfasdfasdf
                    $scope.validForm = true;
                    $scope.resetSent = false;

                    $scope.sendReset = function () {
                        var resetEmail = $scope.email;
                        if (resetEmail == '' || resetEmail == 'undefined' || resetEmail == null) {
                            $scope.validForm = false;

                        } else {
                            $scope.validForm = true;
                            console.log($scope.email);
                            var url = 'api.ashx?action=reset_password&email=' + $scope.email;
                            $http({
                                method: 'POST',
                                url: url,
                                headers: { 'Content-Type': 'text/plain' }
                            }).
                            success(function (data, status, headers, config) {
                                modalInstance.close();
                            }).
                            error(function (data, status, headers, config) {
                                $scope.error = true;
                            });
                        }

                    }
                    $scope.closeModal = function () {
                        modalInstance.close();
                    }

                },
                size: size,
                resolve: {

                }
            });

            modalInstance.result.then(function () {
                $scope.login();
            }, function () {
            });
        };

    }

</script>