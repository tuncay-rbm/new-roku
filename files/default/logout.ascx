﻿<%@ Control Language="C#" AutoEventWireup="true" %>

<script runat="server">

	protected void Page_Load(object sender, EventArgs e)
	{
        Session.Abandon();
		Response.Redirect("/login");
	}
	
</script>