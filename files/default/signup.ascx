﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="Stripe" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="System.Net.Mail" %>
<script runat="server">

protected void Page_Load(object sender, EventArgs e)
{


}
protected void lnkSubmit_Click(object sender, EventArgs e)
{
    try
    {
        //validate user info
        var obj = new rbm.API.stSignup();
        obj.firstname = "ChannelBuilder";
        obj.lastname = "ChannelBuilder";
        obj.email = Request.Form["email"];
        obj.username = Request.Form["email"].Replace(".", "dot").Replace("@", "at");
        obj.password = Request.Form["password"];
        obj.passwordconfirm = Request.Form["passwordconfirm"];
        obj.cardNumber = txtCardNumber.Text;
        obj.cardCVC = txtCVC.Text;
        obj.cardExp = txtExpiration.Text;
        string prodTestPackageID = RBMLib.lib.getSett("tempPackage");
        if (prodTestPackageID == "21")
        {

            obj.packageID = prodTestPackageID;
        }
        else {
            obj.packageID = Request.Form["hdPackageID"];
        
        }

        obj = rbm.API.ValidateSignupFields(obj);
        if (!obj.isValid)
        {
            lblErr.Text = "There was an issue signing up. Please try again.";
        }
        else
        {
            obj = rbm.API.CreateUser(obj);
            if (!obj.isValid)
            {
                lblErr.Text = obj.errMsg;
            }
            else
            {
                //get an auth token
                obj = rbm.API.AuthenticateUser(obj);

                //get user v5 id
                obj = rbm.API.AccountDetails(obj);
                if (!obj.isValid)
                {
                    lblErr.Text = "There was an issue signing up. Please try again.";
                }
                else
                {
                    //use obj to create roku user
                    var user = rbm.Members.add(obj);
                    //use user to create channell                
                    var channelID = rbm.Channel.add(user.id);
                    //create the default channel theme
                    var theme = new rbm.Themes.stTheme();
                    theme.category_bg_color = "#ffffff";
                    theme.category_list_style = "flat-category";
                    theme.category_txt_one = "#000000";
                    theme.category_txt_two = "#000000";
                    theme.channelID = rbm.toStr(channelID);
                    theme.id = "";
                    rbm.Themes.CreateChannelTheme(theme);
                    //create user session
                    Session["user"] = user;

                    //create encode profiles
                    rbm.API.EncodeProfile.CreateRokuGroup(user.id);
                    rbm.API.EncodeProfile.CreateRokuLow(user.id);
                    rbm.API.EncodeProfile.CreateRokuMedium(user.id);
                    rbm.API.EncodeProfile.CreateRokuHigh(user.id);
                    rbm.API.EncodeProfile.PushProfilesToGroup(user.id);

                    //redirect to channel                 
                    Response.Redirect("/channels");
                }
            }
        }
    }
    catch (Exception ex)
    {
        rbm.Log(DateTime.Now + "\tAction:" + "Signup" + "\tData:" + ex.ToString());
        lblErr.Text = "There was an issue signing up. Please try again.";
    }

    }
    </script>
<asp:Label ID="meta_title" runat="server" Text="Roku Channel Builder" Visible="false" />

<inc:module ID="Module1" runat="server">
    <script type="text/javascript" src="js/jquery.payment.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="/js/angular.min.js"></script>
    <script type="text/javascript" src="/js/Controllers/SignupController.js"></script>
</inc:module>
<style>
    .hidden {
        display:none;
    }
</style>
<div data-ng-app="roku" data-ng-controller="SignupController">
    <div id="landing">
      <div class="LandingContainer">
        <div class="VideoOverlay">
          <div class="LandingCnt">
            <a href="/"><img alt="Roku Channel Builder" src="images/logo.png" alt="Roku Channel Builder" class="img-responsive LandingLogo"></a>
            
            <div class="CustomForm">
                <form role="form" _lpchecked="1">
                    <div class="steps-header">
                          <ul id="progressbar">
                            <li data-ng-class="{active: step.user}" data-ng-click="nextStep(0)">Account Setup</li>
                            <li data-ng-class="{active: (step.packages && !exists)}" data-ng-click="nextStep(1)">Choose Plan</li>
                            <li data-ng-class="{active: (hasPackage || step.card) && !(step.packages || step.user)}" data-ng-click="nextStep(2)">Check Out</li>
                          </ul>
                    </div>
                    <div class="user-info" data-ng-class="{hidden: !step.user}">
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input id="txtEmail" type="email" class="form-control" name="email" data-ng-model="formData.email" placeholder="Please enter your email address">
                            <span class="error MarginTopSmall" data-ng-if="validation.error == true">{{validation.email}}</span>
                        </div>
                        <div class="form-group">
                            <label for="company">Password:</label>
                            <input type="password" name="password" data-ng-model="formData.password" class="form-control" >
                            <span class="error MarginTopSmall" data-ng-if="validation.error == true">{{validation.password}}</span>
                        </div>
                        <div class="form-group">
                            <label for="company">Confirm Password:</label>
                            <input type="password" name="passwordconfirm" data-ng-model="formData.passwordconfirm" class="form-control">
                            <span class="error MarginTopSmall" data-ng-if="validation.error == true">{{validation.passwordconfirm}}</span>
                        </div>
                        <div data-ng-if="exists">
                            <div class="error">
                                {{errMsg[0]}}
                                {{errMsg[1]}}
                            </div>
                        </div>
                        <a class="btn btn-default btn-block" data-ng-click="packageOptions()">Next</a>
                    <!--END USER INFO -->
                    </div>
                    <div class="package-options" data-ng-class="{hidden: !(step.packages && !exists)}">
                        <div  data-ng-repeat="package in packages">
                            <!-- PACKAGES -->
                            <a class="packages" data-ng-click="setPackage(package)">
                                <div class="package">
                                    <div class="row">
                                        <div class="col-md-6 CenterTxt">
                                            <div class="pck_decor">
                                                <span>{{package.name}}</span><br />
                                                <span class="price">${{package.fee}}<small>/monthly</small></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="pck_info">
                                                <strong>{{package.bandwidth + ' GB ' }}</strong>Bandwidth
                                            </div>
                                            <div class="pck_info">
                                                <strong>{{package.storage + ' GB ' }}</strong>Storage
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <!-- CARD INFO -->
                    <div id="cardForm" class="form-group" data-ng-class="{hidden: !hasPackage || !step.card}" >
                        <input type="hidden" id="hdPackageID" name="hdPackageID" value="{{selectedPackage.id}}" />
                        <span id="selectedPackage" class="SelectedPck">{{selectedPackage.name}}</span>
                        <span id="pkgErr" class="error"></span>
                        <div class="form-group">
                            <label>Card Number:</label>
                            <asp:TextBox ID="txtCardNumber" runat="server" placeholder ="Card Number" cssClass="form-control"></asp:TextBox>
                            <span id="cardErr" class="error"></span>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Security Code:</label>
                                    <asp:TextBox ID="txtCVC" runat="server" placeholder="Security Code" cssClass="form-control"></asp:TextBox>
                                    <span id="cvcErr" class="error"></span>
                                </div>
                                <div class="col-md-6">
                                    <label>Expiration Date (MM/YY):</label>
                                    <asp:TextBox ID="txtExpiration" runat="server" placeholder="Expiration Date (MM/YY)" cssClass="form-control"></asp:TextBox>
                                    <span id="expErr" class="error"></span>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <asp:CheckBox ID="cbAgree" runat="server" TextAlign="Right" cssClass="TermsTxt" Text="I have read the" /><a href="http://rokuchannelbuilder.com/TermsOfUse.aspx" target="_blank"> Terms Of Use</a>
                        </div>
                        <asp:Label ID="lblErr" runat="server"></asp:Label>       
                        <div id="pnlSignup" data-ng-click="showSpin()" data-ng-show="!spinning">
                            <asp:button runat="server" ID="lnkSubmit" CssClass="btn btn-default btn-block" OnClick="lnkSubmit_Click" Text="SIGN ME UP!"></asp:button>
                        </div>                   
                        <div id="pnlSpin" data-ng-show="spinning" >
                            
                            <img src="/images/loading.gif" />
                        </div>
                    </div>
                    <!-- END CARD INFO -->

                    <!-- Error -->
                    <div data-ng-if="error==true">
                        <div class="error">{{errorMsg}}</div>
                    </div>
                    <!-- Success -->
                    <div data-ng-if="success==true">
                        <div class="error">{{successMsg}}</div>
                    </div>
                </form>
            </div>
          </div>
          <div class="VideoCnt">
            <video autoplay loop muted poster="video_img.jpg" id="bgvid">
              <source src="videos/video_bg.webm" type="video/webm">
              <source src="videos/video_bg.mp4" type="video/mp4">
              <source src="videos/video_bg.ogv" type="video/ogg" />
            </video>
          </div> 
        </div>
      </div>
    </div>

</div>
<script>

</script>