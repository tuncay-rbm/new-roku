﻿<%@ Control Language="C#" ClassName="footer" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>

</div>
<!-- Content Area End -->

<!-- Footer Area Start -->
<div id="footer" class="footer">
	<div class="row">
		<div class="col-md-6">
			Copyright &copy; 2015 <a href="http://www.rightbrainmedia.com" title="Right Brain Media" target="_blank">RiGHT BRAiN MEDiA</a> &nbsp; | &nbsp; Powered by V5 Media Engine &nbsp; | &nbsp; <a target="_blank" href="http://rightbrainmedia.com/portals/0/docs/RBM_PrivacyStatement.pdf">Privacy</a> &nbsp; | &nbsp; <a target="_blank" href="http://rightbrainmedia.com/portals/0/docs/RBM_TermsAndConditions.pdf">Terms</a>
		</div>
		<div class="col-md-6" align="right">
			<strong>RBM Network:</strong> &nbsp; <a href="http://www.rbmtv.com" title="RBMtv" target="_blank">RBM.tv</a> &nbsp; | &nbsp; <a href="http://www.viddyup.com" title="ViddyUP" target="_blank">ViddyUP</a> &nbsp; | &nbsp; <a href="http://readycastapp.com/" title="Ready Cast App" target="_blank">ReadyCastApp</a> &nbsp; | &nbsp; <a href="http://streamorigin.com/" title="Stream Origin" target="_blank">Stream Orgin</a> &nbsp; | &nbsp; <a href="http://www.snapandplay.com/" title="Snap and Play" target="_blank">Snap &amp; Play</a>
		</div>
		<div class="clearboth">&nbsp;</div>
	</div>

</div>
<!-- Footer Area End -->