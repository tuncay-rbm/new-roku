﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>
<%@ Import Namespace="RBMLib" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!lib.isNull(lib.get("action")))
        {
            switch (lib.get("action"))
            {
                case "cloaking":
                    var user = rbm.Members.getData(lib.get("userID"));
                    user.isAdmin = "1";
                    user.authToken = lib.get("token");
                    Session["user"] = user;
                    hdUserID.Value = user.id;
                    litTest.Text = lib.get("userID");
                    break;
            }
        }
        else
        {
            try
            {
                var user = (rbm.Members.stMembers)Session["user"];
                hdUserID.Value = user.id;
                hdAuthToken.Value = user.authToken; 
            }
            catch
            {
                Response.Redirect("/logout");
            }
        }

    }

</script>
<asp:Label ID="meta_title" runat="server" Text="Channels" Visible="false" />

<inc:module runat="server" ID="script">
    <script type="text/javascript" src="/js/angular.min.js"></script>
    <script type="text/javascript" src="/js/ui-bootstrap.js"></script>
    <script type="text/javascript" src="/js/angularFileUpload.js"></script>
    <script type="text/javascript" src="/js/bootstrap-colorpicker-module.js"></script>
    <script type="text/javascript" src="js/controllers/ChannelsController.js"></script>
    <script src="/js/bootstrap.min.js"></script>

</inc:module>

<div data-ng-app="roku" data-ng-controller="ChannelsController">
    <asp:HiddenField runat="server" ID="hdAuthToken" />
    <div class="BlankBar">&nbsp;</div>
    <div class="PgTitle">
        <div class="FloatLeft">
            <h1><span class="fa fa-th-large"></span> &nbsp; Channels<asp:Literal runat="server" ID="litTest"></asp:Literal></h1>
        </div>
        <div class="FloatRight">
            <button type="button" class="btn-inst"
            data-toggle="popover"
            data-placement="left"
            title="Channels Help"
            html="true">
              <span class="fa fa-question-circle"></span>
            </button>   
            <div class="clearboth">&nbsp;</div>  
        </div>
        <div class="clearboth">&nbsp;</div>
    </div>
    <div class="container-fluid TopBottomPad">
        <asp:HiddenField runat="server" ID="hdUserID" />
        <div class="col-md-12 channels">
            <div class="row">
                <div class="col-xs-4 col-lg-3" data-ng-repeat="channel in channels" data-ng-if="channels.length">
                    <div class="img-thumbnail">
                        <div class="ChannelInfo">
                            <img alt="{{channel.title}}" data-err-src="/images/new_category_image.png" src="{{channel.hd_path}}" width="100%" height="210px" />
                            <div class="ChannelBtns">
                                <a class="btn btn-default btn-sm" data-ng-click="editChannel(channel)"><span class="fa fa-pencil" aria-hidden="true"></span> &nbsp; Edit</a>&nbsp; &nbsp; &nbsp;<a class="btn btn-danger btn-sm" data-ng-click="deleteChannel(channel)"><span class="fa fa-trash-o" aria-hidden="true"></span> &nbsp; Delete</a>
                                <div class="clearboth">&nbsp;</div> 
                            </div>
                        </div>
                        <div class="ChannelTitle">
                            <h4>{{channel.title}} &nbsp; <a href="/category?channelID={{channel.id}}" class="btn-add" title="Add Content"><span class="fa fa-plus-circle"></span></a></h4>
                            <!-- <p>{{channel.description}}</p> -->
                        </div>
                        <div data-ng-if="!countingMedia" class="ChannelFiles">
                            <span>{{channel.mediaCount}}</span>
                            <br />
                            Media Files
                        </div>
                        <div data-ng-if="countingMedia" class="ChannelFiles">
                            <img src="/images/loading.gif" />
                        </div>
                    </div>
                </div>
                <div data-ng-click="addChannel()" class="add-channel col-xs-4 col-lg-3">
                    <div class="img-thumbnail">
                        <div class="NewChannel">
                            BUILD NEW CHANNEL<br />
                            <span class="fa fa-plus-circle" aria-hidden="true"></span> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="popoverHiddenContent" style="display:none;">
    <p>This is where all of your ROKU Channels are located.</p>
    <p>You may create an unlimited number of ROKU Channels.</p>
    <p>When you are finished designing your channel, you can submit it to the ROKU channel store by clicking 'Edit' > 'Tools' > 'Submit my Channel!'.</p>
    <p>To edit the content of your channel, click on the <span class="fa fa-plus-circle" style="color:#c9ced3;"></span> next to the channel name.</p>
</div>

<script type="text/javascript">
    $("[data-toggle=popover]").popover({
        html : true,
        content: function() {
         return $('#popoverHiddenContent').html();
        }
     });

</script>