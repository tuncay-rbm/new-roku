﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
            try
            {
                var obj = (rbm.Members.stMembers)Session["user"];
                hdUserID.Value = obj.id;
                hdChannelID.Value = rbm._get("channelID");
                var channel = rbm.Channel.getData(rbm._get("channelID"));
                lnkBackToChannels.Text = "/ Back To All Channels";
                lnkBackToChannels.NavigateUrl = "/channels";
            }
            catch
            {
                Response.Redirect("logout");
            }            
        }
    }
</script>
<asp:Label ID="meta_title" runat="server" Text="Category" Visible="false" />
<inc:module runat="server" ID="login_check"></inc:module>
<inc:module runat="server" ID="script">
    <script type="text/javascript" src="/js/angular.min.js"></script>
    <script type="text/javascript" src="/js/ui-bootstrap.js"></script>
    <script type="text/javascript" src="/js/angularFileUpload.js"></script>
    <script type="text/javascript" src="/js/bootstrap-colorpicker-module.js"></script>
	<script type="text/javascript" src="js/Controllers/CategoryController.js"></script>
    <script src="/js/bootstrap.min.js"></script>

</inc:module>

<div data-ng-app="roku" data-ng-controller="CategoryController">
    <style>
        .category_bg_color {
            /*Background Color Class*/
        }
        .category_txt_one {
            /*Category Titles*/
        }
        .category_txt_two {
        /*Category Descriptions*/
        }
    </style>
    <div class="BlankBar">&nbsp;</div>
    <div class="PgTitle">
        <div class="FloatLeft">
            <h1><span class="fa fa-th-large"></span> &nbsp; {{selectedChannel.title}} &nbsp; <asp:hyperlink runat="server" ID="lnkBackToChannels" CssClass="back-button"></asp:hyperlink></h1>
        </div>
        <div class="FloatRight">
            <button type="button" class="btn-inst" data-toggle="popover" data-placement="left" title="Channel Details Help">
              <span class="fa fa-question-circle"></span>
            </button>   
            <div class="clearboth">&nbsp;</div>  
        </div>
        <div class="clearboth">&nbsp;</div>
    </div>
    <div class="CntSubNav">
        <ul>
            <li><a data-ng-click="editStyles(selectedChannel)" >Channel Style</a></li>
            <li><a data-ng-click="categoryOrder(selectedChannel)" >Order Categories</a></li>

        </ul>
        <div class="clearboth">&nbsp;</div>
    </div>
    <div class="container-fluid TopBottomPad">
        <asp:HiddenField ID="hdChannelID" runat="server" />
        <asp:HiddenField ID="hdUserID" runat="server" />
        <div class="col-md-12">
            <div class="img-thumbnail FullWidth"  data-ng-style="channelBackground">

                <div class="CatHeader">
                    <img alt="{{selectedChannel.title}}" data-err-src="/images/sample_header.jpg" data-ng-src="{{selectedChannel.overhang_path}}" width="100%" height="165px" />
                    <div class="CatLogo">
                        <img class="img-responsive" alt="{{selectedChannel.title}}" data-err-src="/images/new_category_image.png" data-ng-src="{{selectedChannel.logo_path}}" />
                    </div>
                </div>

                <div class="row">
                    <div class="sortable col-xs-4 col-lg-3 text-center" data-ng-repeat="category in categories" data-ng-if="categories.length">
                        <div class="img-thumbnail">
                            <div class="ChannelInfo">
                                <img class="img-responsive CatImg" alt="{{category.title}}" data-err-src="/images/new_category_image.png" src="{{category.hd_path}}" />
                                <div class="ChannelBtns">
                                    <a class="btn btn-default btn-sm" data-ng-click="editCategoryScreen(category)"><span class="fa fa-pencil" aria-hidden="true"></span> &nbsp; Edit</a>&nbsp; &nbsp; &nbsp;<a class="btn btn-danger btn-sm" data-ng-click="deleteCategory(category)"><span class="fa fa-trash-o" aria-hidden="true"></span> &nbsp; Delete</a>
                                    <div class="clearboth">&nbsp;</div> 
                                </div>
                            </div>
                            <div class="ChannelTitle">
                                <h4 data-ng-style="channelTitles">{{category.title}} &nbsp; <a href="/playlists?categoryID={{category.id}}" class="btn-add"><span class="fa fa-plus-circle" aria-hidden="true"></span></a></h4>
                            </div>
                            <div class="CatDesc">
                                <div class="ShortTxt">
                                    <p data-ng-style="channelDescriptions">{{category.description}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div data-ng-click="addCategory()" class="add-channel col-xs-4 col-lg-3 text-center">
                        <div class="img-thumbnail">
                            <div class="NewChannel">
                                New Category
                                <br />
                                <span class="fa fa-plus-circle" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="popoverHiddenContent" style="display:none;">
    <p>This page displays a simple preview of what your channel will look like when viewed on ROKU.</p>
    <p>You can edit your channel style by clicking on 'Channel Style' in the grey menu bar.</p>
    <p>There is no limit to the number of categories you may add to your channel.</p>
    <p>You can edit your channel style by clicking 'Channel Style' in the grey menu bar.</p>
    <p>You can add playlists to each category by clicking on the <span class="fa fa-plus-circle" style="color:#c9ced3;"></span> next to the category name.</p>
    <p>You can change the order of your categories by clicking 'Order Categories' in the grey menu bar.</p>
</div>

<script type="text/javascript">
    $("[data-toggle=popover]").popover({
        html : true,
        content: function() {
         return $('#popoverHiddenContent').html();
        }
     });

</script>