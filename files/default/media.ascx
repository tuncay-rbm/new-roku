﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="Stripe" %>
<%@ Import Namespace="Newtonsoft.Json" %>
<%@ Import Namespace="System.Net.Mail" %>
<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            var obj = (rbm.Members.stMembers)Session["user"];
            hdUserID.Value = obj.id;
            hdAuthToken.Value = obj.authToken;
            hdUserName.Value = obj.username;
            hdUploadUrl.Value = rbm.getSett("uploader");
            hdProfileID.Value = rbm.API.EncodeProfile.getProfileGroupID(obj.id);
        }
        catch
        {
            Response.Redirect("/logout");
        }
    }


</script>

<asp:Label ID="meta_title" runat="server" Text="Media" Visible="false" />
<inc:module runat="server" id="login_check" />



<inc:module runat="server" ID="script">
    <script type="text/javascript" src="/js/angular.min.js"></script>
    <script type="text/javascript" src="/js/ui-bootstrap.js"></script>
    <script type="text/javascript" src="/js/angularFileUpload.js"></script>
    <script type="text/javascript" src="/js/directives/dirPagination.js"></script>	
    <script type="text/javascript" src="js/Controllers/MediaController.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</inc:module>

<div data-ng-app="roku" data-ng-controller="MediaController">
<asp:HiddenField ID="hdUserID" runat="server" />
<asp:HiddenField ID="hdAuthToken" runat="server" />
<asp:HiddenField ID="hdUploadUrl" runat="server" />
<asp:HiddenField ID="hdUserName" runat="server" />
<asp:HiddenField ID="hdProfileID" runat="server" />

    <div class="BlankBar">&nbsp;</div>
    <div class="PgTitle">
        <div class="FloatLeft">
            <h1><span class="fa fa-file-video-o"></span> &nbsp; Media &nbsp; <asp:hyperlink runat="server" ID="lnkBackToChannels" CssClass="back-button"></asp:hyperlink></h1>
        </div>
        <div class="FloatRight">
            <button type="button" class="btn-inst" data-toggle="popover" data-placement="left" data-content="Need to add How-To/Instructions for this page.">
              <span class="fa fa-question-circle"></span>
            </button>   
            <div class="clearboth">&nbsp;</div>  
        </div>
        <div class="clearboth">&nbsp;</div>
    </div>
    <div class="CntSubNav">
        <ul>
            <li><label class="btn btn-info btn-sm CustomLabel active">Browse Files
                    <input id="my-file-sd" style="display:none"  type="file" nv-file-select uploader="uploader" data-ng-click="uploader.clearQueue(); invalidFile = false"/><br/>
                </label>
            </li>
            <li><a id="my-file-url" class="btn btn-default btn-sm" ng-click="showUrlUpload()">Upload From URL</a></li>
        </ul>
        <div class="clearboth">&nbsp;</div>
    </div>

    <div class="container-fluid TopBottomPad">
        <div class="col-md-12">
  
            <!-- FILE QUEUE -->
            <div class="form-inline" data-ng-if="!spin" ng-repeat="item in uploader.queue">
                <div class="form-group">
                    <label for="fileNameSd" class="form-label">Name:</label>
                    <span id="fileNameSd" ng-bind="item.file.name" class="form-control"></span>
                </div>
                <div class="form-group">
                    <label for="fileSizeSd" class="form-label">Size:</label>
                    <span id="fileSizeSd" ng-bind="item.file.size" class="form-control"></span>
                </div>
                <div class="form-group">
                    <span id="fileSizeSd1" ng-bind="item.file.type" class="form-control"></span>
                </div>
                <a href="" class="btn btn-danger btn-sm" ng-click="uploader.clearQueue()">Cancel</a>
                <a  class="btn btn-default btn-sm" data-ng-click="item.upload(); watchFileStatus()">Upload File</a>
                
            </div>
            <div class="form-inline" data-ng-if="spin">
                <img src="/images/loading.gif" />
            </div>
            <div class="form-inline" data-ng-if="invalidFile">
                <p class="invalid-file">
                    {{invalidFileMessage}}
                </p>
            </div>
            <hr/>
            
                    <div class="table-responsive" data-ng-show="original_assets.length">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>FILE</th>
                                    <th class="CenterTxt">STATUS</th>
                                    <th class="CenterTxt"></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr data-ng-repeat="asset in original_assets" >
                                    <td>{{asset.name}}</td>
                                    <td class="CenterTxt" data-ng-class="asset.status">{{asset.status}}</td>
                                    <td class="CenterTxt">                
                                        <img data-ng-if="asset.status != 'failed'" src="/images/loading.gif" />
                                    </td>
                                    <td class="RightTxt">
                                        <a  title="Delete" class="btn btn-danger btn-xs" data-ng-click="deleteAsset(asset)"><span class="fa fa-trash-o" aria-hidden="true"></span></a>
                                        <%-- <a data-ng-click="deleteMedia(asset)" class="btn btn-danger btn-sm">DELETE</a>
                                        <a data-ng-click="addToPlaylist(asset)" class="btn btn-primary btn-sm">ADD TO PLAYLIST</a>
                                        <a data-ng-click="editAsset(asset)" class="btn btn-primary btn-sm">EDIT DETAILS</a> --%>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive" data-ng-show="original_assets.length===0">
                        <table class="table table-striped table-hover">
                            <tr>
                                <td>
                                    There are no media files processing. 
                                </td>
                            </tr>
                            </table>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th></th>
                                <th data-ng-click="sort('title')">NAME</th>
                                <th data-ng-click="sort('id')" class="CenterTxt">ADDED ON</th>
                                <th></th>
                            </tr>
                        </thead>  
                            <tbody>
                                <tr dir-paginate="video in encoded_assets |orderBy: 'createDate':true| itemsPerPage: 10" current-page="currentPage">
                                    <td style="width:85px;height:50px">
                                        <img class="img-responsive" alt="{{video.title}}" data-err-src="/images/NoImgAvailable.jpg" src="{{video.image_path}}" />
                                    </td>
                                    <td>{{video.title}}</td>
                                    <td class="CenterTxt">{{video.createDate | date: 'MM/dd/yyyy @ h:mma'}}</td>
                                    <td class="RightTxt">
                                        <a title="Edit" data-ng-click="editAsset(video)" class="btn btn-default btn-xs"><span class="fa fa-pencil" aria-hidden="true"></span></a>
                                        <a title="Add" data-ng-click="addVideoToPlaylist(video, $index)" class="btn btn-info btn-xs"><span class="fa fa-plus"></span></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    <div class="pull-right">
                        <dir-pagination-controls></dir-pagination-controls>
                        <div class="clearboth">&nbsp;</div>
                    </div>
                    </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $("[data-toggle=popover]").popover();
</script>