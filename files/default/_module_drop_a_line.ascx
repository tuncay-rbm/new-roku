﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>


<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
    }
</script>
<!-- START CONTACT FORM -->
<div class="col-md-6"  data-ng-controller="lineDropController">
    <div class="form-box">
        <div data-ng-if="!success">
            <h3>Drop us a Line!</h3>
            <div class="form-group">
                <label>Name:</label> &nbsp; <span class="error" data-ng-if="validation.error == true">{{validation.name}}</span>
                <input type="text" data-ng-model="formData.name" class="form-control" />
            </div>
            <div class="form-group">
                <label>Email:</label> &nbsp; <span class="error" data-ng-if="validation.error == true">{{validation.email}}</span>
                <input type="text" data-ng-model="formData.email" class="form-control" data-ng-change="validateEmail()" />
            </div>
            <div class="form-group">
                <label>Inquiry:</label> &nbsp; <span class="error" data-ng-if="validation.error == true">{{validation.message}}</span>
                <textarea data-ng-model="formData.message" class="form-control" rows="5"></textarea>
            </div>
            <div class="form-group">
                <label>
                    <input type="checkbox" data-ng-model="formData.subscribe" /> Subscribe me to ROKU Channel Builder emails.
                </label>
            </div>                 
            <div class="form-group" align="center">
                <a data-ng-click="dropTheLine()" class="btn btn-default">Send Your Inquiry</a>
            </div>
        </div>
        <div data-ng-if="success">
            <h3>Thank you for contacting Roku Channel Builder.</h3>
            <h3>We will get back to you soon!</h3>
        </div>
    </div>
</div>
<!-- END CONTACT FORM -->

<script>
    landing.controller('lineDropController', lineDropController);
    function lineDropController($scope, $http, $timeout) {
        ////console.log('line dropper!');
        $scope.success = false;
        $scope.formData = {
            name: '',
            email: '',
            message: '',
            subscribe: true
        }
        $scope.emptyForm = {
            name: '',
            email: '',
            message: '',
            subscribe: true
        }
        $scope.dropTheLine = function () {
            var formvalidate = true;
            $scope.validation = {
                error: true,
                name: '',
                email: '',
                message: ''

            };
            if ($scope.formData.email == undefined || $scope.formData.email == '') {
                $scope.validation.email = 'Valid email is required';
                formvalidate = false;
            }
            if ($scope.formData.name == undefined || $scope.formData.name == '') {
                $scope.validation.name = 'Name is required';
                formvalidate = false;
            }
            if ($scope.formData.message == undefined || $scope.formData.message == '') {
                $scope.validation.message = 'Message is required';
                formvalidate = false;
            }
            var emailregex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (emailregex.test($scope.formData.email)) {
                //console.log('email passes');
            } else {
                //console.log('email does not pass');
                $scope.validation.email = 'Valid email is required';
                formvalidate = false;
            }
            if (formvalidate === true) {
                var url = 'api.ashx?action=drop_a_line',
                jsonObject = {
                    id: '',
                    name: $scope.formData.name,
                    email: $scope.formData.email,
                    message: $scope.formData.message.substring(0,500),
                    subscribe: $scope.formData.subscribe === true ? '1' : '0'
                },
                json = encodeURIComponent(angular.toJson(jsonObject));
                //console.log(json);
                $http({
                    url: url,
                    method: 'POST',
                    data: "data=" + json,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).
                success(function (data, status, headers, config) {
                    $scope.formData = $scope.emptyForm;
                    $scope.success = true;
                    $scope.submitSuccess();


                }).
                error(function (data, status, headers, config) {

                });
            }

        }

        $scope.submitSuccess = function () {
            $timeout(function () {
                $scope.success = false;
            }, 5000);
        };
    };
</script>