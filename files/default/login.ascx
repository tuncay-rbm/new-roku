﻿<%@ Control Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>
<%@ Import Namespace="RBMLib" %>

<script runat="server">

	protected void Page_Load(object sender, EventArgs e){


	}
    protected void lnkLogin_Click(object sender, EventArgs e)
    {

            var user = rbm.Members.getMemberByEmail(txtUsername.Text);
            if (!rbm.isNull(user) && user.status == "1")
            {
                var obj = new rbm.API.stSignup();
                obj.email = txtUsername.Text;
                obj.password = txtPassword.Text;
                obj = rbm.API.AuthenticateUser(obj);
                if (!obj.isValid)
                {
                    lblErrLogin.Text = "Invalid credentials, please try again.";
                    lblErrLogin.Visible = true;
                }
                else
                {
                    //save token to db
                    user.authToken = obj.apiAuthToken;
                    rbm.Members.updateAuthToken(user.id, obj.apiAuthToken);
                    var myUser = rbm.Members.getData(user.id);
                    Session["user"] = myUser;
                    rbm.Log(DateTime.Now + "\tAction:" + "login" + "\tData:" + rbm.obj2json(myUser));
    
                    Response.Redirect("/channels");
                }
            }
            else
            {
                rbm.Log(DateTime.Now + "\tAction:" + "login_failed" + "\tData:" + rbm.obj2json(user));                
                lblErrLogin.Text = "Invalid credentials, please try again.";
                lblErrLogin.Visible = true;
            }  
        
    }
</script>

<asp:Label ID="meta_title" runat="server" Text="Roku Channel Builder" Visible="false" />
<inc:module ID="script" runat="server">
    <script type="text/javascript" src="/js/angular.min.js"></script>
    <script type="text/javascript" src="/js/ui-bootstrap.js"></script>
</inc:module>

<!-- LOGIN START -->
<div data-ng-app="roku" data-ng-controller="loginController">

    <div id="landing">
      <div class="LandingContainer">
        <div class="VideoOverlay">
          <div class="LandingCnt">
            <a href="/"><img alt="Roku Channel Builder" src="images/logo.png" alt="Roku Channel Builder" class="img-responsive LandingLogo"></a>
            
            <div class="CustomForm">
                <asp:Panel ID="form1" runat="server">
                    <div class="form-group">
                        <label>Username or Email</label>
                        <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" Placeholder="Email" />
                    </div>
                    <div class="form-group">
                        <asp:Label ID="lblPass" runat="server" AssociatedControlID="txtPassword">Password</asp:Label>
                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control" Placeholder="Password" />  
                    </div>
                    <div class="form-group">
                        <asp:Button ID="lnkLogin" title="Login" CssClass="btn btn-default btn-block" runat="server" OnClick="lnkLogin_Click" Text="Sign In"></asp:Button>
                    </div> 
                    <div class="form-group">
                        <asp:Label ID="lblErrLogin" runat="server" Text="Please check your username and password!" Visible="False" CssClass="error"></asp:Label>
                    </div>
                    <div class="LandingLinks">
                        <div class="FloatLeft">
                            <a id="lnkSend" data-ng-click="forgotPassword()">Forgot Password?</a>
                        </div>
                        <div class="FloatRight" align="right">
                            <a href="/signup" target="_self">Sign Up &nbsp;<span class="fa fa-long-arrow-right"></span></a>
                        </div>
                        <div class="clearboth">&nbsp;</div>
                    </div>
                </asp:Panel>
            </div>
          </div>
          <div class="VideoCnt">
            <video autoplay loop muted poster="video_img.jpg" id="bgvid">
              <source src="videos/video_bg.webm" type="video/webm">
              <source src="videos/video_bg.mp4" type="video/mp4">
              <source src="videos/video_bg.ogv" type="video/ogg" />
            </video>
          </div> 
        </div>
      </div>
    </div>
    <!-- end landing -->

</div>
<!-- LOGIN END -->
<script>
    jQuery(function () {
        jQuery('#txtUsername').focus();
    })
    angular.module('roku', ['ui.bootstrap']).controller('loginController', loginController);
    function loginController($scope, $filter, $http, $modal) {
        //console.log('here');
        $scope.forgotPassword = function () {
            $scope.editModal('lg');
        };

        $scope.editModal = function (size) {
            var modalInstance = $modal.open({
                templateUrl: '/templates/ForgotPassword.html',
                controller: function ($scope, $modalInstance, $sce) {
                    $scope.email = '';
                    //https://dev-api.rbmtv.com/v5/forgot_password_request?email=asdf@asdf.com&username=asdfasdfasdf
                    $scope.validForm = true;
                    $scope.resetSent = false;

                    $scope.sendReset = function () {
                        var resetEmail = $scope.email;
                        if (resetEmail == '' || resetEmail == 'undefined' || resetEmail == null) {
                            $scope.validForm = false;

                        } else {
                            $scope.validForm = true;
                            console.log($scope.email);
                            var url = 'api.ashx?action=reset_password&email=' + $scope.email;
                            $http({
                                method: 'POST',
                                url: url,
                                headers: { 'Content-Type': 'text/plain' }
                            }).
                            success(function (data, status, headers, config) {
                                console.log(data);
                                $scope.resetSent = true;
                            }).
                            error(function (data, status, headers, config) {
                                $scope.error = true;
                            });
                        }

                    }
                    $scope.closeModal = function () {
                        modalInstance.close();
                    }

                },
                size: size,
                resolve: {

                }
            });

            modalInstance.result.then(function () {
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }

</script>