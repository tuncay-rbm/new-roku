'********************************************************************
'**  Video Player Example Application - Main
'**  November 2009
'**  Copyright (c) 2009 Roku Inc. All Rights Reserved.
'********************************************************************

Sub Main()

    'initialize theme attributes like titles, logos and overhang color
    initTheme()

    'prepare the screen for display and get ready to begin
    screen=preShowHomeScreen("", "")
    if screen=invalid then
        print "unexpected error in preShowHomeScreen"
        return
    end if

    'set to go, time to get started
    showHomeScreen(screen)

End Sub


'*************************************************************
'** Set the configurable theme attributes for the application
'** 
'** Configure the custom overhang and Logo attributes
'** Theme attributes affect the branding of the application
'** and are artwork, colors and offsets specific to the app
'*************************************************************

Sub initTheme()

    app = CreateObject("roAppManager")
    theme = CreateObject("roAssociativeArray")

    theme.BackgroundColor = "{{category_bg_color}}"
    theme.PosterScreenLine1Text = "{{category_txt_one}}"
    theme.PosterScreenLine2Text = "{{category_txt_two}}"
    
    theme.OverhangOffsetSD_X = "72"
    theme.OverhangOffsetSD_Y = "31"
    theme.OverhangSliceSD = "pkg:/images/{{overhang_sd}}"
    theme.OverhangLogoSD  = "pkg:/images/{{logo_sd}}"
    

    theme.OverhangOffsetHD_X = "125"
    theme.OverhangOffsetHD_Y = "35"
    theme.OverhangSliceHD = "pkg:/images/{{overhang_hd}}"
    theme.OverhangLogoHD  = "pkg:/images/{{logo_hd}}"

	'DETAIL SCREEN
	theme.SpringboardSynopsisColor = "{{category_txt_two}}"
	theme.SpringboardTitleText = "{{category_txt_one}}"
	theme.SpringboardRuntimeColor = "{{category_txt_one}}"


    app.SetTheme(theme)

End Sub
