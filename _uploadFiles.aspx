﻿<%@ Page Language="C#" %>
<%@ Register TagPrefix="inc" Namespace="roku" Assembly="App_Code" %>
<%@ Import Namespace="RBMLib" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!lib.isNull(lib.form("action")))
        {

            string filename = "";
            if (!rbm.isNull(HttpContext.Current.Request.Files["file"]))
            {
                if (!rbm.isNull(lib.form("categoryID")))
                    filename = saveFile(lib.form("channelID"), true);
                else
                    filename = saveFile(lib.form("channelID"));
            }
            Response.Clear();
            switch (lib.form("action"))
            {
                    
                case "splash_sd":
                    rbm.Channel.SplashSD(lib.form("channelID"), filename);
                    lib.write("{\"splash_sd\": \"/_channel-images/" + lib.form("channelID") + "/" + filename + "\"}");
                    break;
                case "splash_hd":
                    rbm.Channel.SplashHD(lib.form("channelID"), filename);
                    lib.write("/_channel-images/" + lib.form("channelID") + "/" + filename );
                    break;

                case "side_icon_sd":
                    rbm.Channel.SideIconSD(lib.form("channelID"), filename);
                    lib.write("/_channel-images/" + lib.form("channelID") + "/" + filename);
                    break;
                case "side_icon_hd":
                    rbm.Channel.SideIconHD(lib.form("channelID"), filename);
                    lib.write("/_channel-images/" + lib.form("channelID") + "/" + filename);
                    break;
                case "focus_icon_sd":
                    rbm.Channel.FocusIconSD(lib.form("channelID"), filename);
                    lib.write("/_channel-images/" + lib.form("channelID") + "/" + filename);
                    break;
                case "focus_icon_hd":
                    rbm.CustomTasks.CreateChannelImages(lib.form("channelID"), filename);
                    lib.write("/_channel-images/" + lib.form("channelID") + "/" + filename);
                    break;
                case "logo_sd":
                    rbm.Channel.LogoSD(lib.form("channelID"), filename);
                    lib.write("/_channel-images/" + lib.form("channelID") + "/" + filename);
                    break;
                case "logo_hd":
                    rbm.CustomTasks.CreateLogoImages(lib.form("channelID"), filename);
                    lib.write("/_channel-images/" + lib.form("channelID") + "/" + filename);
                    break;
                case "overhang_sd":
                    rbm.Channel.OverhangSD(lib.form("channelID"), filename);
                    lib.write("/_channel-images/" + lib.form("channelID") + "/" + filename);
                    break;
                case "overhang_hd":
                    rbm.CustomTasks.CreateOverhangImages(lib.form("channelID"), filename);
                    lib.write("/_channel-images/" + lib.form("channelID") + "/" + filename);
                    break;
            }
        }

        Response.End();
    }
    protected string saveFile(string channelID, bool category = false)
    {
        HttpPostedFile file = HttpContext.Current.Request.Files["file"];
        
        try
        {
            string dir = "_channel-images/" + channelID + "/";
            if (category)
            {
                dir = "_category-images/" + channelID + "/";
            }
            string sDirectory = System.IO.Path.Combine(HttpRuntime.AppDomainAppPath, dir);
            if (!System.IO.Directory.Exists(sDirectory)) System.IO.Directory.CreateDirectory(sDirectory);
            //refreshPage = System.IO.File.Exists(HttpContext.Current.Server.MapPath("/" + imagePath)) == true ? true : false;
            System.Threading.Thread.Sleep(100);
            file.SaveAs(sDirectory + file.FileName);
            return file.FileName;
        }
        catch (Exception)
        {

            return file.FileName;

        }

    }
</script>

